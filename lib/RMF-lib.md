## RMF-lib

`C:\_\asciiw\lib`

- `FmColorName` - `purple-dark`
- `ColorHex` - `#BE67D5`
- `ColorRgb` - `190,103,213`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-purple-dark-48.png)](aip://open/C%3A%5C_%5Casciiw%5Clib) _Server Library_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-16.png)](aip://open/C%3A%5C_%5Casciiw%5CREADME.md) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Clib%5CDesktop.ini) - ini

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprism/asciiw/src/master/lib/) - bitbucket

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Clib%5Cget-query-params.js) `get-query-params.js` - Helper
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Clib%5Chttp-handle-ads.js) `http-handle-ads.js` - Add Server, `placekitten.com` 320x200, [r 'random']=0!, returns `Location`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Clib%5Chttp-handle-api.js) `http-handle-api.js` - `cool-ascii-faces` `application/x-json-stream` [limit]=10!, [skip]=0!, [sort]=id!||size||price
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Clib%5CREADME.html) `README.html`

