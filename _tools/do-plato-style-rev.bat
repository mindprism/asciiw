cd %~dp0
cd ..

COPY ._plato\assets\css\*.css .assets\plato\assets\css /y
COPY ._plato\assets\css\vendor\*.css .assets\plato\assets\css\vendor /y
COPY ._plato\assets\font\*.eot .assets\plato\assets\font /y
COPY ._plato\assets\font\*.svg .assets\plato\assets\font /y
COPY ._plato\assets\font\*.ttf .assets\plato\assets\font /y
COPY ._plato\assets\font\*.woff .assets\plato\assets\font /y
COPY ._plato\assets\scripts\*.js .assets\plato\assets\scripts /y
COPY ._plato\assets\scripts\bundles\*.js .assets\plato\assets\scripts\bundles /y
COPY ._plato\assets\scripts\vendor\*.js .assets\plato\assets\scripts\vendor /y
COPY ._plato\assets\scripts\vendor\codemirror\*.js .assets\plato\assets\scripts\vendor\codemirror /y
COPY ._plato\assets\scripts\vendor\codemirror\util\*.js .assets\plato\assets\scripts\vendor\codemirror\util /y
COPY ._plato\assets\scripts\vendor\codemirror\util\*.css .assets\plato\assets\scripts\vendor\codemirror\util /y

pause
