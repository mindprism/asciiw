## README

`C:\_\asciiw\_tools`

- `FmColorName` - `red-dark`
- `ColorHex` - `#F43F2A`
- `ColorRgb` - `244,63,42`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-48.png)](aip://open/C%3A%5C_%5Casciiw%5C_tools) _summary_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-16.png)](aip://open/C%3A%5C_%5Casciiw%5CREADME.md) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C_tools%5CDesktop.ini) - ini

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprism/asciiw/src/master/_tools/) - bitbucket

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_bat-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C_tools%5Cdo-esdoc-pt%31.bat) `do-esdoc-pt1.bat`
- [![](http://mrobbinsassoc.com/images/icons/md/_bat-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C_tools%5Cdo-esdoc.bat) `do-esdoc.bat`
- [![](http://mrobbinsassoc.com/images/icons/md/_json-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C_tools%5Cesdoc.json) `esdoc.json`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C_tools%5CREADME.html) `README.html`

