## README

`C:\_\asciiw\public`

- `FmColorName` - `green-dark`
- `ColorHex` - `#40C040`
- `ColorRgb` - `64,192,64`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-green-dark-48.png)](aip://open/C%3A%5C_%5Casciiw%5Cpublic) _Public assets_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-16.png)](aip://open/C%3A%5C_%5Casciiw%5CREADME.md) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Cpublic%5CDesktop.ini) - ini

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprism/asciiw/src/master/public/) - bitbucket

### Anticipates

_none_

### Folders

- ![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-dark-16.png) `images`

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Cpublic%5Canimate.css) `animate.css`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Cpublic%5Cfavicon.ico) `favicon.ico`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Cpublic%5Cindex.html) `index.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Cpublic%5Cother.css) `other.css`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Cpublic%5CREADME.html) `README.html`

