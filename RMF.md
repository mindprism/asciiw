## RMF

`C:\_\asciiw`

- `FmColorName` - `gray`
- `ColorHex` - `#9999A2`
- `ColorRgb` - `153,153,162`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-48.png)](aip://open/C%3A%5C_%5Casciiw) _Discount Ascii Warehouse React/Node Demo_

## Preface

Read [challenge.md](challenge.md) for specific info on this application demo

Bootstraps from [`create-react-app`](https://github.com/facebookincubator/create-react-app), see [README-bootstrap.md](README-bootstrap.md)

### Installation

- `npm install` or `yarn install`

### Npm Run Scripts

- `start` - Run sass build and watcher, start api server and dev server, watch source changes

- `start:all` - Run sass build and watcher, start api server and dev server, watch source changes

- `start:dev-html` - Start dev server, watch changes

- `start:api` - Start api server

- `css-build` - Sass Build

- `css-watch` - Sass Build and watch

- `build` - Build Sass and App to build directory

- `test` - Run Jest testing

- `eject` - IRREVERSABLE, remove project from `create-react-app` management

### Documentation Locations

- EsDocs - `._esdoc` output directory, also served at [asciiw EsDocs](http://mrobbinsassoc.com/projects/asciiw/esdocs), [Repo](https://bitbucket.org/mindprism/asciiw-esdoc)

- Plato - `._plato` output directory, also served at [asciiw Plato](http://mrobbinsassoc.com/projects/asciiw/plato) [Repo](https://bitbucket.org/mindprism/asciiw-plato)

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5CDesktop.ini) - ini

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprism/asciiw/src/master/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/plato-16.png)](http://mrobbinsassoc.com/projects/asciiw/plato/files/_index_js/index.html) - plato

### Anticipates

_none_

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-white-light-16.png)](aip://open/C%3A%5C_%5Casciiw%5C.assets%5CREADME.md) `.assets` - Various Assets for tools and site
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5C.git%5CDesktop.ini)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Casciiw%5C.git%5CDesktop.ini) `.git` - Wrapper Repository
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5C.idea%5CREADME.md) `.idea` - Webstorm Configuration
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5C._esdoc%5CREADME.md) `._esdoc`
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5C._plato%5CREADME.md) `._plato` - Plato Report Output
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5Cbuild%5CREADME.md) `build` - Production Build
- ![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-dark-16.png) `coverage`
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-purple-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5Clib%5CREADME.md) `lib` - Server Library
- ![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-dark-16.png) `node_modules`
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-green-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5Cpublic%5CREADME.md) `public` - Public assets
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-cyan-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5Csrc%5CREADME.md) `src` - Main Application
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-16.png)](aip://open/C%3A%5C_%5Casciiw%5C_extra%5CREADME.md) `_extra`
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5C_tools%5CREADME.md) `_tools`

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C%21i.txt) `!i.txt` - Information Pad, links to `_extra/_i/` and `.scrap/_extra/_i/`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C%21struct.lst) `!struct.lst` - Hierarchy of files
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C%21t.txt) `!t.txt` - Taskpad
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C%21w.txt) `!w.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.editorconfig) `.editorconfig`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.eslintignore) `.eslintignore`
- [![](http://mrobbinsassoc.com/images/icons/md/_eslintrc-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.eslintrc) `.eslintrc`
- [![](http://mrobbinsassoc.com/images/icons/md/_gitignore-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.gitignore) `.gitignore`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Casciiw.url) `asciiw.url`
- [![](http://mrobbinsassoc.com/images/icons/md/_vpj-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Casciiw.vpj) `asciiw.vpj` - SlickEdit Project
- [![](http://mrobbinsassoc.com/images/icons/md/_vpw-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Casciiw.vpw) `asciiw.vpw` - SlickEdit Workspace
- [![](http://mrobbinsassoc.com/images/icons/md/_md-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Cchallenge.md) `challenge.md`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-index-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Cindex.js) `index.js` - Server Entry
- [![](http://mrobbinsassoc.com/images/icons/md/_json-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Cpackage.json) `package.json`
- [![](http://mrobbinsassoc.com/images/icons/md/_md-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5CREADME-bootstrap.md) `README-bootstrap.md`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5CREADME.html) `README.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_md-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5CREADME.md) `README.md`
- [![](http://mrobbinsassoc.com/images/icons/md/_md-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5CRMF.md) `RMF.md`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Cyarn-error.log) `yarn-error.log`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Cyarn.lock) `yarn.lock`

