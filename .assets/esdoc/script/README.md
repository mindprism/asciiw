## README

`C:\_\asciiw\.assets\esdoc\script`

- `FmColorName` - `black`
- `ColorHex` - `#525257`
- `ColorRgb` - `82,82,87`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-48.png)](aip://open/C%3A%5C_%5Casciiw%5C.assets%5Cesdoc%5Cscript) _Source EsDoc built-in scripts, and additional scripts plus `manual.js` custom script_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5C.assets%5Cesdoc%5CREADME.md) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.assets%5Cesdoc%5Cscript%5CDesktop.ini) - ini

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprism/asciiw/src/master/.assets/esdoc/script/) - bitbucket

### Anticipates

_none_

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-16.png)](aip://open/C%3A%5C_%5Casciiw%5C.assets%5Cesdoc%5Cscript%5Cprettify%5CREADME.md) `prettify` - Source Prettify Code

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.assets%5Cesdoc%5Cscript%5Cclipboard.min.js) `clipboard.min.js` - Clipboard Copy
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.assets%5Cesdoc%5Cscript%5CcontextMenu.min.js) `contextMenu.min.js` - Context Menu
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.assets%5Cesdoc%5Cscript%5Cjquery-%32.%31.%34.min.js) `jquery-2.1.4.min.js` - EsDocs wants
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.assets%5Cesdoc%5Cscript%5Cjquery.qtip.min.js) `jquery.qtip.min.js` - QTip2 for jQuery
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.assets%5Cesdoc%5Cscript%5CloadCSS.js) `loadCSS.js` - Load CSS file asynch with window.loadCSS(href, before, media, idd)`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.assets%5Cesdoc%5Cscript%5Clscache.min.js) `lscache.min.js` - localStorage extension [lscashe Github](https://github.com/pamelafox/lscache)
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.assets%5Cesdoc%5Cscript%5Cmanual.js) `manual.js` - Mark Robbins EsDoc Enhancements `window._`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.assets%5Cesdoc%5Cscript%5Cnotify.min.js) `notify.min.js` - [notifyjs.com](https://notifyjs.com/)
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.assets%5Cesdoc%5Cscript%5ConloadCSS.js) `onloadCSS.js` - adds onload support for asynchronous stylesheets loaded with `loadCSS.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.assets%5Cesdoc%5Cscript%5CREADME.html) `README.html`

