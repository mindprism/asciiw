## RMF-.assets-plato-assets-css

`C:\_\asciiw\.assets\plato\assets\css`

- `FmColorName` - `gray-dark`
- `ColorHex` - `#7D7D85`
- `ColorRgb` - `125,125,133`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-48.png)](aip://open/C%3A%5C_%5Casciiw%5C.assets%5Cplato%5Cassets%5Ccss) _Source Plato Styles_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Casciiw%5C.assets%5Cplato%5Cassets%5CDesktop.ini) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.assets%5Cplato%5Cassets%5Ccss%5CDesktop.ini) - ini

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprism/asciiw/src/master/.assets/plato/assets/css/) - bitbucket

### Anticipates

_none_

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5C.assets%5Cplato%5Cassets%5Ccss%5Cvendor%5CREADME.md) `vendor` - Source Plato Vender Styles

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.assets%5Cplato%5Cassets%5Ccss%5Cmorris.css) `morris.css`
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.assets%5Cplato%5Cassets%5Ccss%5Cplato-display.css) `plato-display.css`
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.assets%5Cplato%5Cassets%5Ccss%5Cplato-file.css) `plato-file.css`
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.assets%5Cplato%5Cassets%5Ccss%5Cplato-overview.css) `plato-overview.css`
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.assets%5Cplato%5Cassets%5Ccss%5Cplato.css) `plato.css`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.assets%5Cplato%5Cassets%5Ccss%5CREADME.html) `README.html`

