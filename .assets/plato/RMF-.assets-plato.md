## RMF-.assets-plato

`C:\_\asciiw\.assets\plato`

- `FmColorName` - `red-dark`
- `ColorHex` - `#F43F2A`
- `ColorRgb` - `244,63,42`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-48.png)](aip://open/C%3A%5C_%5Casciiw%5C.assets%5Cplato) _Source Plato Overrides_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-white-light-16.png)](aip://open/C%3A%5C_%5Casciiw%5C.assets%5CREADME.md) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.assets%5Cplato%5CDesktop.ini) - ini

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprism/asciiw/src/master/.assets/plato/) - bitbucket

### Anticipates

_none_

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5C.assets%5Cplato%5Cassets%5CREADME.md) `assets` - Source Plato styles, fonts and scripts

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.assets%5Cplato%5CREADME.html) `README.html`

