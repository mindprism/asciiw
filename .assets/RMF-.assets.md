## RMF-.assets

`C:\_\asciiw\.assets`

- `FmColorName` - `white-light`
- `ColorHex` - `#FDFDFD`
- `ColorRgb` - `253,253,253`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-white-light-48.png)](aip://open/C%3A%5C_%5Casciiw%5C.assets) _Various Assets for tools and site_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-16.png)](aip://open/C%3A%5C_%5Casciiw%5CREADME.md) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.assets%5CDesktop.ini) - ini

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprism/asciiw/src/master/.assets/) - bitbucket

### Anticipates

- `< more-types>`

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5C.assets%5Cesdoc%5CREADME.md) `esdoc` - EsDoc enhancements, css and script

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.assets%5CREADME.html) `README.html`

