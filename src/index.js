/* global */
/**
 * <h4>`App` entry mount point</h4>
 * <a href="aip://open/C%3A%5C%5F%5Casciiw%5Csrc%5Cindex%2Ejs">C:\_\asciiw\src\index.js</a><br>
 * <br/><br/>
 * <h5>NPM Dependencies</h5>
 * none
 * <br/><br/>
 * <h5>Project Dependencies</h5>
 * - {@link Utils} - `./utils`
 * <br/><br/>
 * <h5>Local Dependencies</h5>
 * lll
 * <br/><br/>
 * @protected
 * @see https://bitbucket.org/mindprism/asciiw/src/master/src/index.js
 */
const src_index_js= "/src/index.js";

/// Base Imports
import React from 'react';
import ReactDOM from 'react-dom';
///
/// Util Imports
import Utils from './utils/utils';
import { use } from './utils/utils';
///
/// Local Imports
import App from './App.jsx';
import './index.css';
///
/// Local Helpers
use(Utils);
use(src_index_js);

/**
 * <h4>`ReactDom render App` mount point</h4>
 * <br/><br/>
 * @protected
 */
const ReactDom_render_App = true;
use(ReactDom_render_App);

/**
 * <h4>`App` mount point</h4>
 * <br/><br/>
 * @protected
 */
ReactDOM.render(
  <App />,
  document.getElementById('root')
); //-render
