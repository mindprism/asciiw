## RMF-src-utils

`C:\_\asciiw\src\utils`

- `FmColorName` - `purple-dark`
- `ColorHex` - `#BE67D5`
- `ColorRgb` - `190,103,213`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-purple-dark-48.png)](aip://open/C%3A%5C_%5Casciiw%5Csrc%5Cutils) _Utilities and Data Containers_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-cyan-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5Csrc%5CREADME.md) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Cutils%5CDesktop.ini) - ini

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprism/asciiw/src/master/src/utils/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/esdoc-16.png)](http://mrobbinsassoc.com/projects/asciiw/esdoc/file/src/utils/utils.js.html) - esdoc
- [![](http://mrobbinsassoc.com/images/icons/md/plato-16.png)](http://mrobbinsassoc.com/projects/asciiw/plato/files/utils_utils_js/index.html) - plato

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Cutils%5CJestHelpers.js) `JestHelpers.js` - Jest Static class
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Cutils%5CMultiSortObjectCache.js) `MultiSortObjectCache.js` - Data Container
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Cutils%5CMultiSortObjectCache.test.js) `MultiSortObjectCache.test.js` - Jest
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Cutils%5CREADME.html) `README.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Cutils%5Cutils.js) `utils.js` - Static utility class

