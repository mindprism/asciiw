/* global */
/**
 * <h4>`MultiSortObjectCache` suites</h4>
 * <a href="aip://open/C%3A%5C%5F%5Casciiw%5Csrc%5Cutils%5CMultiSortObjectCache%2Etest%2Ejs">C:\_\asciiw\src\utils\MultiSortObjectCache.test.js</a><br>
 * <br/><br/>
 * <h5>NPM Dependencies</h5>
 * <br/><br/>
 * <h5>Project Dependencies</h5>
 * - {@link Utils} - `./utils`
 * <br/><br/>
 * <h5>Local Dependencies</h5>
 * - {@link MultiSortObjectCache} - `./MultiSortObjectCache`
 * <br/><br/>
 * @protected
 * @see https://bitbucket.org/mindprism/asciiw/src/master/src/utils/MultiSortObjectCache.test.js
 */
const src_utils_MultiSortObjectCache_test_js= "/src/utils/MultiSortObjectCache.test.js";

///
/// Util Imports
import Utils from './utils';
import { use } from './utils';
import JestHelpers from './JestHelpers';

const J = JestHelpers;
// console.log(J);
// console.log(J.expectType);
// console.log(J.expectArray);
///
/// Local Imports
import MultiSortObjectCache from './MultiSortObjectCache';
///
/// Local Helpers
use(Utils);
use(src_utils_MultiSortObjectCache_test_js);
///
/// Testing Helpers

///
/// Test Suites
/**
 * <h4>`MultiSortObjectCache should construct` suite</h4>
 * <br/><br/>
 * @protected
 */
const MultiSortObjectCache_should_construct_suite = true;
if(MultiSortObjectCache_should_construct_suite) {
  /**
   * @test {MultiSortObjectCache}
   */
  describe('should construct', () => {
    /**
     *
     */
    it('runs a dummy test', () => {
    }); //-test
    /**
     * @test {MultiSortObjectCache}
     */
    it('creates without throw', () => {
      //noinspection ObjectAllocationIgnored
      new MultiSortObjectCache({ keyField:'id' });
    }); //-test
  }); //-suite
} //-MultiSortObjectCache_should_construct_suite

/**
 * <h4>`MultiSortObjectCache instances` suite</h4>
 * <br/><br/>
 * @protected
 */
const MultiSortObjectCache_instances_suite = true;
if(MultiSortObjectCache_instances_suite) {
  /**
   * @test {MultiSortObjectCache}
   */
  describe('with an instance', () => {
    let me;
    const test1 = { id:'id1', price:10, size:2 };
    const test2 = { id:'id2', price:100, size:20 };
    /**
     *
     */
    beforeAll(() => {
      me=new MultiSortObjectCache({ keyField:'id' });
    }); //-beforeAll
    /**
     * @test {MultiSortObjectCache.sortedBy}
     */
    it('sortedBy should return array that is empty', () => {
      const a=me.sortedBy();
      J.expectArray(a);
      expect(a.length).toBe(0);
    }); //-test
    /**
     * @test {MultiSortObjectCache.add}
     * @test {MultiSortObjectCache.clear}
     * @test {MultiSortObjectCache.sortedBy}
     */
    it('Allows adding of object, and has it, and clear clears all', () => {
      me.add(test1);
      let a=me.sortedBy();
      J.expectArray(a);
      expect(a.length).toBe(1);
      expect(a[0]).toEqual(test1);
      me.clear();
      a=me.sortedBy();
      J.expectArray(a);
      expect(a.length).toBe(0);
    }); //-test
    /**
     * @test {MultiSortObjectCache.add}
     * @test {MultiSortObjectCache.clear}
     * @test {MultiSortObjectCache.sortedBy}
     */
    it('Takes two objects and sorts them properly', () => {
      me.clear();
      me.add(test2);
      me.add(test1);
      const a=me.sortedBy('id');
      expect(a[0]).toEqual(test1);
      expect(a[1]).toEqual(test2);
    }); //-test
  }); //-suite
} //-MultiSortObjectCache_instances_suite

