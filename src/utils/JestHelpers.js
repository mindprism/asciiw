/* global */

class JestHelpers {
  static expectType(instance, type) {
    //noinspection JSUnresolvedFunction
    return expect(instance).toBeInstanceOf(type);
  }
  static expectArray(instance) {
    return this.expectType(instance, Array);
  }
}
// JestHelpers.expectType = function(instance,type) {
//     return expect(instance).toBeInstanceOf(type);
//   };
// JestHelpers.expectArray = function(instance){
//     return this.expectType(instance, Array);
//   };

export default JestHelpers;
