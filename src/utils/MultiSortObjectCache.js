/* global */
/**
 * <h4>`MultiSortObjectCache` data container</h4>
 * <a href="aip://open/C%3A%5C%5F%5Casciiw%5Csrc%5Cutils%5CMultiSortObjectCache%2Ejs">C:\_\asciiw\src\utils\MultiSortObjectCache.js</a><br>
 * <br/><br/>
 * <h5>NPM Dependencies</h5>
 * - {@link $}              - `jquery`
 * <br/><br/>
 * <h5>Project Dependencies</h5>
 * - {@link Utils} - `../utils/utils`
 * <br/><br/>
 * <h5>Local Dependencies</h5>
 * <br/><br/>
 * @protected
 * @see https://bitbucket.org/mindprism/asciiw/src/master/src/utils/MultiSortObjectCache.js
 */
const src_utils_MultiSortObjectCache_js= "/src/utils/MultiSortObjectCache.js";

/// Base Imports
//noinspection JSUnresolvedVariable
import $ from 'jquery';
///
/// Util Imports
import Utils from './utils';
import { use, Dispatch } from '../utils/utils';
///
/// Local Helpers
use($);
use(Utils);
use(Dispatch);
use(src_utils_MultiSortObjectCache_js);

/// Class
/**
 * <h4>`MultiSortObjectCache` data container class</h4>
 * Keep unique collection of objects, retreive by sort
 * <br/><br/>
 * <h5>private members</h5>
 * - {@link MultiSortObjectCache#_options} - keyField :String (*undefined)
 * - {@link MultiSortObjectCache#_cache} - Items array
 * <h5>constructor</h5>
 * - {@link MultiSortObjectCache#constructor} - Assign options
 * <h5>private commands</h5>
 * - {@link MultiSortObjectCache#_add} - If not cache has, add and return true, else false
 * - {@link MultiSortObjectCache#_removeUsing} - Remove from cache using key or object, and selector function
 * - {@link MultiSortObjectCache#_remove} - If have options keyField then remove using key else compare using stringify
 * <h5>public commands</h5>
 * - {@link MultiSortObjectCache#clear} - Clear All Items
 * - {@link MultiSortObjectCache#remove} - Remove given item or array of items
 * - {@link MultiSortObjectCache#has} - Determine if in items using object or key and optional keyfield
 * - {@link MultiSortObjectCache#add} - Add object or array of objects, keep unique
 * - {@link MultiSortObjectCache#sortedBy} - Return new array of object sorted
 * <br/><br/>
 * <h5>Dependencies</h5>
 * - {@link $}       - `jquery`
 * <br/><br/>
 * - file {@link src_utils_MultiSortObjectCache_js}
 * @protected
 */
class MultiSortObjectCache {
  //
  /**
   * <h4>`MultiSortObjectCache#_options` constructor options</h4>
   * keyField :String (*undefined)
   * <br/><br/>
   * @protected
   */
  _options = null;

  /**
   * <h4>`MultiSortObjectCache#_cache`</h4>
   * Items array
   * <br/><br/>
   * @protected
   */
  _cache = [];

  /**
   * <h4>`MultiSortObjectCache` constructor</h4>
   * Assign options
   * <br/><br/>
   * @protected
   */
  constructor(options = {}) {
    // options.sortKeys = []
    // options.keyField
    this._options = Object.assign({}, options);
  }

  /**
   * <h4>`MultiSortObjectCache#_add` private command</h4>
   * If not cache has, add and return true, else false
   * <br/><br/>
   * @protected
   */
  _add(object) {
    if (!this.has(object)) {
      this._cache.push(object);
      return true;
    }
    return false;
  }

  /**
   * <h4>`MultiSortObjectCache#_removeUsing` private command</h4>
   * Remove from cache using key or object, and selector function
   * <br/><br/>
   * @protected
   * @param {string|Object} objectOrKey - if object fn operates on it to get find value
   * @param {function} fn - (object) = > return some find
   * @return {Object}
   */
  _removeUsing(objectOrKey, fn) {
    const c = this._cache;
    const l = c.length;
    const cache2 = [];
    const removed = [];
    const find = ('string'===typeof objectOrKey)? objectOrKey:fn(objectOrKey);
    for (let i = 0; i < l; i++) {
      const item = c[i];
      const val= fn(item);
      if (val === find) {
        removed.push(item);
      } else {
        cache2.push(item);
      }
    }
    this._cache = cache2;
    return removed;
  }

  /**
   * <h4>`MultiSortObjectCache#_remove` private command</h4>
   * If have options keyField then remove using key else compare using stringify
   * <br/><br/>
   * ToDo: setup exclusion fields for stringify in options
   * @protected
   * @param {string|Object} objectOrKey
   */
  _remove(objectOrKey) {
    if (this._options.keyField) {
      return this._removeUsing(objectOrKey, (item) => {return item[this._options.keyField];});
    } else {
      return this._removeUsing(objectOrKey, JSON.stringify);
    }
  }

  /**
   * <h4>`MultiSortObjectCache#clear` public command</h4>
   * Clear All Items
   * <br/><br/>
   * @protected
   */
  clear() {
    this._cache = [];
  }

  /**
   * <h4>`MultiSortObjectCache#remove` public command</h4>
   * Remove given item or array of items
   * <br/><br/>
   * @protected
   * @param {Object|[Object]} itemOrItems
   * @return {[Object]} Array of removed
   */
  remove(itemOrItems) {
    let removed = [];
    if ($.isArray(itemOrItems)) {
      const l=itemOrItems.length;
      for (let i = 0; i < l; i++) {
        const item = itemOrItems[i];
        removed = removed.concat(this._remove(item));
      }
    } else {
      removed = removed.concat(this._remove(itemOrItems));
    }
    return removed;
  }

  /**
   * <h4>`MultiSortObjectCache#has` public query</h4>
   * Determine if in items using object or key and optional keyfield
   * <br/><br/>
   * Use options keyField if not provided
   * <br/><br/>
   * @protected
   * @param {string|Object} keyOrObject
   * @param {string} [keyField]
   * @return {boolean} if found
   *
   */
  has(keyOrObject, keyField) {
    const c = this._cache;
    const l = c.length;
    const type = typeof keyOrObject;
    const kf = keyField||this._options.keyField;
    const useJson = (!kf);
    //    type           kf       find               from
    //     string         F        keyOrObject        JSON(item)
    //     string         T        keyOrObject        item[kf]
    //     object         F        JSON(keyOrObject)  JSON(item)
    //     object         T        keyOrObject[kf]    item[kf]
    let f;
    if ('string' === type) {
      f = keyOrObject;
    } else if ('object' === type) {
      f = useJson ? JSON.stringify(keyOrObject) : keyOrObject[kf];
    }
    //noinspection UnnecessaryLocalVariableJS
    const find = f;
    for (let i = 0; i < l; i++) {
      const item = c[i];
      const val = useJson ? JSON.stringify(item) : item[kf];
      if (val === find) {
        return true;
      }
    }
    return false;
  }

  /**
   * <h4>`MultiSortObjectCache#add` main public command</h4>
   * Add object or array of objects, keep unique
   * <br/><br/>
   * @protected
   * @param {Object|[Object]} objectOrObjects
   */
  add(objectOrObjects) {
    const given = objectOrObjects;
    if (!$.isArray(given)) {
      const rv = this._add(given);
      return rv ? 1 : 0;
    }
    let ct = 0;
    const l = given.length;
    for (let i = 0; i < l; i++) {
      const item = given[i];
      const rv = this._add(item);
      ct += rv ? 1 : 0;
    }
    return ct;
  }

  /**
   * <h4>`MultiSortObjectCache#sortedBy` main public query</h4>
   * Return new array of object sorted
   * <br/><br/>
   * @public
   * @param {string} [sortField] (options.keyField)
   * @param {boolean} [descending] (false)
   */
  sortedBy(sortField, descending) {
    const sf = sortField||this._options.keyField;
    const useJson = (!sf);
    const c = this._cache;
    //const l = c.length;
    const ra = c.slice(0);
    function compare(a, b) {
      if (a < b) {return -1;}
      if (a > b) {return 1;}
      return 0;
    }
    ra.sort((a, b) => {
      if (useJson) {
        return compare(JSON.stringify(a), JSON.stringify(b));
      }
      return compare(a[sf], b[sf]);
    });
    if (descending) {
      return ra.reverse();
    }
    return ra;
  }
}
/**
 * <h4>`MultiSortObjectCache` data container default export</h4>
 * <br/><br/>
 * - file {@link src_utils_MultiSortObjectCache_js}
 * @protected
 */
const MultiSortObjectCache_export = 'EsDocs symbol';
use(MultiSortObjectCache_export);
export default MultiSortObjectCache;
