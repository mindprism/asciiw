/* global */
/**
 * <h4>`Utils` static singleton </h4>
 * <a href="aip://open/C%3A%5C%5F%5Casciiw%5Csrc%5Cutils%5Cutils%2Ejs">C:\_\asciiw\src\utils\utils.js</a><br>
 * <br/><br/>
 * Standard Utils
 * <br/><br/>
 * <h5>NPM Dependencies</h5>
 * - {@link $}              - `jquery`
 * - {@link PubSub}         - `pubsub-js`
 * <br/><br/>
 * <h5>Project Dependencies</h5>
 * <br/><br/>
 * <h5>Local Dependencies</h5>
 * <br/><br/>
 * @protected
 * @see https://bitbucket.org/mindprism/asciiw/src/master/src/utils/utils.js
 */
const src_utils_utils_js = "/src/utils/utils.js";

/// Base Imports
//noinspection JSUnresolvedVariable
import $ from 'jquery';
import PubSub from 'pubsub-js';
///
/// Local Helpers

///
/// Exports
//noinspection JSUnusedLocalSymbols
/**
 * <h4>`use` function export</h4>
 * Webstorm noop to get rid of unused lint warnings
 * <br/><br/>
 * @public
 * @return {Object} - null
 */
export function use(...many) { // eslint-disable-line
  return null;
}

/// Class
/**
 * <h4>`Utils` static utility class</h4>
 * <br/><br/>
 * <h5>public static functions</h5>
 * - {@link Utils.noop} - do nothing
 * - {@link Utils.isWindowScrollAtBottom} - detector
 * - {@link Utils.isWindowScrollAtTop} - detector
 * - {@link Utils.simpleCurrencyFromCents} - converter
 * - {@link Utils.djb2} - Hash a string
 * - {@link Utils.hashStringToColor} - Convert arbritrary string to color
 * <br/><br/>
 * <h5>public static commands</h5>
 * - {@link Utils.updateNamedStyle} - Replace or create styleStr to head with id and callback
 * - {@link Utils.jQueryExtendAnimateCss} - Plugin AnimateCss for animate.css
 * <br/><br/>
 * <h5>Dependencies</h5>
 * - {@link $}              - `jquery`
 * @public
 */
class Utils {
  //
  //noinspection JSUnusedLocalSymbols
  /**
   * <h4>`Utils.noop` static function</h4>
   * Takes any, returns nothing, no action
   * <br/><br/>
   * @public
   * @param many -
   */
  static noop(...many) { // eslint-disable-line
  }

  /**
   * <h4>`Utils.isWindowScrollAtBottom` static function</h4>
   * <br/><br/>
   * @public
   */
  static isWindowScrollAtBottom() {
    return $(window).scrollTop() + $(window).height() === $(document).height();
  }

  /**
   * <h4>`Utils.isWindowScrollAtTop` static function</h4>
   * <br/><br/>
   * @public
   */
  static isWindowScrollAtTop() {
    return 0 === $(window).scrollTop();
  }

  /**
   * <h4>`Utils.simpleCurrencyFromCents`</h4>
   * Given 0 return '$ 0.00', positive numbers only
   * <br/><br/>
   * @public
   * @param {string|number} cents
   */
  static simpleCurrencyFromCents(cents) {
    const n=parseInt(cents, 10);
    //noinspection MagicNumberJS
    const ten = 10;
    //noinspection MagicNumberJS
    const hundred = 100;
    if(ten > n) {
      return `$ 0.0${n}`;
    }
    if(hundred > n) {
      return `$ 0.${n}`;
    }
    const d=Math.floor(n/hundred);
    const c=n-(d*hundred);
    const cc=`${ten> c ? '0' + c : '' + c}`;
    return `$ ${d}.${cc}`;
  }

  /**
   * <h4>`Utils.updateNamedStyle` static function</h4>
   * Replace or create styleStr to head with id and callback
   * <br/><br/>
   * @public
   * @param {string} styleStr
   * @param {string} id
   * @param {function} callback (*null)
   */
  static updateNamedStyle(styleStr, id, callback = null) {
    const st=document.getElementById(id);
    if (null === st) {
      const head = document.getElementsByTagName("HEAD")[0];
      const ele = head.appendChild(window.document.createElement('style'));
      const q = $(ele);
      q.attr('id', id);
      q.attr('type', 'text/css');
      if (callback !== undefined) {
        q.on('load', callback);
      }
      ele.innerHTML = styleStr;
    } else {
      st.innerHTML = styleStr;
    }
  }

  /**
   * <h4>`Utils.jQueryExtendAnimateCss` static command</h4>
   * Plugin AnimateCss for animate.css
   * <br/><br/>
   * @public
   * @param $  - jQuery instance
   */
  static jQueryExtendAnimateCss($) {
    $.fn.extend({
      animateCss: function (animationName) {
        const animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        const classes=`animated ${animationName}`;
        this.addClass(classes).one(animationEnd, function() {
          $(this).removeClass(classes);
        });
      }
    });
  }

  /**
   * <h4>Utils.djb2` static function</h4>
   * Hash a string
   * <br/><br/>
   * @public
   * @param {string} str
   * @return  {number}
   */
  static djb2(str) {
    //noinspection MagicNumberJS
    let hash = 5381;
    for (let i = 0; i < str.length; i++) {
      hash = ((hash << 5) + hash) + str.charCodeAt(i); /* hash * 33 + c */
    }
    return hash;
  }

  /**
   * <h4>`Utils.hashStringToColor`</h4>
   * Convert arbritrary string to color
   * <br/><br/>
   * @public
   * @param {string} str
   * @return {string} like #FFFFFF
   */
  static hashStringToColor(str) {
    const hash = this.djb2(str);
    //noinspection MagicNumberJS,NonShortCircuitBooleanExpressionJS
    const r = (hash & 0xFF0000) >> 16;
    //noinspection MagicNumberJS,NonShortCircuitBooleanExpressionJS
    const g = (hash & 0x00FF00) >> 8;
    //noinspection MagicNumberJS,NonShortCircuitBooleanExpressionJS
    const b = hash & 0x0000FF;
    return `#${("0" + r.toString(16)).substr(-2)}${("0" + g.toString(16)).substr(-2)}${("0" + b.toString(16)).substr(-2)}`;
  }
}
/**
 * <h4>`Utils` default export</h4>
 * <br/><br/>
 * - file {@link src_utils_utils_js}
 * @protected
 */
const Utils_export = 'EsDocs symbol';
use(Utils_export);
export default Utils;
use(src_utils_utils_js);

/**
 * <h4>`Dispatch` {@link PubSub} class</h4>
 * Used on static `pubs` object initialized by {@link Dispatch.initPubs}
 * <br/><br/>
 * <h5>public static command</h5>
 * - {@link Dispatch.initPubs} - Create a new dispatch instance on every object path of `that.pubs`
 * - {@link Dispatch.dataOf} - Given any value or object or object created by {BaseComponent#say}, dig out actual data
 * - {@link Dispatch.listHear} - Create code to listen to class pubs
 * <br/><br/>
 * <h5>constructor</h5>
 * - {@link Dispatch#constructor} - Set `.msg`, `._what`, init `.__log`
 * <br/><br/>
 * <h5>private commands</h5>
 * - {@link Dispatch#_log} - Add new item to log with date, trim logs
 * <br/><br/>
 * <h5>public readonly properties</h5>
 * - {@link Dispatch#what} - Returns documentation string from _what
 * <br/><br/>
 * <h5>public commands</h5>
 * - {@link Dispatch#hear} - Subscribe, add to fn.hears and return token
 * - {@link Dispatch#say} - Async publish data with extra topic, log it
 * - {@link Dispatch#yell} - Synch publish data with extra topic, log it
 * - {@link Dispatch#sleep} - Unsubscribe function, keep its hears
 * - {@link Dispatch#wake} - Use functions hears to re-subscribe
 * - {@link Dispatch#kill} - Permanently unsubscribe funtion or token
 * <br/><br/>
 * <h5>Dependencies</h5>
 * - {@link PubSub}         - `pubsub-js`
 * <br/><br/>
 * @protected
 */
export class Dispatch {
  //
  /**
   * <h4>`Dispatch.initPubs`</h4>
   * Create a new dispatch instance on every object path of `that.pubs`
   * <br/><br/>
   * @public
   * @param {Object} that - the clas with `pubs` object
   */
  static initPubs(that) {// that is class
    ///                                         bail if no pubs
    if(!that.pubs) {
      return;
    }
    ///                                         bail if done already
    if(that.__pubs) {
      return;
    }
    ///                                         recursive proc
    function addIt(o, n) {
      for(let i in o) {
        if(!o.hasOwnProperty(i)) {
          continue;
        }
        const v=o[i];
        const t=typeof v;
        if('object'===t) {
          if(null !== v) {
            addIt(v, `${n}.${i}`);
          }
        } else if('string'===t) {
          o[i]=new Dispatch(`${n}.${i}`, v);
        }
      }
    }
    ///                                         capture snapshot for later
    const pubs=JSON.stringify(that.pubs);
    ///                                         do changes
    addIt(that.pubs, that.name);
    ///                                         mark as done, store snapshot
    console.log('initPubs:', that.name);
    that.__pubs=pubs;
  }

  /**
   * <h4>`Dispatch.dataOf` static public command</h4>
   * Given any value or object or object created by {BaseComponent#say}, dig out actual data
   * <br/><br/>
   * @public
   */
  static dataOf(data) {
    ///                                         detector utility
    function isWrapper(data) {
      if (data===undefined) {
        return false;
      }
      if (null===data) {
        return false;
      }
      if ('object'!==typeof data) {
        return false;
      }
      const keys=Object.keys(data);
      return keys.indexOf('sender') !== -1 && keys.indexOf('data') !== -1;
      //return !!(data.sender && data.data);
    }
    ///                                         if not wrapped return given
    if (!isWrapper(data)) {
      return data;
    }
    ///                                         dig till we find actual data
    let data1=data.data;
    while (isWrapper(data1)) {
      data1=data1.data;
    }
    ///                                         return found
    return data1;
  }

  /**
   * <h4>`Dispatch.listHear` public static command</h4>
   * Create code to listen to class pubs
   * <br/><br/>
   * @public
   */
  static listHear(that) {
    ///                                         bail if not initialized
    if (!that.__pubs) {
      return;
    }
    ///                                         get captured data
    const itm=JSON.parse(that.__pubs);
    const itms=[];
    ///                                         string util
    function hear(n, v) {
      return `${n}.hear((msg, data) => {}); // ${v}`;
    }
    ///                                         recursive fn
    function addIt(o, n) {
      for(let i in o) {
        if(!o.hasOwnProperty(i)) {
          continue;
        }
        const v=o[i];
        const t=typeof v;
        if('object'===t) {
          if(null !== v) {
            addIt(v, `${n}.${i}`);
          }
        } else if('string'===t) {
          itms.push(hear(`${n}.${i}`, v));
        }
      }
    }
    ///                                         process object
    addIt(itm, that.name);
    ///                                         log result codes
    console.log(itms.join('\n'));
  }

  /**
   * <h4>`Dispatch.constructor` public constructor</h4>
   * Set `.msg`, `._what`, init `.__log`
   * <br/><br/>
   * @public
   * @param {string} msg - dot msg for {@link PubSub.publish}, {@link PubSub.publishSync}
   * @param {string} what - documentation for this message
   */
  constructor(msg, what) {
    this.msg=msg;
    this._what=what;
    this.__log=[];
  }

  /**
   * <h4>`Dispatch._log` private command</h4>
   * Add new item to log with date, trim logs
   * <br/><br/>
   * @protected
   * @param {*} data
   * @param {string} extra - additional topic
   * @param {string} verb - how it was dispatched (say || yell)
   */
  _log(data, extra, verb) {
    const date= new Date();
    this.__log.push({ data, extra, verb, date });
    //noinspection MagicNumberJS
    if(1000 < this.__log.length) {
      this.__log.shift();
    }
  }

  /**
   * <h4>`Dispatch#what` public readonly property</h4>
   * Returns documentation string from _what
   * <br/><br/>
   * @public
   * @return {string}
   */
  get what() {
    return this._what;
  }

  /**
   * <h4>`Dispatch#hear` public command</h4>
   * Subscribe, add to fn.hears and return token
   * <br/><br/>
   * @public
   * @param {HearFunc} fn - (msg, data)
   * @param {string} [extra] - additional topic (*null)
   * @return {string} token
   */
  hear(fn, extra) {
    const msg=`${this.msg}${extra ? '.' + extra : ''}`;
    if(!fn.hears) {
      fn.hears={};
    }
    if(!fn.hears[this.msg]) {
      fn.hears[this.msg]=[];
    }
    const token=PubSub.subscribe(msg, fn);
    fn.hears[this.msg].push({ msg:this.msg, extra, token });
    return token;
  }

  /**
   * <h4>`Dispatch#say` public command</h4>
   * Async publish data with extra topic, log it
   * <br/><br/>
   * @public
   * @param {*} data
   * @param {string} [extra]
   */
  say(data, extra) {
    const msg=`${this.msg}${extra ? '.' + extra : ''}`;
    this._log(data, extra, 'say');
    PubSub.publish(msg, data);
  }

  /**
   * <h4>`Dispatch#yell` public command</h4>
   * Synch publish data with extra topic, log it
   * <br/><br/>
   * @public
   * @param {*} data
   * @param {string} extra
   */
  yell(data, extra) {
    const msg=`${this.msg}${extra ? '.' + extra : ''}`;
    this._log(data, extra, 'yell');
    PubSub.publishSync(msg, data);
  }

  /**
   * <h4>`Dispatch#sleep` public command</h4>
   * Unsubscribe function, keep its hears
   * <br/><br/>
   * @public
   * @param {HearFunc} fn
   */
  sleep(fn) {
    use(this);
    PubSub.unsubscribe(fn);
  }

  /**
   * <h4>`Dispatch#wake` public command</h4>
   * Use functions hears to re-subscribe
   * <br/><br/>
   * @protected
   * @param {HearFunc} fn
   */
  wake(fn) {
    const mine=fn.hears[this.msg];//array
    const a=mine.slice(0);
    fn.hears[this.msg]=[];
    for(let x=0;x<a.length;x++) {
      const o=a[x];// msg, token
      const msg=o.msg;
      const extra=o.extra;
      const token=PubSub.subscribe(msg, fn);
      fn.hears[this.msg].push({ msg, extra, token });
    }
  }

  /**
   * <h4>`Dispatch#kill` public command</h4>
   * Permanently unsubscribe funtion or token
   * <br/><br/>
   * @protected
   */
  kill(fnOrToken) {
    use(this);
    if('function'===typeof fnOrToken) {
      PubSub.unsubscribe(fnOrToken);
      delete fnOrToken.hears;
    }else{
      PubSub.unsubscribe(fnOrToken);
    }
  }

}

//export Dispatch = Dispatch;
