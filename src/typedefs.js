/**
 * <h4>``</h4>
 * <a href="aip://open/C%3A%5C%5F%5Casciiw%5Csrc%5Ctypedefs%2Ejs">C:\_\asciiw\src\typedefs.js</a><br>
 * <br/><br/>
 * <h5>NPM Dependencies</h5>
 * none
 * <br/><br/>
 * <h5>Project Dependencies</h5>
 * - {@link Utils}       - `../utils/utils`
 * <br/><br/>
 * <h5>Local Dependencies</h5>
 * <br/><br/>
 * @protected
 * @see https://bitbucket.org/mindprism/asciiw/src/master/src/typedefs.js
 */
const src_typedefs_js= "/src/typedefs.js";

///
/// Util Imports
import { use } from './utils/utils';
use(src_typedefs_js);

/**
 * <h4>`SortEnum` string enum typedef</h4>
 * (id || size || price)
 * <br/><br/>
 * @typedef {string} SortEnum
 */

/**
 * <h4>`SortBarButtonSpec` object enum typedef</h4>
 * Used by {@link SortBar}
 * <br/><br/>
 * @typedef {Object} SortBarButtonSpec
 * @property {string} id - Proper Case, becomes ref and key and lowcase data-id
 * @property {boolean} selected - controls clickable and .selected
 * @property {boolean} disabled - controls clickable and .disabled
 * @property {string} text - button text
 */

/**
 * <h4>`HearSpec` array typedef</h4>
 * function is (msg :String, data :Object) : void
 * <br/><br/>
 * @typedef {[class, HearFunc]} HearSpec
 */

/**
 * <h4>`HearFunc` function typedef</h4>
 * function is (msg :String, data :Object) : void
 * <br/><br/>
 * @typedef {function} HearFunc
 */

/**
 * <h4>`ApiRequester` object typedef</h4>
 * <br/><br/>
 * @typedef {Object} ApiRequester
 * @property {function} request  - (params={}) :Promise
 */

/**
 * <h4>`MultiSortApiFetcherConstructorOptions` object typedef</h4>
 * <br/><br/>
 * @typedef {Object} MultiSortApiFetcherConstructorOptions
 * @property {number} [fetchSize=10]
 * @property {ApiRequester} api
 * @property {string} [keyField]
 * @property {number} [requestTimeout=10000]
 */

