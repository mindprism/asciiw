/* global */
/**
 * <h4>`ItemDetails` component</h4>
 * <a href="aip://open/C%3A%5C%5F%5Casciiw%5Csrc%5Ccomponents%5CItemDetails%2Ejsx">C:\_\asciiw\src\components\ItemDetails.jsx</a><br>
 * <br/><br/>
 * Face Item Details - Id, Size, Price
 * <br/><br/>
 * <h5>NPM Dependencies</h5>
 * - {@link React}      - `react`
 * - {@link Component}  - `react`
 * - {@link PropTypes}  - `react`
 * - {@link $}          - `jquery`
 * - {@link classNames} - `classnames`
 * <br/><br/>
 * <h5>Project Dependencies</h5>
 * - {@link Utils}       - `../utils/utils`
 * - {@link Dispatch}    - `../utils/utils`
 * - {@link UiComponent} - `../react/UiComponent`
 * <br/><br/>
 * <h5>Local Dependencies</h5>
 * None
 * <br/><br/>
 * <h5>Product</h5>
 * {@link ItemDetails}
 * <br/><br/>
 * @protected
 * @see https://bitbucket.org/mindprism/asciiw/src/master/src/components/ItemDetails.jsx
 */
const src_components_ItemDetails_jsx= "/src/components/ItemDetails.jsx";

/// Base Imports
import UiComponent from '../react/UiComponent';
import React, { Component, PropTypes } from 'react';
//noinspection JSUnresolvedVariable
import $ from 'jquery';
import classNames from 'classnames';
///
/// Util Imports
import Utils from '../utils/utils';
import { use, Dispatch } from '../utils/utils';
///
/// Local Imports
import './ItemDetails.css';
///
/// Local Helpers
use($);
use(Component);
use(PropTypes);
use(Utils);
use(Dispatch);
use(src_components_ItemDetails_jsx);

/// Class
/**
 * <h4>`ItemDetails` React Component class</h4>
 * - {@link ItemDetails.displayName} - ItemDetails
 * - {@link ItemDetails.propTypes} - itemId :String.'', itemSize :Number.NaN, itemPrice :Number.NaN
 * - {@link ItemDetails.defaultProps} - itemId :String.'', itemSize :Number.NaN, itemPrice :Number.NaN
 * - {@link ItemDetails#constructor} - no action
 * - {@link ItemDetails#render} - div>(span.itemId span.itemPrice span.itemSize)
 * <br/><br/>
 * <h5>Dependencies</h5>
 * - {@link Utils}       - `../utils/utils`
 * - {@link classNames} - `classnames`
 * - {@link UiComponent} - `../react/UiComponent`
 * <br/><br/>
 * - file {@link src_components_ItemDetails_jsx}
 * @protected
 */
class ItemDetails extends UiComponent {
  //
  /**
   * <h4>`ItemDetails.displayName` static member</h4>
   *  String is used in debugging messages. JSX sets this value automatically.
   * <br/><br/>
   * @protected
   */
  static displayName = 'ItemDetails';

  /**
   * <h4>`ItemDetails.propTypes` static member</h4>
   *   Defined as a property on the component class itself, to define what types the props should be. It should be a map from prop names to types as defined in React.PropTypes. In development mode, when an invalid value is provided for a prop, a warning will be shown in the JavaScript console. In production mode, propTypes checks are skipped for efficiency.
   * <br/><br/>
   * itemId :String.'', itemSize :Number.NaN, itemPrice :Number.NaN
   * <br/><br/>
   * @type {Object}
   * @property {PropTypes} propTypes.itemId `PropTypes.string` (*'')
   * @property {PropTypes} propTypes.itemSize `PropTypes.number` (*NaN)
   * @property {PropTypes} propTypes.itemPrice `PropTypes.number` (*NaN)
   * <br/><br/>
   * @protected
   */
  static propTypes = {
    itemId: React.PropTypes.string
    , itemSize: React.PropTypes.number
    , itemPrice: React.PropTypes.number
  };

  /**
   * <h4>`ItemDetails.defaultProps` static member</h4>
   *  Defined as a property on the component class itself, to set the default props for the class. This is used for undefined props, but not for null props.
   * <br/><br/>
   * itemId :String.'', itemSize :Number.NaN, itemPrice :Number.NaN
   * <br/><br/>
   * @type {Object}
   * @property {string} defaultProps.itemId
   * @property {number} defaultProps.itemSize
   * @property {number} defaultProps.itemPrice
   * <br/><br/>
   * @protected
   */
  static defaultProps = {
    itemId: ''
    , itemSize: NaN
    , itemPrice: NaN
  };

  /**
   * <h4>`ItemDetails` constructor</h4>
   * - call `super`
   * <br/><br/>
   * - see {@link ItemDetails_React_Component_export} for docs
   * <br/><br/>
   * @protected
   * @param {Object} props
   * @param {Object} context
   */
  constructor(props, context) {
    super(props, context);
    // set state
    this.state = { // keep to avoid no-useless-constructors
    };
  }

  /// LifeCycle
    /// componentWillMount
    /// componentDidMount
    /// componentWillReceiveProps
    /// shouldComponentUpdate
    /// componentWillUpdate
    /// componentDidUpdate
    /// componentWillUnmount
  ///

  /**
   * <h4>`ItemDetails#render` function</h4>
   * div>(span.itemId span.itemPrice span.itemSize)
   * <br/><br/>
   * Watch for ungiven props
   * <br/><br/>
   * <h5>Dependencies</h5>
   * - {@link Utils}       - `../utils/utils`
   * - {@link classNames} - `classnames`
   * <br/><br/>
   * @protected
   */
  render() {
    use(this); // webstorm can be static
    const rootClasses = classNames({
      'ItemDetails': true
      , 'root-ItemDetails': true
    });
    //id,size,price,face,date
    const itemId=`${this.props.itemId ? this.props.itemId : 'N/A'}`;
    const itemSize=`${Number.isNaN(this.props.itemSize) ? 'N/A' : this.props.itemSize}`;
    const itemPrice=`${Number.isNaN(this.props.itemPrice) ? 'N/A' : Utils.simpleCurrencyFromCents(this.props.itemPrice)}`;
    return (
      <div
        className={rootClasses}
        data-react-class="ItemDetails"
        data-react-file="ItemDetails.jsx"
        >
        <span className="itemId">{itemId}</span>
        <span className="itemPrice">{itemPrice}</span>
        <span className="itemSize">{itemSize}</span>
      </div>
    );
  }
}
/**
 * <h4>`ItemDetails` React Component default export</h4>
 * <br/><br/>
 * - file {@link src_components_ItemDetails_jsx}
 * @protected
 */
const ItemDetails_React_Component_export = 'EsDocs symbol';
use(ItemDetails_React_Component_export);
export default ItemDetails;
