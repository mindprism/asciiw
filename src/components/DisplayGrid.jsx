/* global */
/**
 * <h4>`DisplayGrid` component</h4>
 * <a href="aip://open/C%3A%5C%5F%5Casciiw%5Csrc%5Ccomponents%5CDisplayGrid%2Ejsx">C:\_\asciiw\src\components\DisplayGrid.jsx</a><br>
 * <br/><br/>
 * Container for GridItems
 * <h5>NPM Dependencies</h5>
 * - {@link React}          - `react`
 * - {@link Component}      - `react`
 * - {@link PropTypes}      - `react`
 * - {@link $}              - `jquery`
 * - {@link classNames}     - `classnames`
 * - {@link Motion}         - `react-motion`
 * - {@link spring}         - `react-motion`
 * <br/><br/>
 * <h5>Project Dependencies</h5>
 * - {@link Utils}        - `../utils/utils`
 * - {@link Dispatch}     - `../utils/utils`
 * - {@link UiComponent}  - `../react/UiComponent`
 * <br/><br/>
 * <h5>Local Dependencies</h5>
 * - {@link GridItem} - `./GridItem`
 * <br/><br/>
 * <h5>Products</h5>
 * - {@link DisplayGrid} - {@link Motion}, {@link GridItem}
 * <br/><br/>
 * @protected
 * @see https://bitbucket.org/mindprism/asciiw/src/master/src/components/DisplayGrid.jsx
 */
const src_components_DisplayGrid_jsx= "/src/components/DisplayGrid.jsx";

/// Base Imports
import UiComponent from '../react/UiComponent';
import React, { Component, PropTypes } from 'react';
import { Motion, spring } from 'react-motion';
//noinspection JSUnresolvedVariable
import $ from 'jquery';
import classNames from 'classnames';
///
/// Util Imports
import Utils from '../utils/utils';
import { use, Dispatch } from '../utils/utils';
///
/// Component Imports
import GridItem  from './GridItem';
///
/// Local Imports
import './DisplayGrid.css';
///
/// Local Helpers
use($);
use(Utils);
use(Component);
use(src_components_DisplayGrid_jsx);

/// Class
/**
 * <h4>`DisplayGrid` React Component class</h4>
 * - {@link DisplayGrid.displayName} - DisplayGrid
 * - {@link DisplayGrid.propTypes} - items, bottomMargin
 * - {@link DisplayGrid.defaultProps} - items, bottomMargin
 * - {@link DisplayGrid.pubs} - items.more
 * - {@link DisplayGrid._position} - Compute item position and size, container right bottom, based on item index, cols, total width and margin
 * - {@link DisplayGrid#_previousLeftTops} - dictionary
 * - {@link DisplayGrid#constructor} - no actions
 * - {@link DisplayGrid#_checkDisplaySpace} - If window.innerHeight is greater than container bottom, ask for more items
 * - {@link DisplayGrid#componentDidMount} - Randomly animate `.itemFaceFace` loop
 * - {@link DisplayGrid#render} - div>div>([Motion>GridItem])
 * <br/><br/>
 * <h5>Dependencies</h5>
 * - {@link $}       - `jquery`
 * - {@link classNames} - `classnames`
 * - {@link Dispatch}    - `../utils/utils`
 *
 * - {@link UiComponent} - `../react/UiComponent`
 *
 * - {@link Motion}         - `react-motion`
 * - {@link spring}         - `react-motion`
 * - {@link GridItem} - `./GridItem`
 * <br/><br/>
 * - file {@link src_components_DisplayGrid_jsx}
 * @protected
 */
class DisplayGrid extends UiComponent {
  //
  /**
   * <h4>`DisplayGrid.displayName` static member</h4>
   *  String is used in debugging messages. JSX sets this value automatically.
   * <br/><br/>
   * @protected
   */
  static displayName = 'DisplayGrid';

  /**
   * <h4>`DisplayGrid.propTypes` static member</h4>
   *  Defined as a property on the component class itself, to define what types the props should be. It should be a map from prop names to types as defined in React.PropTypes. In development mode, when an invalid value is provided for a prop, a warning will be shown in the JavaScript console. In production mode, propTypes checks are skipped for efficiency.
   * <br/><br/>
   * items :Array.[], bottomMargin :Number.0
   * <br/><br/>
   * @type {Object}
   * @property {PropTypes}  propTypes.items `PropTypes.arrayOf(PropTypes.object)` (*[]) {@link Face} objects
   * @property {PropTypes}  propTypes.bottomMargin `PropTypes.number` (*0) trigger more margin
   * @protected
   */
  static propTypes = {
    items: PropTypes.arrayOf(React.PropTypes.object)
    , bottomMargin: PropTypes.number
  };

  /**
   * <h4>`DisplayGrid.defaultProps` static member</h4>
   *  Defined as a property on the component class itself, to set the default props for the class. This is used for undefined props, but not for null props.
   * <br/><br/>
   * items:Array.[], bottomMargin.0
   * <br/><br/>
   * @type {Object}
   * @property {Face[]} defaultProps.items {@link Face} (*[]) objects
   * @property {number} defaultProps.bottomMargin (*0) triggers more margin
   * <br/><br/>
   * @protected
   */
  static defaultProps = {
    items:[]
    , bottomMargin: 0
  };

  /**
   * <h4>`DisplayGrid.pubs` static object</h4>
   * {@link PubSub} {@link Dispatch} Interface
   * - `items.more` - request items, no data
   * <br/><br/>
   * See {@link AppDisplay.pubs} for documentation
   * <br/><br/>
   * @type {Object}
   * @property {Dispatch} items.more - request items, no data
   * @public
   */
  static pubs = {
    items: {
      more: 'request items, no data'
    }
  };

  /**
   * <h4>`DisplayGrid._position` public static utility</h4>
   * Compute item position and size, container right bottom, based on item index, cols, total width and margin
   * <br/><br/>
   * @protected
   */
  static _position(idx, cols, total_width, margin) {
    //                                          margin total, item size
    const mt = margin * (cols + 1);
    const width = (total_width - mt) / cols;
    //noinspection MagicNumberJS
    const height = width * 0.6;
    //                                          item row, col, left, top
    const row = Math.floor(idx / cols);
    const col = idx % cols;
    const ll = (col * width + ((col + 1) * margin));
    const tt = (row * height + ((row + 1) * margin));
    //                                          container right, bottom
    const bottom = tt + height + margin;
    const right=ll + width + margin;
    //                                          item xy width height, container right bottom
    return { x:ll, y:tt, width, height, right, bottom };
  }

  /**
   * <h4>`DisplayGrid#_previousLeftTops` private dictionary</h4>
   * Storage for `{left,top}` keyed on {@link Face#face}
   * <br/><br/>
   * @protected
   */
  _previousLeftTops={
  };

  /**
   * <h4>`DisplayGrid` constructor</h4>
   * - call `super`
   * <br/><br/>
   * - see {@link DisplayGrid_React_Component_export} for docs
   * <br/><br/>
   * @protected
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    // set state
    this.state = { // keep to avoid no-useless-constructors
    };
  }

  /**
   * <h4>`DisplayGrid#_checkDisplaySpace` private command</h4>
   * If window.innerHeight is greater than container bottom, ask for more items
   * <br/><br/>
   * @protected
   */
  _checkDisplaySpace() {
    const wh=window.innerHeight;
    const br=this.node.getBoundingClientRect();
    if(br.bottom<wh) {
      this.say(this.pubs.items.more, null);
    }
  }

  /// componentWillMount

  /**
   * <h4>`DisplayGrid#componentDidMount` event</h4>
   * Randomly animate `.itemFaceFace` loop
   * <br/><br/>
   * @protected
   */
  componentDidMount() {
    super.componentDidMount();
    /// Randomly animate loop
    Utils.jQueryExtendAnimateCss($);
    const a='bounceIn bounceInDown bounceInLeft bounceInRight bounceInUp bounceOut bounceOutDown bounceOutLeft bounceOutRight bounceOutUp fadeIn fadeInDown fadeInDownBig fadeInLeft fadeInLeftBig fadeInRight fadeInRightBig fadeInUp fadeInUpBig fadeOut fadeOutDown fadeOutDownBig fadeOutLeft fadeOutLeftBig fadeOutRight fadeOutRightBig fadeOutUp fadeOutUpBig flipInX flipInY flipOutX flipOutY lightSpeedIn lightSpeedOut rotateIn rotateInDownLeft rotateInDownRight rotateInUpLeft rotateInUpRight rotateOut rotateOutDownLeft rotateOutDownRight rotateOutUpLeft rotateOutUpRight hinge rollIn rollOut zoomIn zoomInDown zoomInLeft zoomInRight zoomInUp zoomOut zoomOutDown zoomOutLeft zoomOutRight zoomOutUp slideInDown slideInLeft slideInRight slideInUp slideOutDown slideOutLeft slideOutRight slideOutUp'.split(' ');
    const b='bounce flash pulse rubberBand shake headShake swing tada wobble jello'.split(' ');
    //noinspection MagicNumberJS
    const f=0.25;// higher is more often
    function doanim() {
      $('.itemFaceFace').each(function(i) {
        use(i);
        const me=$(this);
        const r=Math.random();
        //console.log(i);
        //noinspection MagicNumberJS
        if(r<0.2*f) {
          //noinspection JSUnresolvedFunction
          me.find('.Face').animateCss(a[Math.floor(Math.random()*a.length)]);
        }
        //noinspection MagicNumberJS
        if(r<0.75*f) {
          //noinspection JSUnresolvedFunction
          me.find('.FacePart').animateCss(b[Math.floor(Math.random()*b.length)]);
        }
      });
      //noinspection MagicNumberJS
      setTimeout(doanim, Math.floor(Math.random()*2000));
    }
    doanim();
  }

  /// LifeCycle
    /// componentWillReceiveProps
    /// shouldComponentUpdate
    /// componentWillUpdate
    /// componentDidUpdate
    /// componentWillUnmount
  ///

  /**
   * <h4>`DisplayGrid#render` function</h4>
   * div>div>([Motion>GridItem])
   * <br/><br/>
   * Item size is fixed currently to match css size of face
   * <br/><br/>
   * <h5>Dependencies</h5>
   * - {@link classNames} - `classnames`
   * - {@link Motion}         - `react-motion`
   * - {@link spring}         - `react-motion`
   * - {@link GridItem} - `./GridItem`
   * @protected
   */
  render() {
    use(this); // webstorm can be static
    const rootClasses = classNames({
      'DisplayGrid': true
      , 'root-DisplayGrid': true
    });
    ///                                     settings - maybe move to props
    //noinspection MagicNumberJS
    const item_width=300;
    //noinspection MagicNumberJS
    const item_margins=20; //
    //noinspection MagicNumberJS
    const container_width_reduction=120;
    ///                                     derivatives
    const container_width=window.innerWidth-container_width_reduction;
    const cols_maybe_zero=Math.floor(container_width / item_width);
    const cols=1 > cols_maybe_zero?1:cols_maybe_zero;
    ///                                     moving bottom
    let bottom=0;
    ///                                     buid Motion-ized GridItem
    const items=this.props.items.map((e, i)=>{
      const row=Math.floor(i/cols);
      const col=i-(row*cols);
      e.row=row;
      e.col=col;
      //                       _position(idx, cols, total_width, margin)
      const p=this.constructor._position(i, cols, container_width, item_margins);
      bottom=p.bottom;
      ///                                         get previous left top utility
      const previous = (face) => {
        if(this._previousLeftTops[face]) {return this._previousLeftTops[face];}
        return { left:0, top:0 };
      };
      ///                                         set previous left top utility
      const savePrev = (face, p) => {
        this._previousLeftTops[face]={ left:p.x, top:p.y };
      };
      ///                                         get/save previous
      const prev=previous(e.face);
      savePrev(e.face, p);
      ///                                         build item
      return (
        <Motion
          key={e.face}
          defaultStyle={prev}
          style={{
            left:spring(p.x)
            , top:spring(p.y)
            , width:p.width
            , height:p.height
          }}
          onRest={this._checkDisplaySpace.bind(this)}
          >
          {interpolatingStyle => <GridItem item={e} style={interpolatingStyle}/> }
        </Motion>
      );
    });
    ///                                     style for root
    const style = {
      width:`${container_width}px`
      , minWidth:`${container_width}px`
      , maxWidth:`${container_width}px`
      , height:`${bottom}px`
      , minHeight:`${bottom}px`
      , maxHeight:`${bottom}px`
      , textAlign:'left'
    };
    ///                                     relative div is needed
    return (
      <div
        className={rootClasses}
        style={style}
        data-react-class="DisplayGrid"
        data-react-file="DisplayGrid.jsx"
        >
        <div style={{ position:'relative' }}>
          {items}
        </div>
      </div>
    );
  }
}
Dispatch.initPubs(DisplayGrid);
/**
 * <h4>`DisplayGrid` React Component default export</h4>
 * <br/><br/>
 * - file {@link src_components_DisplayGrid_jsx}
 * @protected
 */
const DisplayGrid_React_Component_export = 'EsDocs symbol';
use(DisplayGrid_React_Component_export);
export default DisplayGrid;
