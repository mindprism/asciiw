## RMF-src-components

`C:\_\asciiw\src\components`

- `FmColorName` - `cyan`
- `ColorHex` - `#78C9FC`
- `ColorRgb` - `120,201,252`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-cyan-48.png)](aip://open/C%3A%5C_%5Casciiw%5Csrc%5Ccomponents) _React Components_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-cyan-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5Csrc%5CREADME.md) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Ccomponents%5CDesktop.ini) - ini

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprism/asciiw/src/master/src/components/) - bitbucket

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_jsx-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Ccomponents%5CAdPic.jsx) `AdPic.jsx`
- [![](http://mrobbinsassoc.com/images/icons/md/_jsx-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Ccomponents%5CAppDisplay.jsx) `AppDisplay.jsx` - Wrapper
- [![](http://mrobbinsassoc.com/images/icons/md/_scss-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Ccomponents%5CAppDisplay.scss) `AppDisplay.scss`
- [![](http://mrobbinsassoc.com/images/icons/md/_jsx-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Ccomponents%5CDisplayGrid.jsx) `DisplayGrid.jsx` - Grid for GridItems
- [![](http://mrobbinsassoc.com/images/icons/md/_scss-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Ccomponents%5CDisplayGrid.scss) `DisplayGrid.scss`
- [![](http://mrobbinsassoc.com/images/icons/md/_jsx-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Ccomponents%5CGridItem.jsx) `GridItem.jsx` - Container for ItemFace and ItemDetails
- [![](http://mrobbinsassoc.com/images/icons/md/_scss-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Ccomponents%5CGridItem.scss) `GridItem.scss`
- [![](http://mrobbinsassoc.com/images/icons/md/_jsx-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Ccomponents%5CItemDetails.jsx) `ItemDetails.jsx` - Id, Size, Price
- [![](http://mrobbinsassoc.com/images/icons/md/_scss-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Ccomponents%5CItemDetails.scss) `ItemDetails.scss`
- [![](http://mrobbinsassoc.com/images/icons/md/_jsx-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Ccomponents%5CItemFace.jsx) `ItemFace.jsx` - Wrapper for Face
- [![](http://mrobbinsassoc.com/images/icons/md/_scss-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Ccomponents%5CItemFace.scss) `ItemFace.scss`
- [![](http://mrobbinsassoc.com/images/icons/md/_jsx-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Ccomponents%5CLoadMoreBar.jsx) `LoadMoreBar.jsx`
- [![](http://mrobbinsassoc.com/images/icons/md/_scss-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Ccomponents%5CLoadMoreBar.scss) `LoadMoreBar.scss`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Ccomponents%5CREADME.html) `README.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_jsx-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Ccomponents%5CSortBar.jsx) `SortBar.jsx` - Id, Size, Price for AppDisplay
- [![](http://mrobbinsassoc.com/images/icons/md/_scss-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Ccomponents%5CSortBar.scss) `SortBar.scss`

