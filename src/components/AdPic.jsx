/* globals */
/**
 * <h4>`AdPic`</h4>
 * <a href="aip://open/C%3A%5C%5F%5Casciiw%5Csrc%5Ccomponents%5CAdPic%2Ejsx">C:\_\asciiw\src\components\AdPic.jsx</a><br>
 * <p>
 * <h5>NPM Dependencies</h5>
 * - {@link React} - `react`
 * - {@link ReactDOM} - `react-dom`
 * - {@link $} `jquery`
 * - {@link classNames} - `classnames`
 * <p>
 * <h5>Project Dependencies</h5>
 * - {@link Utils} - `../utils/utils`
 * <p>
 * <h5>Local Dependencies</h5>
 * lll
 * <p>
 * @protected
 * @see https://bitbucket.org/mindprism/asciiw/src/master/src/components/AdPic.jsx
 */
const src_components_AdPic_jsx= "/src/components/AdPic.jsx";

/// Base Imports
import UiComponent from '../react/UiComponent';
import React from 'react';
//noinspection JSUnresolvedVariable
import $ from 'jquery';
import classNames from 'classnames';
///
/// Util Imports
import Utils from '../utils/utils';
import { use } from '../utils/utils';
///
/// Service Imports
///
/// Component Imports
///
/// Local Helpers
use($);
use(classNames);
use(Utils);
use(src_components_AdPic_jsx);

/// Class
/**
 * <h4>`AdPic` React Component default export</h4>
 * - {@link AdPic#constructor} -
 * - {@link AdPic#componentWillMount} -
 * - {@link AdPic#componentDidMount} -
 * - {@link AdPic#componentWillReceiveProps} -
 * - {@link AdPic#shouldComponentUpdate} -
 * - {@link AdPic#componentWillUpdate} -
 * - {@link AdPic#componentDidUpdate} -
 * - {@link AdPic#componentWillUnmount} -
 * - {@link AdPic#render} -
 * <p>
 * <h5>Dependencies</h5>
 * - {@link React}   - `react`
 * - {@link ReactDOM}   - `react-dom`
 * - {@link $}       - `jquery`
 * <p>
 * - file {@link src_components_AdPic_jsx}
 * @protected
 */
class AdPic extends UiComponent {
  /**
   * <h4>`AdPic.displayName` static member</h4>
   *  String is used in debugging messages. JSX sets this value automatically.
   * <p>
   * @protected
   */
  static displayName = 'AdPic';
  /**
   * <h4>`AdPic.propTypes` static member</h4>
   *   Defined as a property on the component class itself, to define what types the props should be. It should be a map from prop names to types as defined in React.PropTypes. In development mode, when an invalid value is provided for a prop, a warning will be shown in the JavaScript console. In production mode, propTypes checks are skipped for efficiency.
   * <p>
   * Summary!!
   * <p>
   * @protected
   */
  static propTypes = {
    width: React.PropTypes.string
    , height: React.PropTypes.string
    , href: React.PropTypes.string
  };
  /**
   * <h4>`AdPic.defaultProfile` static member</h4>
   *  Defined as a property on the component class itself, to set the default props for the class. This is used for undefined props, but not for null props.
   * <p>
   * Summary!!
   * <p>
   * @protected
   */
  static defaultProps = {
    width: '320px'
    , height: '200px'
  };

  /**
   * <h4>`AdPic` constructor</h4>
   * call `super`
   * - see {@link AdPic_React_Component_export} for docs
   * <p>
   * @protected
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    // set state
    //const d = new Date().getTime();
    //noinspection MagicNumberJS
    const n = Math.random() * 1000;
    const v = Math.floor(n);
    this.state = {
      url: `/ad/?r=${v}`
    };
  }

  handleClick = (e) => {
    use(this); // webstorm can be static
    use(e);
    if (this.props.href) {
      window.open(this.props.href, '_blank');
    }
  };

  /// componentWillMount

  /// componentDidMount

  /// componentWillReceiveProps

  /// shouldComponentUpdate

  /// componentWillUpdate

  /// componentDidUpdate

  /// componentWillUnmount

  /**
   * <h4>`AdPic.render` function</h4>
   * <p>
   * Summary!!
   * <p>
   * <h5>Dependencies</h5>
   * lll
   * @protected
   */
  render() {
    use(this); // webstorm can be static
    const rootClasses = classNames({
      'AdPic': true
      , 'root-AdPic': true
    });
    const style = {
      backgroundImage: `url(${this.state.url})`
      , width:this.props.width
      , height:this.props.height
      , backgroundSize:'cover'
      , backgroundRepeat:'no-repeat'
      , backgroundPosition:'center center'
      , cursor: this.props.href?'pointer':'default'
    };
    const cleanProps = Object.assign({}, this.props);
    delete cleanProps.width;
    delete cleanProps.height;
    const props=Object.assign({}, cleanProps);
    return (
      <div className={rootClasses} onClick={this.handleClick} title={this.state.url} data-react-class="AdPic" data-react-file="AdPic.jsx" {...props} style={style}>
      </div>
    );
  }
}
/**
 * <h4>`AdPic` React Component default export</h4>
 * <p>
 * - file {@link src_components_AdPic_jsx}
 * @protected
 */
const AdPic_React_Component_export = 'EsDocs symbol';
use(AdPic_React_Component_export);
export default AdPic;
