/* global */
/**
 * <h4>`AppDisplay` wrapper component</h4>
 * <a href="aip://open/C%3A%5C%5F%5Casciiw%5Csrc%5Ccomponents%5CAppDisplay%2Ejsx">C:\_\asciiw\src\components\AppDisplay.jsx</a><br>
 * <br/><br/>
 * Main Wrapper
 * <h5>NPM Dependencies</h5>
 * - {@link React}      - `react`
 * - {@link Component}  - `react`
 * - {@link PropTypes}  - `react`
 * - {@link $}          - `jquery`
 * - {@link classNames} - `classnames`
 * <br/><br/>
 * <h5>Project Dependencies</h5>
 * - {@link Utils}       - `../utils/utils`
 * - {@link Dispatch}    - `../utils/utils`
 * - {@link UiComponent} - `../react/UiComponent`
 * <br/><br/>
 * <h5>Local Dependencies</h5>
 * - {@link DisplayGrid} - `./DisplayGrid`
 * - {@link SortBar}     - `./SortBar`
 * - {@link LoadMoreBar} - `./LoadMoreBar`
 * - {@link SortBarButtonSpec}
 * <br/><br/>
 * <h5>Products</h5>
 * - {@link AppDisplay} - {@link SortBar}, {@link DisplayGrid}, {@link LoadMoreBar}
 * <br/><br/>
 * @protected
 * @see https://bitbucket.org/mindprism/asciiw/src/master/src/components/AppDisplay.jsx
 */
const src_components_AppDisplay_jsx= "/src/components/AppDisplay.jsx";

/// Base Imports
import UiComponent from '../react/UiComponent';
import React, { Component, PropTypes } from 'react';
//noinspection JSUnresolvedVariable
import $ from 'jquery';
import classNames from 'classnames';
///
/// Util Imports
import Utils from '../utils/utils';
import { use, Dispatch } from '../utils/utils';
///
/// Component Imports
import DisplayGrid from './DisplayGrid';
import SortBar from './SortBar';
import LoadMoreBar from './LoadMoreBar';
///
/// Local Imports
import './AppDisplay.css';
///
/// Local Helpers
use($);
use(Utils);
use(Component);
use(src_components_AppDisplay_jsx);

/// Class
/**
 * <h4>`AppDisplay` React Component class</h4>
 * {@link SortBar}, {@link DisplayGrid}, {@link LoadMoreBar}
 * <br/><br/>
 * <h5>Members</h5>
 * - {@link AppDisplay.displayName}  - AppDisplay
 * - {@link AppDisplay.propTypes}    - items, sort, fetching, end
 * - {@link AppDisplay.defaultProps} - items, sort, fetching, end
 * - {@link AppDisplay.pubs}         - sort.change, items.more
 * - {@link AppDisplay.hear}         - SortBar.change, DisplayGrid.items.more, LoadMoreBar.click
 * - {@link AppDisplay#constructor}  - no actions
 * - {@link AppDisplay#render}       - SortBar, DisplayGrid, LoadMoreBar
 * <br/><br/>
 * <h5>Dependencies</h5>
 * - {@link $}       - `jquery`
 * - {@link classNames}  - `classnames`
 * - {@link Dispatch}    - `../utils/utils`
 *
 * - {@link UiComponent} - `../react/UiComponent`
 *
 * - {@link DisplayGrid} - `./DisplayGrid`
 * - {@link SortBar}     - `./SortBar`
 * - {@link LoadMoreBar} - `./LoadMoreBar`
 *
 * - {@link SortBarButtonSpec}
 * <br/><br/>
 * - file {@link src_components_AppDisplay_jsx}
 * @protected
 */
class AppDisplay extends UiComponent {
  //
  /**
   * <h4>`AppDisplay.displayName` static member</h4>
   *  String is used in debugging messages. JSX sets this value automatically.
   * <br/><br/>
   * @public
   */
  static displayName = 'AppDisplay';

  /**
   * <h4>`AppDisplay.propTypes` static member</h4>
   *   Defined as a property on the component class itself, to define what types the props should be. It should be a map from prop names to types as defined in React.PropTypes. In development mode, when an invalid value is provided for a prop, a warning will be shown in the JavaScript console. In production mode, propTypes checks are skipped for efficiency.
   * <br/><br/>
   * items :Array.[], sort :String.id, fetching :Boolean.false, end :Boolean.false
   * <br/><br/>
   * @type {Object}
   * @property {PropTypes}  propTypes.items `PropTypes.arrayOf(PropTypes.object)` (*[]) {@link Face} objects
   * @property {PropTypes}  propTypes.sort `PropTypes.string` {@link SortEnum} (*id || size|| price) for {@link SortBar}
   * @property {PropTypes}  propTypes.fetching `PropTypes.bool` (true || *false) for {@link LoadMoreBar} getting records
   * @property {PropTypes}  propTypes.end `PropTypes.bool` (true || *false) for {@link LoadMoreBar} no more records
   * <br/><br/>
   * @protected
   */
  static propTypes = {
    items: PropTypes.arrayOf(PropTypes.object)
    , sort: PropTypes.string
    , fetching: PropTypes.bool
    , end: PropTypes.bool
  };

  /**
   * <h4>`AppDisplay.defaultProps` static member</h4>
   *  Defined as a property on the component class itself, to set the default props for the class. This is used for undefined props, but not for null props.
   * <br/><br/>
   * items:Array.[], sort :String.id, fetching :Boolean.false, end :Boolean.false
   * <br/><br/>
   * @type {Object}
   * @property {Face[]} defaultProps.items {@link Face} objects
   * @property {string} defaultProps.sort {@link SortEnum} *id
   * @property {boolean} defaultProps.fetching *false
   * @property {boolean} defaultProps.end *false
   * <br/><br/>
   * @protected
   */
  static defaultProps = {
    items: []
    , sort: 'id'
    , fetching: false
    , end: false
  };

  /**
   * <h4>`AppDisplay#hear` static object</h4>
   * {@link PubSub} Subscribe Interface
   * - `Sortbar.change`
   * - `DisplayGrid.items.more`
   * - `LoadMoreBar.items.more`
   * <br/><br/>
   * <br/><br/>
   * <h5>Usage</h5>
   * First see {@link AppDisplay.pubs} to see how PubSub messages are produced.
   * <br/><br/>
   * This object gets processed and initialized in {@link BaseComponent#constructor}, using its keys to produce {@link PubSub} subscriptions.
   * The item format is `[< class with pubs >.< object path >.msg]: [< class with pubs >, function(msg, data)]` - note `function` must be used, not arrow functions.
   * The `function` is bound to `this` during initialization of each class instance. The `this` inside of `function` is therefore the class instance, not the static class.
   * <br/><br/>
   * Inside the `function` data may be 'raw' or wrapped when it is sent by `this.say` ({@link BaseComponent#say}).
   * It is ok to simply pass the data along though `this.say()` and it will get further wrapped.
   * If you wish to unwrap before passing it along, or to get at the actual data, use `Dispatch.dataOf(data)`.
   * <br/><br/>
   * See {@link BaseComponent#say} to see how data is wrapped when using `this.say()`.
   * Basically `say(< dispatch >, data, extra)` will produce a data object with a `sender` member equal to `this` and `data` member equal to `data`.
   * In this way, senders can be traced. Also, messages are dispatched with extra info using `this.say`, see {@link BaseComponent#say}
   * <br/><br/>
   * @type {Object}
   * @property {HearSpec} SortBar.change -- say sort.change
   * @property {HearSpec} DisplayGrid.items.more -- say items.more
   * @property {HearSpec} LoadMoreBar.items.more -- say items.more
   * @protected
   */
  static hear = {
    [SortBar.pubs.change.msg]: [SortBar, function (msg, data) {
      this.say(this.pubs.sort.change, data);
    }]
    , [DisplayGrid.pubs.items.more.msg]: [DisplayGrid, function (msg, data) {
      this.say(this.pubs.items.more, data);
    }]
    , [LoadMoreBar.pubs.click.msg]: [LoadMoreBar, function (msg, data) {
      this.say(this.pubs.items.more, data);
    }]
  };

  /**
   * <h4>`AppDisplay#pubs` static object</h4>
   * {@link PubSub} {@link Dispatch} Interface
   * - `sort.change` - notification, new sort field: id, size, price
   * - `items.more` - request items, no data
   * <br/><br/>
   * <h5>Goals</h5>
   * - Avoid passing down callback functions as attributes though nested React components.
   * - Provide loose coupling of classes through a PubSub system that is declaritive.
   * - Avoid use of strings to create messages.
   * - Provide top-level static class object in order to visibly or programatically query the message system of a particular class.
   * <br/><br/>
   * <h5>Usage</h5>
   * <br/><br/>
   *  The core PubSub 'publish' signature includes a 'topic' to which subscribers subscribe.
   *  The object paths in `.pubs` produce these signatures.
   * <br/><br/>
   *  Any object path in `.pubs` ending in a string (ie, the publication description) will be replaced by a new {@link Dispatch} instance after running `Dispatch.initPubs(AppDisplay)` - ie `Dispatch.initPubs(< class with static pubs object >)`
   *  The description string is an arbritrary description and will be internalized in the new Dispatch instance and available on the Dispatch instance as `.what()`
   * <br/><br/>
   *  After `.pubs` is initialized with {@link Dispatch} instances, it is possible to publish a message via `.pubs.< path >.say(data)` or `.yell(data)`, the latter being a synch version.
   *  It is more recommended to use the helper function (`.say` {@link BaseComponent#say}) available in classes derived from {@link BaseComponent}.
   * <br/><br/>
   *  The method `.say` will automate packaging of `sender` (`this`) into `data` of message and also use `extra` and other info about `this` to be appended to the topic.
   * <br/><br/>
   *  Such a call (inside an instance) looks like : `this.say(this.pubs.< path >, data, "extra");`
   *  Example: `this.say(this.pubs.items.more, data);`
   *  If your instance is an `AppDisplay` then the actual topic would be, `AppDisplay.items.more.extra.< signIt >` see {@link BaseComponent#signIt}
   * <br/><br/>
   *  See {@link AppDisplay.hear} for how to subscribe to messages.
   * <br/><br/>
   * <h5>To Do</h5>
   * This is a work in progress, there may be glitches or architectual problems or voids
   * Currently, there is no way to have a message object path that is not a leaf.
   * For instance, if you have `sort.change` then you cannot have `sort`
   * This will likely be remedied by using `_` as a key that uses its parent as the actual message object path.
   * <br/><br/>
   * @type {Object}
   * @property {Dispatch} sort.change - notification, new sort field: id, size, price
   * @property {Dispatch} items.more - request items, no data
   * @public
   */
  static pubs = {
    sort:{
      change:'notification, new sort field: id, size, price'
    }
    , items:{
      more: 'request items, no data'
    }
  };

  /**
   * <h4>`AppDisplay` constructor</h4>
   * - call `super`
   * <br/><br/>
   * - see {@link AppDisplay_React_Component_export} for docs
   * - see {@link src_components_AppDisplay_jsx} for file docs
   * <br/><br/>
   * @protected
   * @param {Object} props
   * @param {Object} context
   */
  constructor(props, context) {
    super(props, context);
    // set state
    this.state = { // keep to avoid no-useless-constructors
    };
  }

  /// LifeCycle
    /// componentWillMount
    /// componentDidMount
    /// componentWillReceiveProps
    /// shouldComponentUpdate
    /// componentWillUpdate
    /// componentDidUpdate
    /// componentWillUnmount
  ///

  /**
   * <h4>`AppDisplay#render` function</h4>
   * div>(SortBar DisplayGrid LoadMoreBar)
   * <br/><br/>
   * Watch proper case on {@link SortEnum}
   * <br/><br/>
   * <h5>Dependencies</h5>
   * - {@link classNames} - `classnames`
   * - {@link DisplayGrid} - `./DisplayGrid`
   * - {@link SortBar}     - `./SortBar`
   * - {@link LoadMoreBar} - `./LoadMoreBar`
   * @protected
   */
  render() {
    use(this); // webstorm can be static
    const rootClasses = classNames({
      'AppDisplay': true
      , 'root-AppDisplay': true
    });
    ///                                         create SortBarButtonSpecs
    const sorts=[
      { id:'Id', text:'Id' }
      , { id:'Size', text:'Size' }
      , { id:'Price', text:'Price' }
    ].map((e, i)=>{
      use(i);
      if (e.id.toLowerCase()===this.props.sort) {
        e.selected=true;
      }
      return e;
    });
    ///                                         compute LoadMoreButton text
    //noinspection NestedConditionalExpressionJS
    const more_text=this.props.end
      ?"No more items"
      :(this.props.fetching
        ?"Loading..."
        :"Click to load more"
      );
    return (
      <div
        className={rootClasses}
        data-react-class="AppDisplay"
        data-react-file="AppDisplay.jsx"
        >
        <SortBar
          items={sorts}
          />
        <DisplayGrid
          items={this.props.items}
          />
        <LoadMoreBar
          loading={this.props.fetching}
          text={more_text}
          clickable={!(this.props.fetching||this.props.end)}
          end={this.props.end}
          />
      </div>
    );
  }
}
Dispatch.initPubs(AppDisplay);
/**
 * <h4>`AppDisplay` React Component default export</h4>
 * <br/><br/>
 * - file {@link src_components_AppDisplay_jsx}
 * @protected
 */
const AppDisplay_React_Component_export = 'EsDocs symbol';
use(AppDisplay_React_Component_export);
export default AppDisplay;
