/* global */
/**
 * <h4>`GridItem` component</h4>
 * <a href="aip://open/C%3A%5C%5F%5Casciiw%5Csrc%5Ccomponents%5CGridItem%2Ejsx">C:\_\asciiw\src\components\GridItem.jsx</a><br>
 * <br/><br/>
 * Container for ItemFace and ItemDetails
 * <h5>NPM Dependencies</h5>
 * - {@link React}      - `react`
 * - {@link Component}  - `react`
 * - {@link PropTypes}  - `react`
 * - {@link $}          - `jquery`
 * - {@link classNames} - `classnames`
 * <br/><br/>
 * <h5>Project Dependencies</h5>
 * - {@link Utils}       - `../utils/utils`
 * - {@link Dispatch}    - `../utils/utils`
 * - {@link UiComponent} - `../react/UiComponent`
 * <br/><br/>
 * <h5>Local Dependencies</h5>
 * - {@link ItemFace}    - `./ItemFace`
 * - {@link ItemDetails} - `./ItemDetails`
 * <br/><br/>
 * <h5>Product</h5>
 * - {@link GridItem} - {@link ItemFace}, {@link ItemDetails}
 * @protected
 * @see https://bitbucket.org/mindprism/asciiw/src/master/src/components/GridItem.jsx
 */
const src_components_GridItem_jsx= "/src/components/GridItem.jsx";

/// Base Imports
import UiComponent from '../react/UiComponent';
import React, { Component, PropTypes } from 'react';
//noinspection JSUnresolvedVariable
import $ from 'jquery';
import classNames from 'classnames';
///
/// Util Imports
import Utils from '../utils/utils';
import { use } from '../utils/utils';
///
/// Component Imports
import ItemFace from './ItemFace';
import ItemDetails from './ItemDetails';
///
/// Local Imports
import './GridItem.css';
///
/// Local Helpers
use($);
use(Utils);
use(Component);
use(PropTypes);
use(src_components_GridItem_jsx);

/// Class
/**
 * <h4>`GridItem` React Component class</h4>
 * - {@link GridItem.displayName} - GridItem
 * - {@link GridItem.propTypes} - item :Face
 * - {@link GridItem.defaultProps} - none
 * - {@link GridItem#constructor} - no actions
 * - {@link GridItem#render} - div>(ItemFace ItemDetails)
 * <br/><br/>
 * <h5>Dependencies</h5>
 * - {@link UiComponent} - `../react/UiComponent`
 *
 * - {@link ItemFace}    - `./ItemFace`
 * - {@link ItemDetails} - `./ItemDetails`
 * <br/><br/>
 * - file {@link src_components_GridItem_jsx}
 * @protected
 */
class GridItem extends UiComponent {
  //
  /**
   * <h4>`GridItem.displayName` static member</h4>
   *  String is used in debugging messages. JSX sets this value automatically.
   * <br/><br/>
   * @protected
   */
  static displayName = 'GridItem';

  /**
   * <h4>`GridItem.propTypes` static member</h4>
   *   Defined as a property on the component class itself, to define what types the props should be. It should be a map from prop names to types as defined in React.PropTypes. In development mode, when an invalid value is provided for a prop, a warning will be shown in the JavaScript console. In production mode, propTypes checks are skipped for efficiency.
   * <br/><br/>
   * item :Face
   * <br/><br/>
   * @type {Object}
   * @property {PropTypes} propTypes.items `PropTypes.object.required` (!) {@link Face} object
   * <br/><br/>
   * @protected
   */
  static propTypes = {
    item: React.PropTypes.object
  };

  /**
   * <h4>`GridItem.defaultProps` static member</h4>
   *  Defined as a property on the component class itself, to set the default props for the class. This is used for undefined props, but not for null props.
   * <br/><br/>
   * None
   * <br/><br/>
   * @type {Object}
   * <br/><br/>
   * @protected
   */
  static defaultProps = {};

  /**
   * <h4>`GridItem` constructor</h4>
   * - call `super`
   * <br/><br/>
   * - see {@link GridItem_React_Component_export} for docs
   * <br/><br/>
   * @protected
   * @param {Object} props
   * @param {Object} context
   */
  constructor(props, context) {
    super(props, context);
    // set state
    this.state = { // keep to avoid no-useless-constructors
    };
  }

  /// LifeCycle
    /// componentWillMount
    /// componentDidMount
    /// componentWillReceiveProps
    /// shouldComponentUpdate
    /// componentWillUpdate
    /// componentDidUpdate
    /// componentWillUnmount
  ///

  /**
   * <h4>`GridItem#render` function</h4>
   * div>(ItemFace ItemDetails)
   * <br/><br/>
   * ToDo: work on cleanProps - genericize
   * <br/><br/>
   * <h5>Dependencies</h5>
   * - {@link classNames} - `classnames`
   * - {@link ItemFace}    - `./ItemFace`
   * - {@link ItemDetails} - `./ItemDetails`
   * <br/><br/>
   * @protected
   */
  render() {
    use(this); // webstorm can be static
    const rootClasses = classNames({
      'GridItem': true
      , 'root-GridItem': true
    });
    const cl2=`${rootClasses} r${this.props.item.row}c${this.props.item.col}`;
    const cleanProps=Object.assign({}, this.props);
    delete cleanProps.item;
    //
    //id,size,price,face,date
    return (
      <div
        className={cl2}
        data-react-class="GridItem"
        data-react-file="GridItem.jsx"
        data-id={this.props.item.id}
        {...cleanProps}
        >
        <ItemFace
          itemFace={this.props.item.face}
          item={this.props.item}
          />
        <ItemDetails
          itemId={this.props.item.id}
          itemSize={this.props.item.size}
          itemPrice={this.props.item.price}
          />
      </div>
    );
  }
}
/**
 * <h4>`GridItem` React Component default export</h4>
 * <br/><br/>
 * - file {@link src_components_GridItem_jsx}
 * @protected
 */
const GridItem_React_Component_export = 'EsDocs symbol';
use(GridItem_React_Component_export);
export default GridItem;
