/* global */
/**
 * <h4>`LoadMoreBar` component</h4>
 * <a href="aip://open/C%3A%5C%5F%5Casciiw%5Csrc%5Ccomponents%5CLoadMoreBar%2Ejsx">C:\_\asciiw\src\components\LoadMoreBar.jsx</a><br>
 * <br/><br/>
 * Bar is clickable or displays loading status or no more records, all text is specifiable
 * <br/><br/>
 * <h5>NPM Dependencies</h5>
 * - {@link React}          - `react`
 * - {@link Component}      - `react`
 * - {@link PropTypes}      - `react`
 * - {@link $}              - `jquery`
 * - {@link classNames}     - `classnames`
 * <br/><br/>
 * <h5>Project Dependencies</h5>
 * - {@link Utils}        - `../utils/utils`
 * - {@link Dispatch}     - `../utils/utils`
 * - {@link UiComponent}  - `../react/UiComponent`
 * <br/><br/>
 * <h5>Local Dependencies</h5>
 * None
 * <br/><br/>
 * @protected
 * @see https://bitbucket.org/mindprism/asciiw/src/master/src/components/LoadMoreBar.jsx
 */
const src_components_LoadMoreBar_jsx= "/src/components/LoadMoreBar.jsx";

/// Base Imports
import UiComponent from '../react/UiComponent';
import React, { Component, PropTypes } from 'react';
//noinspection JSUnresolvedVariable
import $ from 'jquery';
import classNames from 'classnames';
///
/// Util Imports
import Utils from '../utils/utils';
import { use, Dispatch } from '../utils/utils';
///
/// Local Imports
import './LoadMoreBar.css';
///
/// Local Helpers
use($);
use(Utils);
use(Dispatch);
use(Component);
use(src_components_LoadMoreBar_jsx);

/// Class
/**
 * <h4>`LoadMoreBar` React Component default export</h4>
 * - {@link LoadMoreBar.displayName} - LoadMoreBar
 * - {@link LoadMoreBar.propTypes} - clickable :Boolean.true, end :Boolean.false, loading :Boolean.false, text :String.'Load more...'
 * - {@link LoadMoreBar.defaultProps} - clickable :Boolean.true, end :Boolean.false, loading :Boolean.false, text :String.'Load more...'
 * - {@link LoadMoreBar.pubs} - click
 * - {@link LoadMoreBar#constructor} - no action
 * - {@link LoadMoreBar#handleClick} - say click
 * - {@link LoadMoreBar#render} - div>button(.loading .clickable)
 * <br/><br/>
 * <h5>Dependencies</h5>
 * - {@link classNames}     - `classnames`
 * <br/><br/>
 * - file {@link src_components_LoadMoreBar_jsx}
 * @protected
 */
class LoadMoreBar extends UiComponent {
  //
  /**
   * <h4>`LoadMoreBar.displayName` static member</h4>
   *  String is used in debugging messages. JSX sets this value automatically.
   * <br/><br/>
   * @protected
   */
  static displayName = 'LoadMoreBar';

  /**
   * <h4>`LoadMoreBar.propTypes` static member</h4>
   *   Defined as a property on the component class itself, to define what types the props should be. It should be a map from prop names to types as defined in React.PropTypes. In development mode, when an invalid value is provided for a prop, a warning will be shown in the JavaScript console. In production mode, propTypes checks are skipped for efficiency.
   * <br/><br/>
   * clickable :Boolean.true, end :Boolean.false, loading :Boolean.false, text :String.'Load more...'
   * <br/><br/>
   * @type {Object}
   * @property {PropTypes} propTypes.clickable `PropTypes.bool` (*true)
   * @property {PropTypes} propTypes.end `PropTypes.bool` (*false)
   * @property {PropTypes} propTypes.loading `PropTypes.bool` (*false)
   * @property {PropTypes} propTypes.text `PropTypes.string` (*'Load more...')
   * <br/><br/>
   * @protected
   */
  static propTypes = {
    clickable: PropTypes.bool
    , end: PropTypes.bool
    , loading: PropTypes.bool
    , text: PropTypes.string
  };

  /**
   * <h4>`LoadMoreBar.defaultProps` static member</h4>
   *  Defined as a property on the component class itself, to set the default props for the class. This is used for undefined props, but not for null props.
   * <br/><br/>
   * clickable :Boolean.true, end :Boolean.false, loading :Boolean.false, text :String.'Load more...'
   * <br/><br/>
   * @type {Object}
   * @property {boolean} clickable (*true)
   * @property {boolean} end (*false)
   * @property {boolean} loading (*false)
   * @property {string} text (*'Load more...')
   * <br/><br/>
   * @protected
   */
  static defaultProps = {
    clickable: true
    , end: false
    , loading: false
    , text:'Load more...'
  };

  /**
   * <h4>`LoadMoreBar.pubs` static object</h4>
   * {@link PubSub} {@link Dispatch} Interface
   * - `click` - notification, no data
   * <br/><br/>
   * See {@link AppDisplay.pubs} for documentation
   * <br/><br/>
   * @type {Object}
   * @property {Dispatch} click - notification, no data
   * @public
   */
  static pubs = {
    click:'on click, no data'
  };

  /**
   * <h4>`LoadMoreBar` constructor</h4>
   * - call `super`
   * <br/><br/>
   * - see {@link LoadMoreBar_React_Component_export} for docs
   * <br/><br/>
   * @protected
   * @param {Object} props
   * @param {Object} context
   */
  constructor(props, context) {
    super(props, context);
    // set state
    this.state = { // keep to avoid no-useless-constructors
    };
  }

  /**
   * <h4>`LoadMoreBar#handleClick` bound handler</h4>
   * Say click
   * <br/><br/>
   * @protected
   */
  handleClick= () => {
    this.say(this.pubs.click, null);
  };

  /// LifeCycle
    /// componentWillMount
    /// componentDidMount
    /// componentWillReceiveProps
    /// shouldComponentUpdate
    /// componentWillUpdate
    /// componentDidUpdate
    /// componentWillUnmount
  ///

  /**
   * <h4>`LoadMoreBar#render` function</h4>
   * div>button(.loading .clickable)
   * <br/><br/>
   * Say clicks
   * <br/><br/>
   * <h5>Dependencies</h5>
   * - {@link classNames}     - `classnames`
   * <br/><br/>
   * @protected
   */
  render() {
    use(this); // webstorm can be static
    const rootClasses = classNames({
      'LoadMoreBar': true
      , 'root-LoadMoreBar': true
      , 'loading': this.props.loading
      , 'clickable': this.props.clickable
    });
    const buttonClasses = classNames({
      'loading': this.props.loading
      , 'clickable': this.props.clickable
    });
    return (
      <div
        className={rootClasses}
        data-react-class="LoadMoreBar"
        data-react-file="LoadMoreBar.jsx"
        >
        <button
          ref='btn'
          className={buttonClasses}
          onClick={this.handleClick}
          >
          {this.props.text}
        </button>
      </div>
    );
  }
}
Dispatch.initPubs(LoadMoreBar);
/**
 * <h4>`LoadMoreBar` React Component default export</h4>
 * <br/><br/>
 * - file {@link src_components_LoadMoreBar_jsx}
 * @protected
 */
const LoadMoreBar_React_Component_export = 'EsDocs symbol';
use(LoadMoreBar_React_Component_export);
export default LoadMoreBar;
