/* global */
/**
 * <h4>`SortBar` component</h4>
 * <a href="aip://open/C%3A%5C%5F%5Casciiw%5Csrc%5Ccomponents%5CSortBar%2Ejsx">C:\_\asciiw\src\components\SortBar.jsx</a><br>
 * <br/><br/>
 * Id, Size, Price for AppDisplay
 * <br/><br/>
 * <h5>NPM Dependencies</h5>
 * - {@link React}          - `react`
 * - {@link Component}      - `react`
 * - {@link PropTypes}      - `react`
 * - {@link $}              - `jquery`
 * - {@link classNames}     - `classnames`
 * <br/><br/>
 * <h5>Project Dependencies</h5>
 * - {@link Utils}        - `../utils/utils`
 * - {@link Dispatch}     - `../utils/utils`
 * - {@link UiComponent}  - `../react/UiComponent`
 * <br/><br/>
 * <h5>Local Dependencies</h5>
 * - {@link SortBarButtonSpec}
 * <br/><br/>
 * <h5>Produce</h5>
 * - {@link SortBar}
 * <br/><br/>
 * @protected
 * @see https://bitbucket.org/mindprism/asciiw/src/master/src/components/SortBar.jsx
 */
const src_components_SortBar_jsx= "/src/components/SortBar.jsx";

/// Base Imports
import UiComponent from '../react/UiComponent';
import React, { Component, PropTypes } from 'react';
//noinspection JSUnresolvedVariable
import $ from 'jquery';
import classNames from 'classnames';
///
/// Util Imports
import Utils from '../utils/utils';
import { use, Dispatch } from '../utils/utils';
///
/// Local Imports
import './SortBar.css'; //
///
/// Local Helpers
use($);
use(Utils);
use(Component);
use(src_components_SortBar_jsx);

/// Class
/**
 * <h4>`SortBar` React Component class</h4>
 * - {@link SortBar.displayName} - SortBar
 * - {@link SortBar.propTypes} - items :[{@link SortBarButtonSpec}] (*[])
 * - {@link SortBar.defaultProps} - items :[{@link SortBarButtonSpec}] (*[])
 * - {@link SortBar.pubs} - `change` - notification sending data-id
 * - {@link SortBar#constructor} - no action
 * - {@link SortBar#handleClick} - Say `change` with source data-id
 * - {@link SortBar#render} - div>(span [button]) -- Series of toggle buttons with 'sort by' label
 * <br/><br/>
 * <h5>Dependencies</h5>
 * - {@link classNames}     - `classnames`
 * - {@link SortBarButtonSpec}
 * <br/><br/>
 * - file {@link src_components_SortBar_jsx}
 * @protected
 */
class SortBar extends UiComponent {
  //
  /**
   * <h4>`SortBar.displayName` static member</h4>
   *  String is used in debugging messages. JSX sets this value automatically.
   * <br/><br/>
   * @protected
   */
  static displayName = 'SortBar';

  /**
   * <h4>`SortBar.propTypes` static member</h4>
   *   Defined as a property on the component class itself, to define what types the props should be. It should be a map from prop names to types as defined in React.PropTypes. In development mode, when an invalid value is provided for a prop, a warning will be shown in the JavaScript console. In production mode, propTypes checks are skipped for efficiency.
   * <br/><br/>
   * items :[{@link SortBarButtonSpec}] (*[])
   * <br/><br/>
   * @protected
   */
  static propTypes = {
    items: PropTypes.arrayOf(React.PropTypes.object) // id text selected
  };

  /**
   * <h4>`SortBar.defaultProps` static member</h4>
   *  Defined as a property on the component class itself, to set the default props for the class. This is used for undefined props, but not for null props.
   * <br/><br/>
   * items :[{@link SortBarButtonSpec}] (*[])
   * <br/><br/>
   * @protected
   */
  static defaultProps = {
    items:[]
  };

  /**
   * <h4>`pubs` static object</h4>
   * {@link PubSub} {@link Dispatch} Interface
   * - `change` - notification sending data-id
   * <br/><br/>
   * See {@link AppDisplay.pubs} for documentation
   * <br/><br/>
   * @type {Object}
   * @property {Dispatch} change - notification sending data-id
   * @public
   */
  static pubs ={
    change:'sort change: id size price'
  };

  /**
   * <h4>`SortBar` constructor</h4>
   * call `super`
   * - see {@link SortBar_React_Component_export} for docs
   * <br/><br/>
   * @protected
   * @param {Object} props
   * @param {Object} context
   */
  constructor(props, context) {
    super(props, context);
    // set state
    this.state = { // keep to avoid no-useless-constructors
    };
  }

  /**
   * <h4>`SortBar#handleClick` bound handler</h4>
   * Say `change` with source data-id
   * <br/><br/>
   * @protected
   */
  handleClick = (e) => {
    const tgt=e.currentTarget;
    const me=$(tgt);
    this.say(this.pubs.change, me.attr('data-id'));
  };

  /// LifeCycle
    /// componentWillMount
    /// componentDidMount
    /// componentWillReceiveProps
    /// shouldComponentUpdate
    /// componentWillUpdate
    /// componentDidUpdate
    /// componentWillUnmount
  ///

  /**
   * <h4>`SortBar#render` function</h4>
   * div>(span [button])
   * <br/><br/>
   * Series of toggle buttons with 'sort by' label
   * ToDo: move 'sort by' to props
   * <br/><br/>
   * <h5>Dependencies</h5>
   * None
   * @protected
   */
  render() {
    use(this); // webstorm can be static
    const rootClasses = classNames({
      'SortBar': true
      , 'root-SortBar': true
    });
    ///                                         build buttons from items
    const items = this.props.items.map((e, i)=>{
      ///                                            classnames
      const itemClasses = classNames({
        leftCap:0 === i
        , rightCap:i===this.props.items.length-1
        , selected: e.selected
        , disabled: e.disabled
      });
      ///                                            attribs
      const id=e.id;
      const data_id=e.id.toLowerCase();
      const attrs = {
        ref:id
        , key:id
        , 'data-id':data_id
        , onClick:e.selected?undefined:this.handleClick
        , className:itemClasses
      };
      return (<button {...attrs}>{e.text}</button>);
    });
    return (
      <div
        className={rootClasses}
        data-react-class="SortBar"
        data-react-file="SortBar.jsx"
        >
        <span>sort by</span><br/>
        {items}
      </div>
    );
  }
}
Dispatch.initPubs(SortBar);
/**
 * <h4>`SortBar` React Component default export</h4>
 * <br/><br/>
 * - file {@link src_components_SortBar_jsx}
 * @protected
 */
const SortBar_React_Component_export = 'EsDocs symbol';
use(SortBar_React_Component_export);
export default SortBar;
