/* global */
/**
 * <h4>`ItemFace` component</h4>
 * <a href="aip://open/C%3A%5C%5F%5Casciiw%5Csrc%5Ccomponents%5CItemFace%2Ejsx">C:\_\asciiw\src\components\ItemFace.jsx</a><br>
 * <br/><br/>
 * Wrapper for Face
 * <br/><br/>
 * <h5>NPM Dependencies</h5>
 * - {@link React}          - `react`
 * - {@link Component}      - `react`
 * - {@link PropTypes}      - `react`
 * - {@link $}              - `jquery`
 * - {@link classNames}     - `classnames`
 * - {@link Trianglify}     - `trianglify`
 * - {@link tinycolor}      - `tinycolor2`
 * <br/><br/>
 * <h5>Project Dependencies</h5>
 * - {@link Utils}        - `../utils/utils`
 * - {@link Dispatch}     - `../utils/utils`
 * - {@link UiComponent}  - `../react/UiComponent`
 * <br/><br/>
 * <h5>Local Dependencies</h5>
 * None
 * <br/><br/>
 * <h5>Product {@link ItemFace}</h5>
 * <br/><br/>
 * @protected
 * @see https://bitbucket.org/mindprism/asciiw/src/master/src/components/ItemFace.jsx
 */
const src_components_ItemFace_jsx= "/src/components/ItemFace.jsx";

/// Base Imports
import UiComponent from '../react/UiComponent';
import React, { Component, PropTypes } from 'react';
import tinycolor from 'tinycolor2';
import Trianglify from 'trianglify';
//noinspection JSUnresolvedVariable
import $ from 'jquery';
import classNames from 'classnames';
///
/// Util Imports
import Utils from '../utils/utils';
import { use, Dispatch } from '../utils/utils';
///
/// Local Imports
import './ItemFace.css';
///
/// Local Helpers
use($);
use(Utils);
use(Dispatch);
use(Component);
use(src_components_ItemFace_jsx);

/// Class
/**
 * <h4>`ItemFace` React Component class</h4>
 * Color face parts using hash of face, text shadow, background png from Trianglify
 * <br/><br/>
 * - {@link ItemFace.displayName} - ItemFace
 * - {@link ItemFace.propTypes} - itemFace :String.!, item :Face.!
 * - {@link ItemFace.defaultProps} - none
 * - {@link ItemFace._pngs} - String of png created by {@link Trianglify} keyed by {@link Face.face}
 * - {@link ItemFace#constructor} - no action
 * - {@link ItemFace#render} -div>div.itemFaceFrame>div.itemFaceFace>div.Face>([]div.FacePart])
 * <br/><br/>
 * <h5>Dependencies</h5>
 * - {@link classNames}     - `classnames`
 * - {@link Trianglify}     - `trianglify`
 * - {@link tinycolor}      - `tinycolor2`
 * - {@link UiComponent}  - `../react/UiComponent`
 * <br/><br/>
 * - file {@link src_components_ItemFace_jsx}
 * @protected
 */
class ItemFace extends UiComponent {
  //
  /**
   * <h4>`ItemFace.displayName` static member</h4>
   *  String is used in debugging messages. JSX sets this value automatically.
   * <br/><br/>
   * @protected
   */
  static displayName = 'ItemFace';

  /**
   * <h4>`ItemFace.propTypes` static member</h4>
   *   Defined as a property on the component class itself, to define what types the props should be. It should be a map from prop names to types as defined in React.PropTypes. In development mode, when an invalid value is provided for a prop, a warning will be shown in the JavaScript console. In production mode, propTypes checks are skipped for efficiency.
   * <br/><br/>
   * itemFace :String.!, item :Face.!
   * <br/><br/>
   * @type {Object}
   * @property {PropTypes} propTypes.itemFace `PropTypes.string.required` (!)
   * @property {PropTypes} propTypes.item `PropTypes.object.required` (!)
   * <br/><br/>
   * @protected
   */
  static propTypes = {
    itemFace: PropTypes.string
    , item: PropTypes.object
  };

  /**
   * <h4>`ItemFace.defaultProps` static member</h4>
   *  Defined as a property on the component class itself, to set the default props for the class. This is used for undefined props, but not for null props.
   * <br/><br/>
   * itemFace :String.!, item :Face.!
   * <br/><br/>
   * @type {Object}
   * <br/><br/>
   * @protected
   */
  static defaultProps = {};

  /**
   * <h4>`ItemFace._pngs` private dictionary</h4>
   * <br/><br/>
   * String of png created by {@link Trianglify} keyed by {@link Face.face}
   * <br/><br/>
   * @type {Object}
   * <br/><br/>
   * @protected
   */
  static _pngs = {
  };

  /**
   * <h4>`ItemFace` constructor</h4>
   * - call `super`
   * <br/><br/>
   * - see {@link ItemFace_React_Component_export} for docs
   * <br/><br/>
   * @protected
   * @param {Object} props
   * @param {Object} context
   */
  constructor(props, context) {
    super(props, context);
    // set state
    this.state = { // keep to avoid no-useless-constructors
    };
  }

  /// LifeCycle
    /// componentWillMount
    /// componentDidMount
    /// componentWillReceiveProps
    /// shouldComponentUpdate
    /// componentWillUpdate
    /// componentDidUpdate
    /// componentWillUnmount
  ///

  /**
   * <h4>`ItemFace#render` function</h4>
   * div>div.itemFaceFrame>div.itemFaceFace>div.Face>([]div.FacePart])
   * <br/><br/>
   * Color face parts using hash of face, text shadow, background png from Trianglify
   * Todo: Move some of this to css, simplify
   * <br/><br/>
   * <h5>Dependencies</h5>
   * - {@link classNames}     - `classnames`
   * - {@link Trianglify}     - `trianglify`
   * - {@link tinycolor}      - `tinycolor2`
   * <br/><br/>
   * @protected
   */
  render() {
    use(this); // webstorm can be static
    const rootClasses = classNames({
      'ItemFace': true
      , 'root-ItemFace': true
    });
    const faceStyle={
      color:Utils.hashStringToColor(this.props.itemFace)
      , background:'radial-gradient(rgba(0,0,0,0.9), rgba(0,0,0,0.7))'
    };
    ///                                    save colors used
    const colors=[];
    ///                                    break up itemFace and build components
    const str=this.props.itemFace.split('').map((e, i)=>{
      ///                                        get colors for text and shadow
      const clr=tinycolor(Utils.hashStringToColor(e+this.props.itemFace));
      const clr2=tinycolor(clr.toString());
      ///                                        lighten or darken for shadow
      let clrtxt;
      //noinspection JSUnresolvedFunction
      if(clr.isDark()) {
        //noinspection MagicNumberJS,JSUnresolvedFunction
        clrtxt=clr2.lighten(30).spin(-25).toString();
      }else{
        //noinspection MagicNumberJS,JSUnresolvedFunction
        clrtxt=clr2.darken(30).spin(25).toString();
      }
      ///                                        add color of item to colors used
      colors.push(clr.toString());
      ///                                        create item style
      const st={
        color:clr.toString()
        , textShadow:`2px 2px 2px ${clrtxt}`//+', 0px 0px 10px #000000'
        , display:'inline-block'
      };
      return <div className="FacePart" key={i} style={st}>{e}</div>;
    });
    ///                                    get Trianglify png string from cache
    const getPng = () => {
      const _pngs=this.constructor._pngs;
      ///                                        indicate no need to write css style, it should exist
      if(_pngs[this.props.itemFace]) {
        return { png:_pngs[this.props.itemFace], write:false };
      }
      ///                                        need to create, compute properties
      const cellsize_=this.props.item.price/10;
      const cellsize=5 < cellsize_?cellsize_:6;
      ///                                        make it
      //noinspection MagicNumberJS
      const pattern = Trianglify({
        height: 300,
        width: 120,
        cell_size: cellsize,
        variance: this.props.item.size /100,
        x_colors: colors
      });
      ///                                        compute png and cache it
      //noinspection JSUnresolvedFunction
      const png=pattern.png();
      _pngs[this.props.itemFace]=png;
      ///                                        indicate need to write css, return data
      return { png, write:true };
    };
    ///                                    query cache for png
    const png_=getPng();
    ///                                    write css if necc
    if(png_.write) {
      const css=`#_${this.props.item.id}\{background-image:url(${png_.png});}`;
      Utils.updateNamedStyle(css, `_${this.props.item.id}`);
    }
    ///                                    style for frame, some of this can be moved to css
    const frameStyle={
      //backgroundImage:'url('+getPng()+')'
      backgroundSize:'cover'
      , backgroundPosition:'center center'
      , boxShadow:`inset 6px 6px 6px 0px #000000, inset -1px -1px 23px 0px ${colors[colors.length - 1]}`
    };
    ///                                    use id to paint png from css
    return (
      <div
        className={rootClasses}
        data-react-class="ItemFace"
        data-react-file="ItemFace.jsx"
        >
        <div className="itemFaceFrame" id={`_${this.props.item.id}`} style={frameStyle}>
          <div className="itemFaceFace" style={faceStyle}>
            <div className="Face">
              {str}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
/**
 * <h4>`ItemFace` React Component default export</h4>
 * <br/><br/>
 * - file {@link src_components_ItemFace_jsx}
 * @protected
 */
const ItemFace_React_Component_export = 'EsDocs symbol';
use(ItemFace_React_Component_export);
export default ItemFace;
