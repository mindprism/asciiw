/* global */
/**
 * <h4>`App` top component</h4>
 * <a href="aip://open/C%3A%5C%5F%5Casciiw%5Csrc%5CApp%2Ejsx">C:\_\asciiw\src\App.jsx</a><br>
 * <br/><br/>
 * The Application
 * <h5>NPM Dependencies</h5>
 * - {@link React}          - `react`
 * - {@link Component}      - `react`
 * - {@link PropTypes}      - `react`
 * - {@link $}              - `jquery`
 * - {@link classNames}     - `classnames`
 * - {@link ReactTooltip}   - `react-tooltip`
 * - {@link ToastContainer} - `react-toastify`
 * - {@link toast}          - `react-toastify`
 * <br/><br/>
 * <h5>Project Dependencies</h5>
 * - {@link Utils}        - `../utils/utils`
 * - {@link Dispatch}     - `../utils/utils`
 * - {@link UiComponent}  - `../react/UiComponent`
 * <br/><br/>
 * <h5>Local Dependencies</h5>
 * - {@link NewLineJsonRequester} - `./api/NewLineJsonRequester`
 * - {@link MultiSortApiFetcher}  - `./services/MultiSortApiFetcher`
 * - {@link AppDisplay}           - `./components/AppDisplay`
 * - {@link AppHeader}            - `./AppHeader`
 * <br/><br/>
 * <h5>Products</h5>
 * - {@link App} - {@link AppHeader}, {@link AppDisplay}
 * <br/><br/>
 * @protected
 * @see https://bitbucket.org/mindprism/asciiw/src/master/src/App.jsx
 */
const src_App_jsx= "/src/App.jsx";

/// Base Imports
import UiComponent from './react/UiComponent';
import React, { Component, PropTypes } from 'react';
//noinspection JSUnresolvedVariable
import $ from 'jquery';
import classNames from 'classnames';
import ReactTooltip from 'react-tooltip';
import { ToastContainer } from 'react-toastify';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';
///
/// Util Imports
import Utils from './utils/utils';
import { use, Dispatch } from './utils/utils';
///
/// Api Imports
import NewLineJsonRequester from './api/NewLineJsonRequester';
///
/// Service Imports
import MultiSortApiFetcher from './services/MultiSortApiFetcher';
///
/// Component Imports
import AppHeader from './AppHeader';
import AppDisplay from './components/AppDisplay';
///
/// Local Imports
import './App.css';
///
/// Local Helpers
use($);
use(Utils);
use(Component);
use(PropTypes);
use(src_App_jsx);

/// Class
/**
 * <h4>`App` React Component class</h4>
 * {@link AppHeader}, {@link AppDisplay}, {@link ReactTooltip}, {@link ReactToastify}
 * <br/><br/>
 * <h5>Members</h5>
 * - {@link App.displayName} - App
 * - {@link App.propTypes} - none
 * - {@link App.defaultProps} - none
 * - {@link App.hear} - AppDisplay.sort.change, AppDisplay.items.more, MultiSortApiFetcher.data
 * - {@link App#fetcher} - {@link MultiSortApiFetcher}
 * - {@link App#constructor} - setState, create {@link App#fetcher}
 * - {@link App#getItems} - Use {@link App#fetcher} to get {@link Face} data from server
 * - {@link App#handleReactTip} - If ctrl key is down return reactTipHtml from state else clear reactTipHtml and hide {@link ReactTooltip}
 * - {@link App#componentDidMount} - handle fadein, watch window scroll, get Faces
 * - {@link App#render} - div>(AppHeader AppDisplay ReactTooltip ToastContainer)
 * <br/><br/>
 * <h5>NPM Dependencies</h5>
 * - {@link React}          - `react`
 * - {@link Component}      - `react`
 * - {@link PropTypes}      - `react`
 * - {@link $}              - `jquery`
 * - {@link classNames}     - `classnames`
 * - {@link ReactTooltip}   - `react-tooltip`
 * - {@link ToastContainer} - `react-toastify`
 * - {@link toast}          - `react-toastify`
 * <br/><br/>
 * <h5>Local Dependencies</h5>
 * - {@link UiComponent}          - `../react/UiComponent`
 * - {@link NewLineJsonRequester} - `./api/NewLineJsonRequester`
 * - {@link MultiSortApiFetcher}  - `./services/MultiSortApiFetcher`
 * - {@link AppDisplay}           - `./components/AppDisplay`
 * - {@link AppHeader}            - `./AppHeader`
 * <br/><br/>
 * <h5>Produces</h5>
 * - {@link App}
 * <br/><br/>
 * - file {@link src_App_jsx}
 * @protected
 */
class App extends UiComponent {
  //
  /**
   * <h4>`App.displayName` static member</h4>
   *  String is used in debugging messages. JSX sets this value automatically.
   * <br/><br/>
   * @protected
   */
  static displayName = 'App';

  /**
   * <h4>`App.propTypes` static member</h4>
   *   Defined as a property on the component class itself, to define what types the props should be. It should be a map from prop names to types as defined in React.PropTypes. In development mode, when an invalid value is provided for a prop, a warning will be shown in the JavaScript console. In production mode, propTypes checks are skipped for efficiency.
   * <br/><br/>
   * None - top level component
   * <br/><br/>
   * @protected
   */
  static propTypes = {};

  /**
   * <h4>`App.defaultProps` static member</h4>
   *  Defined as a property on the component class itself, to set the default props for the class. This is used for undefined props, but not for null props.
   * <br/><br/>
   * None - top level component
   * <br/><br/>
   * @protected
   */
  static defaultProps = {};

  //noinspection JSFunctionExpressionToArrowFunction
  /**
   * <h4>`App.hear` static object</h4>
   * {@link PubSub} Subscribe Interface
   * - `AppDisplay.sort.change`
   * - `AppDisplay.items.more`
   * - `MultiSortApiFetcher.data`
   * <br/><br/>
   * <br/><br/>
   * <h5>Usage</h5>
   * First see {@link AppDisplay.pubs} to see how PubSub messages are produced.
   * <br/><br/>
   * This object gets processed and initialized in {@link BaseComponent#constructor}, using its keys to produce {@link PubSub} subscriptions.
   * The item format is `[< class with pubs >.< object path >.msg]: [< class with pubs >, function(msg, data)]` - note `function` must be used, not arrow functions.
   * The `function` is bound to `this` during initialization of each class instance. The `this` inside of `function` is therefore the class instance, not the static class.
   * <br/><br/>
   * Inside the `function` data may be 'raw' or wrapped when it is sent by `this.say` ({@link BaseComponent#say}).
   * It is ok to simply pass the data along though `this.say()` and it will get further wrapped.
   * If you wish to unwrap before passing it along, or to get at the actual data, use `Dispatch.dataOf(data)`.
   * <br/><br/>
   * See {@link BaseComponent#say} to see how data is wrapped when using `this.say()`.
   * Basically `say(< dispatch >, data, extra)` will produce a data object with a `sender` member equal to `this` and `data` member equal to `data`.
   * In this way, senders can be traced. Also, messages are dispatched with extra info using `this.say`, see {@link BaseComponent#say}
   * <br/><br/>
   * @type {Object}
   * @property {HearSpec} AppDisplay.sort.change -- getItems
   * @property {HearSpec} AppDisplay.items.more -- getItems
   * @property {HearSpec} MultiSortApiFetcher.data -- toast
   * @protected
   */
  static hear = {
    [AppDisplay.pubs.sort.change.msg]: [AppDisplay, function (msg, data) {
      this.getItemsNoMore(Dispatch.dataOf(data));
    }]
    , [AppDisplay.pubs.items.more.msg]: [AppDisplay, function (msg, data) {
      this.getItems(Dispatch.dataOf(data));
    }]
    , [MultiSortApiFetcher.pubs.data.msg]: [MultiSortApiFetcher, function (msg, data) {
      console.log('MultiSortApiFetcher.pubs.data.msg');
      toast(`MultiSortApiFetcher:got data, length:${Dispatch.dataOf(data).length}`, { type: toast.TYPE.INFO, style:{ zIndex:10000 }});
      //this.getItems(Dispatch.dataOf(data));
    }]
  };

  /**
   * <h4>`App#fetcher` service member</h4>
   * {@link MultiSortApiFetcher}
   * <br/><br/>
   * @protected
   */
  fetcher = null;

  /**
   * <h4>`App` constructor</h4>
   * - call `super`
   * - setState
   * - new {@link App#fetcher}
   * <br/><br/>
   * - see {@link App_React_Component_export} for docs
   * - see {@link src_App_jsx} for file docs
   * <br/><br/>
   * @protected
   * @param {Object} props
   * @param {Object} context
   */
  constructor(props, context) {
    super(props, context);
    // set state
    this.state = {
      reactTip:'reactTip'
      , reactTipHtml:''
      , items:[]
      , fetching: false
      , sortField:'id'
      , limit: 10
    };
    const requester = new NewLineJsonRequester('/api', {});
    const options = { api: requester, keyField: 'face' };
    //noinspection JSCheckFunctionSignatures
    this.fetcher = new MultiSortApiFetcher(options);
  }

  /**
   * <h4>`App#getItems` private command</h4>
   * Use {@link App#fetcher} to get {@link Face} data from server
   * <br/><br/>
   * Put items in state, show toast, set sortField
   * <br/><br/>
   * @protected
   */
  getItems(sortField) {
    //noinspection AssignmentToFunctionParameterJS
    sortField=sortField?sortField:this.state.sortField;
    const count=this.state.items.length;
    const p=this.fetcher.fetch(sortField, count+this.state.limit);
    if (!p) { // fetcher is busy
      return;
    }
    this.setState({ fetching: true });
    p.then((data)=>{
      const trimmed = data.slice(0, count+this.state.limit);
      toast(`App: offer data, length:${trimmed.length}`, { type: toast.TYPE.INFO });
      this.setState({ items:trimmed, fetching:false, sortField: sortField });
    });
  }

  getItemsNoMore(sortField) {
    //noinspection AssignmentToFunctionParameterJS
    sortField=sortField?sortField:this.state.sortField;
    this.setState({ fetching: true });
    const count=this.state.items.length;
    const p=this.fetcher.fetch(sortField, count);
    p.then((data)=>{
      const trimmed = data.slice(0, count);
      this.setState({ items:trimmed, fetching:false, sortField: sortField });
    });
  }

  /**
   * <h4>`App#handleReactTip` bound handler</h4>
   * If ctrl key is down return reactTipHtml from state
   * else clear reactTipHtml and hide {@link ReactTooltip}
   * <br/><br/>
   * @protected
   */
  handleReactTip = () => {
    if ('c' === this.cas) {
      return this.state.reactTipHtml;
    }
    if ('' !== this.state.reactTipHtml) {
      this.setState({ reactTipHtml:'' });
    }
    ReactTooltip.hide(this.node);
    return null;
  };

  /// componentWillMount

  /**
   * <h4>`App#componentDidMount` event</h4>
   * - remove fadein class from body
   * - setup window scroll at top/bottom
   * - request {@link Face} using {@link App#getItems}
   * <br/><br/>
   * <h5>Dependencies</h5>
   * - {@link Utils}        - `../utils/utils`
   * - {@link $}            - `jquery`
   * @protected
   */
  componentDidMount() {
    use(this); // webstorm can be static
    //noinspection JSCheckFunctionSignatures
    $('body').removeClass('fadein');
    $(window).scroll(() => {
      if (Utils.isWindowScrollAtBottom()) {
        if(!this.APP.scrollAtBottomTimeout) {
          //noinspection MagicNumberJS
          this.APP.scrollAtBottomTimeout = setTimeout(()=>{
            delete this.APP.scrollAtBottomTimeout;
            if (Utils.isWindowScrollAtBottom()) {
              this.getItems();
            }
          }, 2000);
        }
      }
      if (Utils.isWindowScrollAtTop()) {
        if(!this.APP.scrollAtTopTimeout) {
          //noinspection MagicNumberJS
          this.APP.scrollAtTopTimeout = setTimeout(()=>{
            delete this.APP.scrollAtTopTimeout;
            if (Utils.isWindowScrollAtTop()) {
              $('body').removeClass('short');
            }
          }, 2000);
        }
      }else{
        $('body').addClass('short');
      }
    });
    this.getItems();
  }

  /// LifeCycle
    /// componentWillReceiveProps
    /// shouldComponentUpdate
    /// componentWillUpdate
    /// componentDidUpdate
    /// componentWillUnmount
  ///

  /**
   * <h4>`App#render` function</h4>
   * div>(AppHeader AppDisplay ReactTooltip ToastContainer)
   * <br/><br/>
   * Watch ReactTooltip polling - maybe make this depend on state
   * <br/><br/>
   * <h5>Dependencies</h5>
   * - {@link AppHeader}      - `./AppHeader`
   * - {@link AppDisplay}     - `./components/AppDisplay`
   * - {@link ReactTooltip}   - `react-tooltip`
   * - {@link ToastContainer} - `react-toastify`
   * @protected
   */
  render() {
    use(this); // webstorm can be static
    const rootClasses = classNames({
      'App': true
      , 'root-App': true
    });
    //noinspection MagicNumberJS
    return (
      <div
        className={rootClasses}
        data-react-class="App"
        data-react-file="App.jsx"
        data-tip=""
        data-for="rtt"
        >
        <AppHeader/>
        <AppDisplay
          items={this.state.items}
          sort={this.state.sortField}
          fetching={this.state.fetching}
          />
        <ReactTooltip
          id="rtt"
          html={true}
          getContent={[this.handleReactTip, 300]}
          />
        <ToastContainer autoClose={6000} position="bottom-right"/>
      </div>
    );
  }
}
/**
 * <h4>`App` React Component default export</h4>
 * <br/><br/>
 * - file {@link src_App_jsx}
 * @protected
 */
const App_React_Component_export = 'EsDocs symbol';
use(App_React_Component_export);
export default App;
