/* global */
/**
 * <h4>`App` tests</h4>
 * <a href="aip://open/C%3A%5C%5F%5Casciiw%5Csrc%5CApp%2Etest%2Ejs">C:\_\asciiw\src\App.test.js</a><br>
 * <br/><br/>
 * <h5>NPM Dependencies</h5>
 * - {@link React} - `react`
 * - {@link ReactDOM} - `react-dom`
 * <br/><br/>
 * <h5>Project Dependencies</h5>
 * - {@link Utils} - `../utils/utils`
 * <br/><br/>
 * <h5>Local Dependencies</h5>
 * - {@link App} - `./App`
 * <br/><br/>
 * @protected
 * @see https://bitbucket.org/mindprism/asciiw/src/master/src/App.test.js
 */
const src_App_test_js= "/src/App.test.js";

/// Base Imports
import React from 'react';
import ReactDOM from 'react-dom';
///
/// Util Imports
import Utils from './utils/utils';
import { use } from './utils/utils';
///
/// Local Imports
import App from './App';
///
/// Local Helpers
use(Utils);
use(src_App_test_js);

///
/// Test Suites
/**
 * <h4>`App should render` suite</h4>
 * <br/><br/>
 * @protected
 */
const App_should_render_suite = true;
if(App_should_render_suite) {
  /**
   * @test {App}
   */
  describe('should render', () => {
    /**
     * @test {App}
     */
    it('renders into div', () => {
      const div = document.createElement('div');
      ReactDOM.render(<App />, div);
    }); //-suite
  }); //-suite
} //-App_should_render_suite

