/**
 * <h4>External Definitions for EsDoc </h4>
 * <a href="aip://open/C%3A%5C%5F%5Casciiw%5Csrc%5Cexternals%2Ejs">C:\_\asciiw\src\externals.js</a><br>
 * <br/><br/>
 * <h5>Project Dependencies</h5>
 * - {@link Utils} - `../utils/utils`
 * <h5>Testing External Definitions</h5>
 * lll
 * <br/><br/>
 * <h5>NPM External Definitions</h5>
 * none
 * <br/><br/>
 * @protected
 * @see https://bitbucket.org/mindprism/asciiw/src/master/src/externals.js
 */
const src_externals_js= "/src/externals.js";

///
/// Util Imports
import Utils from './utils/utils';
import { use } from './utils/utils';
///
/// Local Helpers
use($);
use(Utils);
use(src_externals_js);

/// UNUSED
/**
 * @typedef {Object} _
 * @external {_} http://underscorejs.org/
 */

////// TESTING

/**
 * Jest
 * @typedef {Object} jest
 * @external {jest} https://facebook.github.io/jest/docs/jest-object.html#content
 */

/**
 * Jest
 * @typedef {function} expect
 * @external {expect} https://facebook.github.io/jest/docs/expect.html
 */

/// UNUSED
/**
 * @typedef {Object} chai
 * @external {chai} http://chaijs.com/api/
 */
/// UNUSED
/**
 * @typedef {function} assert
 * @external {assert} http://chaijs.com/api/assert/
 */
/// UNUSED
/**
 * @typedef {function} expectX
 * @external {expectX} http://chaijs.com/api/bdd/
 */
/// UNUSED
/**
 * @typedef {function} unexpected
 * @external {unexpected} http://unexpected.js.org/
 */
/// UNUSED
/**
 * @typedef {function} demand
 * @external {demand} http://unexpected.js.org/
 */
/**
 * @typedef {Object} JsDom
 * @external {JsDom} https://github.com/tmpvar/jsdom
 */
/// UNUSED
/**
 * @typedef {Object} mochajsdom
 * @external {mochajsdom} https://www.npmjs.com/package/mocha-jsdom
 */
/// UNUSED
/**
 * @typedef {Object} Reactor
 * @external {Reactor} https://facebook.github.io/react/docs/test-utils.html
 */
/// UNUSED
/**
 * @typedef {Object} sinon
 * @external {sinon} http://sinonjs.org/docs/
 */

////// TESTING SUITES

/**
 * @typedef {function} afterAll
 * @external {afterAll} http://facebook.github.io/jest/docs/api.html#afterallfn
 */
/**
 * @typedef {function} afterEach
 * @external {afterEach} http://facebook.github.io/jest/docs/api.html#aftereachfn
 */

/**
 * @typedef {function} beforeAll
 * @external {beforeAll} http://facebook.github.io/jest/docs/api.html#beforeallfn
 */
/**
 * @typedef {function} beforeEach
 * @external {beforeEach} http://facebook.github.io/jest/docs/api.html#beforeeachfn
 */

//

/**
 * @typedef {function} describe
 * @external {describe} http://facebook.github.io/jest/docs/api.html#describename-fn
 */
/**
 * @typedef {function} describe.only
 * @external {describe.only} http://facebook.github.io/jest/docs/api.html#describeonlyname-fn
 */
/**
 * @typedef {function} describe.skip
 * @external {describe.skip} http://facebook.github.io/jest/docs/api.html#describeskipname-fn
 */

/**
 * @typedef {function} require.requireActual
 * @external {require.requireActual} http://facebook.github.io/jest/docs/api.html#requirerequireactualmodulename
 */
/**
 * @typedef {function} require.requireMock
 * @external {require.requireMock} http://facebook.github.io/jest/docs/api.html#requirerequiremockmodulename
 */

/**
 * @typedef {function} it
 * @external {it} http://facebook.github.io/jest/docs/api.html#testname-fn
 */
/**
 * @typedef {function} test
 * @external {test} http://facebook.github.io/jest/docs/api.html#testname-fn
 */

/**
 * @typedef {function} it.only
 * @external {it.only} http://facebook.github.io/jest/docs/api.html#testonlyname-fn
 */
/**
 * @typedef {function} test.only
 * @external {test.only} http://facebook.github.io/jest/docs/api.html#testonlyname-fn
 */

/**
 * @typedef {function} it.skip
 * @external {it.skip} http://facebook.github.io/jest/docs/api.html#testskipname-fn
 */
/**
 * @typedef {function} test.skip
 * @external {test.skip} http://facebook.github.io/jest/docs/api.html#testskipname-fn
 */

//

/**
 * @typedef {function} browser
 * @external {browser} http://webdriver.io/api.html
 */

////// NPM

/**
 * @typedef {Object} React
 * @external {React} https://facebook.github.io/react/docs/hello-world.html
 */
/**
 * @typedef {Object} ReactDOM
 * @external {ReactDOM} https://facebook.github.io/react/docs/hello-world.html
 */
/**
 * @typedef {function} rest
 * @external {rest} https://www.npmjs.com/package/rest
 */
/**
 * @typedef {function} classNames
 * @external {classNames} https://www.npmjs.com/package/classnames
 */

//

/**
 * @typedef {function} jQuery
 * @external {jQuery} http://api.jquery.com/
 */
/**
 * @typedef {function} $
 * @external {$} http://api.jquery.com/
 */

/**
 * @typedef {function} ReactTooltip
 * @external {ReactTooltip} https://www.npmjs.com/package/react-tooltip
 */

/**
 * @typedef {object} PubSub
 * @external {PubSub} https://www.npmjs.com/package/pubsub-js
 */

/**
 * @typedef {Object} Face
 * @property {string} id
 * @property {string} face
 * @property {number} size
 * @property {number} price
 * @external {Face} https://www.npmjs.com/package/cool-ascii-faces
 */

/**
 * @typedef {Object} PropTypes
 * @external {PropTypes} https://facebook.github.io/react/docs/typechecking-with-proptypes.html
 */

/**
 * @typedef {Object} Component
 * @external {Component} https://facebook.github.io/react/docs/react-component.html
 */

/**
 * @typedef {Object} ToastContainer
 * @external {ToastContainer} https://www.npmjs.com/package/react-toastify
 */

/**
 * @typedef {function} toast
 * @external {toast} https://www.npmjs.com/package/react-toastify
 */

/**
 * @typedef {Object} Motion
 * @external {Motion} https://www.npmjs.com/package/react-motion
 */

/**
 * @typedef {function} spring
 * @external {spring} https://www.npmjs.com/package/react-motion
 */

/**
 * @typedef {function} Trianglify
 * @external {Trianglify} https://www.npmjs.com/package/trianglify
 */

/**
 * @typedef {function} tinycolor
 * @external {tinycolor} https://www.npmjs.com/package/tinycolor2
 */

//

/**
 * @typedef {Object} Audio
 * @external {Audio} https://www.w3schools.com/jsref/dom_obj_audio.asp
 */

/**
 * @typedef {Object} localStorage
 * @external {localStorage} https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage
 */

///

// * @typedef {Object} WriteResult
// * @property {number} nInserted
// * @property {number} nMatched
// * @property {number} nModified
// * @property {number} nUpserted
// * @property {string} _id
// * @property {Object} writeError
// * @property {Object} writeConcernError
