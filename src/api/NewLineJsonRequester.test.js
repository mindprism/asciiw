/* global jasmine*/
/**
 * <h4>`NewLineJsonRequester` tests</h4>
 * <a href="aip://open/C%3A%5C%5F%5Casciiw%5Csrc%5Capi%5CNewLineJsonRequester%2Etest%2Ejs">C:\_\asciiw\src\api\NewLineJsonRequester.test.js</a><br>
 * <br/><br/>
 * <h5>NPM Dependencies</h5>
 * <br/><br/>
 * <h5>Project Dependencies</h5>
 * - {@link Utils} - `../utils/utils`
 * <br/><br/>
 * <h5>Local Dependencies</h5>
 * - {@link NewLineJsonRequester} - `./NewLineJsonRequester`
 * <br/><br/>
 * @protected
 * @see https://bitbucket.org/mindprism/asciiw/src/master/src/api/NewLineJsonRequester.test.js
 */
const src_api_NewLineJsonRequester_test_js= "/src/api/NewLineJsonRequester.test.js";

///
/// Util Imports
import Utils from '../utils/utils';
import { use } from '../utils/utils';
import JestHelpers from '../utils/JestHelpers';

const J = JestHelpers;
///
/// Local Imports
import NewLineJsonRequester from './NewLineJsonRequester';
///
/// Local Helpers
use(Utils);
use(src_api_NewLineJsonRequester_test_js);

///
/// Test Suites
/**
 * <h4>`NewLineJsonRequester should construct` suite</h4>
 * <br/><br/>
 * @protected
 */
const NewLineJsonRequester_should_construct_suite = true;
if (NewLineJsonRequester_should_construct_suite) {
  /**
   * @test {NewLineJsonRequester}
   */
  describe('should construct', () => {
    /**
     *
     */
    it('runs a dummy test', () => {
    }); //- test
    /**
     * @test {NewLineJsonRequester}
     */
    it('creates a new instance', () => {
      const r=new NewLineJsonRequester('http://localhost:3000/api', {});
      J.expectType(r, NewLineJsonRequester);
    }); //- test
  });
} //- NewLineJsonRequester_should_construct_suite

/**
 * <h4>`NewLineJsonRequester instances` suite</h4>
 * <br/><br/>
 * @protected
 */
const NewLineJsonRequester_instances_suite = true;
if (NewLineJsonRequester_instances_suite) {
  /**
   * @test {NewLineJsonRequester}
   */
  describe('instances', () => {
    let _good;
    let _bad;
    /**
     *
     */
    beforeAll(() => {
      _good = new NewLineJsonRequester('http://localhost:3000/api', {});
      _bad = new NewLineJsonRequester('http://localhost:3000/xapi', {});
      //noinspection MagicNumberJS
      jasmine.DEFAULT_TIMEOUT_INTERVAL = 6000;// was 5000
    }); //- beforeAll
    /**
     *
     */
    // Cannot use 'resolves' or 'rejects' unless Jest version 20.0.0+
    // have to use method illustrated next
    // xit('should process request', async () => {
    //   await expect(R.request({ sort:'id', skip:0 })).resolves.toBeInstanceOf(Array);
    // });
    /**
     * @test {NewLineJsonRequester#request}
     */
    it('should process request', () => {
      const rv=_good.request({ sort:'id', skip:0 });
      rv.then((res)=>{
        J.expectArray(res);
      })
        .catch((xx)=>{
          console.log('FAIL HERE', xx);
        })
      ;
      return rv;
    }); //- test
    /**
     * @test {NewLineJsonRequester#request}
     */
    it('should process request sorted by size', () => {
      const rv=_good.request({ sort:'size', skip:0 });
      rv.then((res)=>{
        J.expectArray(res);
      })
        .catch((xx)=>{
          console.log('FAIL HERE', xx);
        })
      ;
      return rv;
    }); //- test
    /**
     * @test {NewLineJsonRequester#request}
     */
    it('should process request sorted by price', () => {
      const rv=_good.request({ sort:'price', skip:0 });
      rv.then((res)=>{
        J.expectArray(res);
      })
        .catch((xx)=>{
          console.log('FAIL HERE', xx);
        })
      ;
      return rv;
    }); //- test
    /**
     * @test {NewLineJsonRequester#request}
     */
    it('should process request with limit', () => {
      const rv=_good.request({ sort:'id', skip:0, limit:2 });
      rv.then((res)=>{
        J.expectArray(res);
        expect(res.length).toBe(2);
      })
        .catch((xx)=>{
          console.log('FAIL HERE', xx);
        })
      ;
      return rv;
    }); //- test
    /**
     * @test {NewLineJsonRequester#request}
     */
    it('should process request with skip', () => {
      return new Promise((resolve, reject) => {
        const rv=_good.request({ sort:'id', skip:0 });
        let f2;
        rv.then((res) => {
          f2=res[1].face;
          const rv2=_good.request({ sort:'id', skip:1 });
          rv2.then((res) => {
            const f1=res[0].face;
            expect(f1).toEqual(f2);
            if (f1===f2) {
              resolve({ f1:f1, f2:f2 });
            }else{
              reject({ f1:f1, f2:f2 });
            }
          });//then
        }); //then
      });//promise
    }); //- test
    /**
     * @test {NewLineJsonRequester#request}
     */
    it('should fail to process request', () => {
      return new Promise((resolve, reject) => {
        const rv=_bad.request({ sort:'id', skip:0 });
        rv.then((res)=>{
          reject(res);
        })
        .catch((xx)=>{
          resolve(xx);
        });
      });//promise
    }); //- test
  }); //-suite
} //-NewLineJsonRequester_instances_suite

