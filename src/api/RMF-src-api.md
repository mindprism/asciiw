## RMF-src-api

`C:\_\asciiw\src\api`

- `FmColorName` - `blue-dark`
- `ColorHex` - `#455DAC`
- `ColorRgb` - `69,93,172`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-dark-48.png)](aip://open/C%3A%5C_%5Casciiw%5Csrc%5Capi) _Rest Api_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-cyan-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5Csrc%5CREADME.md) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Capi%5CDesktop.ini) - ini

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprism/asciiw/src/master/src/api/) - bitbucket

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Capi%5CNewLineJsonRequester.js) `NewLineJsonRequester.js` - Rest tool for json responses separated by new lines
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Capi%5CNewLineJsonRequester.test.js) `NewLineJsonRequester.test.js` - Jest
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Capi%5CREADME.html) `README.html`

