/* global */
/**
 * <h4>`NewLineJsonRequestor` rest tool</h4>
 * <a href="aip://open/C%3A%5C%5F%5Casciiw%5Csrc%5Capi%5CNewLineJsonRequester%2Ejs">C:\_\asciiw\src\api\NewLineJsonRequester.js</a><br>
 * <br/><br/>
 * <h5>NPM Dependencies</h5>
 * - {@link $}              - `jquery`
 * - {@link rest}           - `rest`
 * <br/><br/>
 * <h5>Project Dependencies</h5>
 * - {@link Utils} - `../utils/utils`
 * <br/><br/>
 * <h5>Local Dependencies</h5>
 * - {@link rest} - `rest`
 * <br/><br/>
 * @protected
 * @see https://bitbucket.org/mindprism/asciiw/src/master/src/api/NewLineJsonRequester.js
 */
const src_api_NewLineJsonRequester_js= "/src/api/NewLineJsonRequester.js";

/// Base Imports
///
/// Util Imports
import Utils from '../utils/utils';
import { use, Dispatch } from '../utils/utils';
///
/// Local Imports
import rest from 'rest';
///
/// Local Helpers
use(Utils);
use(Dispatch);
use(src_api_NewLineJsonRequester_js);

/// Class
/**
 * <h4>`NewLineJsonRequester` rest tool class</h4>
 * Make requests of given url with parameters, return json as array using promise
 * <br/><br/>
 * <h5>private static functions</h5>
 * - {@link NewLineJsonRequester._cleanUndefined} - used as Filter
 * - {@link NewLineJsonRequester._jsonParse} - Parse with JSON
 * - {@link NewLineJsonRequester._queryString} - Turn object into query string
 * <h5>private members</h5>
 * - {@link NewLineJsonRequester#_requests} - Request counter for instance
 * - {@link NewLineJsonRequester#_options} - Constructor options
 * - {@link NewLineJsonRequester#_url} - Url for rest call
 * <h5>constructor</h5>
 * - {@link NewLineJsonRequester#constructor} - Set url and options
 * <h5>public commands</h5>
 * - {@link NewLineJsonRequester#request} - Perform the request, return promise
 * <br/><br/>
 * <h5>Dependencies</h5>
 * - {@link rest}           - `rest`
 * <br/><br/>
 * - file {@link src_api_NewLineJsonRequester_js}
 * @protected
 */
class NewLineJsonRequester {
  //
  /**
   * <h4>`NewLineJsonRequester._cleanUndefined` static utility</h4>
   * Filter
   * <br/><br/>
   * @protected
   * @param {*} x
   * @return {*}
   */
  static _cleanUndefined(x) {
    return x;
  }

  /**
   * <h4>`NewLineJsonRequester._jsonParse` static utility</h4>
   * Parse with JSON
   * <br/><br/>
   * ToDo: try catch
   * <br/><br/>
   * @protected
   * @param {string} s - json string
   * @return {Object}
   */
  static _jsonParse(s) {
    return JSON.parse(s);
  }

  /**
   * <h4>`NewLineJsonRequester._queryString` static utility</h4>
   * Turn object into query string
   * <br/><br/>
   * @protected
   * @param {Object} obj
   * @return {string} - does not start with '?'
   */
  static _queryString(obj) {
    const str = [];
    for(let p in obj) {
      if (obj.hasOwnProperty(p)) {
        str.push(`${encodeURIComponent(p)}=${encodeURIComponent(obj[p])}`);
      }
    }
    return str.join("&");
  }

  /**
   * <h4>`NewLineJsonRequester#_requests` instance value</h4>
   *  Request counter for instance
   * <br/><br/>
   * @protected
   * @type {number}
   */
  _requests = 0;

  /**
   * <h4>`NewLineJsonRequester#_options`</h4>
   * Constructor options
   * <br/><br/>
   * None at present
   * <br/><br/>
   * @protected
   * @type {Object}
   * @property {Object} defaults - used in constructing query string
   */
  _options = {};

  /**
   * <h4>`NewLineJsonRequester#_url` private member</h4>
   * Url for rest call
   * <br/><br/>
   * @protected
   */
  _url = null;

  /**
   * <h4>`NewLineJsonRequester` constructor</h4>
   * Set url and options
   * <br/><br/>
   * @protected
   * @param {string} url
   * @param {Object} options
   */
  constructor(url, options = {}) {
    // options.defaults - passed to get
    this._url=url;
    this._options=Object.assign({}, options);
  }

  /**
   * <h4>`NewLineJsonRequester.request` instance command</h4>
   * Perform the request, return promise
   * <br/><br/>
   * @protected
   */
  request(params = {}) {
    this._requests++;
    console.log(`${this.constructor.name} request#:${this._requests}`);
    const defaults = this._options.defaults||{};
    const opts = Object.assign({}, defaults, params);
    let url=this._url;
    const klass = NewLineJsonRequester;
    const qs=klass._queryString(opts);
    if (qs) {
      url+=`?${qs}`;
    }
    return new Promise((resolve, reject) => {
      //noinspection JSUnresolvedFunction
      rest(url).then((response) => {
        //noinspection MagicNumberJS
        if (200 === response.status.code) {
          const data = response.entity;
          const items = data.split("\n");
          const rvData = items.filter(klass._cleanUndefined).map(klass._jsonParse);
          resolve(rvData);
        } else {
          reject(`${response.status.code} ${response.request.method} ${response.url} ${response.entity}`);
        }
      }).catch(() => {
      });
    });
  }
}
/**
 * <h4>`NewLineJsonRequester` rest tool default export</h4>
 * <br/><br/>
 * - file {@link src_api_NewLineJsonRequester_js}
 * @protected
 */
const NewLineJsonRequester_export = 'EsDocs symbol';
use(NewLineJsonRequester_export);
export default NewLineJsonRequester;
