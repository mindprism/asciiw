## README

`C:\_\asciiw\src\react`

- `FmColorName` - `cyan-dark`
- `ColorHex` - `#4A9EFE`
- `ColorRgb` - `74,158,254`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-cyan-dark-48.png)](aip://open/C%3A%5C_%5Casciiw%5Csrc%5Creact) _React Base_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-cyan-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5Csrc%5CREADME.md) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Creact%5CDesktop.ini) - ini

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprism/asciiw/src/master/src/react/) - bitbucket

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Creact%5CBaseComponent.js) `BaseComponent.js` - Child of `BaseMixin`, Parent of `UiComponent`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Creact%5CBaseMixin.js) `BaseMixin.js` - Mixin utility
 - `BaseMixin` Class
 - `._autobind`
 - `mixin` Function

<hr>

- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Creact%5CREADME.html) `README.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Creact%5CUiComponent.js) `UiComponent.js` - Base for Rendered

