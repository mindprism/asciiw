/* global */
/**
 * <h4>`BaseComponent` class export</h4>
 * <a href="aip://open/C%3A%5C%5F%5Casciiw%5Csrc%5Creact%5CBaseComponent%2Ejs">C:\_\asciiw\src\react\BaseComponent.js</a><br>
 * <br/><br/>
 * <h5>NPM Dependencies</h5>
 * - {@link React}          - `react`
 * - {@link Component}      - `react`
 * - {@link PropTypes}      - `react`
 * - {@link $}              - `jquery`
 * - {@link objectPath} - `object-path`
 * <br/><br/>
 * <h5>Project Dependencies</h5>
 * None
 * <br/><br/>
 * <h5>Local Dependencies</h5>
 * - {@link BaseMixin} - `./BaseMixin`
 * - {@link mixin}     - `./BaseMixin`
 * - {@link use}       - `./BaseMixin`
 * <br/><br/>
 * @protected
 * @see https://bitbucket.org/mindprism/asciiw/src/master/src/react/BaseComponent.js
 */
const src_react_BaseComponent_js= "/src/react/BaseComponent.js";

///
/// Base Imports
import React, { Component, PropTypes } from 'react';
//noinspection JSUnresolvedVariable
import $ from 'jquery';
import objectPath from 'object-path';
///
/// Local Imports
import { mixin, BaseMixin, use }   from './BaseMixin';
///
/// Local Helpers
use(use);
use(React);
use(PropTypes);
use(src_react_BaseComponent_js);

/**
 * BaseComponent:ReactComponent:BaseMixin
 */
  /// var  ._lifeCycleDebugDisable                                                        :Boolean   -*,
  /// var  ._componentRegistry                                                            :Object    -*,
  /// var  .APP                                                                           :Object    +*,
  ///
  /// cmd  ._registerInstance(that)                                                       :void      -*!
  /// cmd  ._registerMount(that)                                                          :void      -*!
  /// cmd  ._registerUnmount(that)                                                        :void      -*!
  /// cmd  ._reactNodeHtml()                                                              :void      -*!
  ///
  /// ppb  ._mounted                                                                      :Boolean   -,
  /// ppt  .mounted                                                                       :Boolean   %.
  /// ppt  .pubs                                                                          :Object    %.
  /// ppt  .signature                                                                     :String    %.
  /// ppt  .signIt                                                                        :String    %.
  /// ppt  .cas                                                                           :String    %.
  ///
  /// ppt  .Class                                                                         :String    +@.
  /// ppt  .componentId                                                                   :String    +@.
  /// ppt  .componentInfo                                                                 :String    +@.
  ///
  /// ctor constructor(publicProps, publicContext)
  ///
  /// cmd  .proxyFrom(obj :Object)                                                        :void      %!
  /// cmd  ._componentLifecycleDebug(evt :String, ob :Object)                             :void      -!
  ///
  /// evt  .componentWillMount()                                                          :void      %\
  /// evt  .componentDidMount()                                                           :void      %\
  /// evt  .componentWillReceiveProps(props :Object)                                      :void      %\
  /// evt  .componentWillUpdate()                                                         :void      %\
  /// evt  .componentDidUpdate(prevProps :Object, prevState :Object)                      :void      %\
  /// evt  .componentWillUnmount()                                                        :void      %\
  ///
  /// fn   ._componentLifecycleInfo()                                                     :String    -?
  /// fn   ._letDebugLifecycle(evt:String='')                                             :Boolean   -?
  ///
/**
 * <h4>`BaseComponent` Component derived class</h4>
 * Enhancements to Component
 * <br/><br/>
 * <h5>private static members</h5>
 * - {@link BaseComponent._lifeCycleDebugDisable} - Global flag to stop debug system
 * - {@link BaseComponent._componentRegistry} - `{< classname >:{count, _< componentId >: < mounted instance >}`
 * <br/><br/>
 * <h5>public static members</h5>
 * - {@link BaseComponent.APP} - various
 * <br/><br/>
 * <h5>private property backers</h5>
 * - {@link BaseComponent#_mounted} - `true` if mounted
 * - {@link BaseComponent#} -
 * <br/><br/>
 * <h5>protected properties</h5>
 * - {@link BaseComponent#mounted} - readonly
 * - {@link BaseComponent#pubs} - convenience property to access static pubs
 * - {@link BaseComponent#signature} - descriptive instance string
 * - {@link BaseComponent#signIt} - descriptive instance string used in messaging
 * - {@link BaseComponent#cas} - string for ctrl, alt, shift keys down
 * <br/><br/>
 * <h5>public readonly properties</h5>
 * - {@link BaseComponent#Class} - readonly convenience alias for `.constructor`
 * - {@link BaseComponent#componentId} - serial number for instances created of this class
 * - {@link BaseComponent#componentInfo} - informative string
 * <br/><br/>
 * <h5>constructor</h5>
 * - {@link BaseComponent#constructor} - ctor
 * <br/><br/>
 * <h5>protected commands</h5>
 * - {@link BaseComponent#proxyFrom} -
 * <br/><br/>
 * <h5>private commands</h5>
 * - {@link BaseComponent#_componentLifecycleDebug} -
 * <br/><br/>
 * <h5>protected events</h5>
 * - {@link BaseComponent#componentWillMount} - lifecycle event
 * - {@link BaseComponent#componentDidMount} - lifecycle event
 * - {@link BaseComponent#componentWillReceiveProps} - lifecycle event
 * - {@link BaseComponent#componentWillUpdate} - lifecycle event
 * - {@link BaseComponent#componentDidUpdate} - lifecycle event
 * - {@link BaseComponent#componentWillUnmount} - lifecycle event
 * <br/><br/>
 * <h5>private functions</h5>
 * - {@link BaseComponent#_componentLifecycleInfo} - string info
 * - {@link BaseComponent#_letDebugLifecycle} - allow
 * <br/><br/>
 * <h5>Dependencies</h5>
 * - {@link BaseMixin} - `./BaseMixin`
 * - {@link $}              - `jquery`
 * <br/><br/>
 * @public
 */
export default class BaseComponent extends mixin(Component, BaseMixin) {
  //
  /**
   * <h4>`_lifeCycleDebugDisable` static flag</h4>
   * Global flag to stop debug system
   * <br/><br/>
   * @type {boolean} - false
   * <br/><br/>
   * @protected
   */
  static _lifeCycleDebugDisable = false;

  /**
   * <h4>`_componentRegistry` static dictionary</h4>
   * `{< classname >:{count, _< componentId >: < mounted instance >}`
   * <br/><br/>
   * Instance access
   * <br/><br/>
   * @type {Object}
   * @protected
   */
  static _componentRegistry ={};

  //noinspection LocalVariableNamingConventionJS
  /**
   * <h4>`APP` Main static dictionary</h4>
   * Various
   * <br/><br/>
   * @type {Object}
   * @property {node} `reactMouseOverNode`
   * @property {Object} `keysDown` - `.ctrl`, `.alt`, `shift` or `_< keycode >: true`
   * <br/><br/>
   * @protected
   */
  static APP={};

  /**
   * <h4>`_registerInstance` private static command</h4>
   * Add to static counters, give that a componentId
   * <br/><br/>
   * @protected
   */
  static _registerInstance(that) {
    const reg = this._componentRegistry;
    this._ = this._componentRegistry;
    const name = that.constructor.name;
    reg[name] = reg[name]?reg[name]:{
      count:0
    };
    const slot = reg[name];
    slot.count += 1;
    ///                                         set readonly componentId
    Object.defineProperty(that, 'componentId', {
      value:slot.count
    });
    // eslint-disable-next-line
    }//-_registerInstance

  /**
   * <h4>`_registerMount` static command</h4>
   * Put instance in registry as mounted
   * <br/><br/>
   * @protected
   */
  static _registerMount(that) {
    const reg = this._componentRegistry;
    const name = that.constructor.name;
    const slot = reg[name];
    ///                                         use componentId
    slot[`_${that.componentId}`] = that;
    // eslint-disable-next-line
    }//-_registerMount

  /**
   * <h4>`_registerUnmount` static command</h4>
   * Pull instance from registry as unmounted
   * <br/><br/>
   * @protected
   */
  static _registerUnmount(that) {
    const reg = this._componentRegistry;
    const name = that.constructor.name;
    const slot = reg[name];
    ///                                         use componentId
    delete slot[`_${that.componentId}`];
    // eslint-disable-next-line
    }//-_registerUnmount

  //noinspection FunctionWithMultipleLoopsJS
  /**
   * <h4>`_reactNodeHtml`</h4>
   * Mouseover attribs as html
   * <br/><br/>
   * @protected
   */
  static _reactNodeHtml() {
    ///                                         paranoia and checker, {@link ReactTooltip} requires `null`
    if(!this.APP||!this.APP.reactMouseOverNode) {
      return null;
    }
    ///                                         got subject
    const node = this.APP.reactMouseOverNode;
    ///                                         escapeHtml utility
    function escapeHtml(unsafe) {
      return unsafe
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#039;");
    }
    ///                                         css3toObj utility
    function css3toObj(cssText) {
      const regex = /([\w-]*)\s*:\s*([^;]*)/g;
      let match;
      const properties={};
      //noinspection AssignmentResultUsedJS
      while(match = regex.exec(cssText)) { // eslint-disable-line no-cond-assign
        properties[match[1]] = match[2].trim();
      }
      return properties;
    }
    ///                                         css3ObjToLi utility
    function css3ObjToLi(o) {
      let s = '';
      const sorted = Object.keys(o);
      sorted.sort();
      const l = sorted.length;
      for(let i = 0; i<l; i++) {
        const n = sorted[i];
        const v = escapeHtml(o[n]);
        s+=`<li style="padding-left:2em"><code>${n}:${v};</li>`;
      }
      return s;
    }
    ///                                         styleLi utility
    function styleLi(s) {
      return css3ObjToLi(css3toObj(s));
    }
    ///                                         sorter for attributes
    const sorted = [];
    ///                                         ignore these
    const skip = ['data-react-class', 'data-react-file'];
    const l = node.attributes.length;
    ///                                         add items to sorter
    for (let i = 0; i < l; i++) {
      const attrib = node.attributes[i];
      if (attrib.specified) {
        sorted.push(attrib.name);
      }
    }
    ///                                         sort in place
    sorted.sort();
    ///                                         build output html string
    let s='';
    for (let i = 0; i < l; i++) {
      const attrib = node.attributes[sorted[i]];
      if (skip.indexOf(attrib.name)===-1) {
        //sorted.push(attrib.name);
        if ('style' === attrib.name) {
          s += `<li><code>${attrib.name}=</code></li>`;
          s += styleLi(attrib.value);
        } else {
          s += `<li><code>${attrib.name}="${escapeHtml(attrib.value)}"</code></li>`;
        }
      }
    }
    ///                                         return (h4 ul([li>code]))
    let head=`${node.attributes['data-react-class'].value}:${node.attributes['data-react-file'].value}`;
    return `<h4>${head}</h4><ul style="list-style: none;text-align:left; font-weight:normal;">${s}</ul>`;
    // eslint-disable-next-line
    }//-_reactNodeHtml

  //noinspection FunctionNamingConventionJS
  /**
   * <h4>`Class` public readonly property</h4>
   * Alias for `this.constructor`
   * <br/><br/>
   * @protected
   */
  get Class() {
    return this.constructor;
    // eslint-disable-next-line
    }//-Class get

  /**
   * <h4>`_mounted` private flag</h4>
   * Mounted flag for public readonly property `mounted`
   * @see https://facebook.github.io/react/blog/2015/12/16/ismounted-antipattern.html
   * <br/><br/>
   * @protected
   */
  _mounted = false;

  /**
   * <h4>`mounted` public property</h4>
   * Set `_mounted` - private, get `mounted` public
   * <br/><br/>
   * @protected
   */
  set mounted(x) {
    this._mounted = x;
    // eslint-disable-next-line
    }//-mounted set
  get mounted() {
    return this._mounted;
    // eslint-disable-next-line
    }//-mounted get

  /**
   * <h4>`pubs` convenience readonly property</h4>
   * Convenience alias for static pubs
   * <br/><br/>
   * @protected
   */
  get pubs() {
    return this.constructor.pubs;
    // eslint-disable-next-line
    }//-pubs get

  /**
   * <h4>`signature` readonly property</h4>
   * Create unique descriptive string for instance
   * ToDo: needs work
   * <br/><br/>
   * Uses `.constructor.name` `.componentId` `.ref` `.context.root.constructor.name`
   * <br/><br/>
   * @protected
   * @return {string} - dot delimited
   */
  get signature() {
    let namer=this.constructor.name;
    if(this.componentId) {
      namer=`${namer}#${this.componentId}`;
    }
    if(this.ref) {
      namer=`${namer}.${this.ref}`;
    }
    let root = this.context.root;
    if(root) {
      namer=`${namer}.in.${root.constructor.name}`;
    }
    return namer;
    // eslint-disable-next-line
    }//-signature get

  /**
   * <h4>`signIt` readonly property</h4>
   * Create unique descriptive string for instance PubSub messaging
   * ToDo: needs work
   * <br/><br/>
   * Uses `.componentId` `.ref` `.context.root.constructor.name`
   * <br/><br/>
   * @protected
   * @return {string|undefined} - dot delimited or undefined if none
   */
  get signIt() {
    let namer='';
    if(this.componentId) {
      namer=`${namer}#${this.componentId}`;
    }
    if(this.ref) {
      namer=`${namer}.${this.ref}`;
    }
    let root = this.context.root;
    if(root) {
      namer=`${namer}.in.${root.constructor.name}`;
    }
    if ('' === namer) {
      return undefined;
    }
    return namer;
    // eslint-disable-next-line
    }//-signIt get

  /**
   * <h4>`cas` readonly property</h4>
   * String showing ctrl, alt, shift keys down
   * <br/><br/>
   * @protected
   * @return {string}
   */
  get cas() {
    use(this);
    let s='';
    const kd=BaseComponent.APP.keysDown;
    s+=kd.ctrl?'c':'';
    s+=kd.alt?'a':'';
    s+=kd.shift?'s':'';
    return s;
    // eslint-disable-next-line
    }//-cas get

  //noinspection FunctionNamingConventionJS
  /**
   * <h4>`APP` convenience</h4>
   * BaseComponent.APP
   * <br/><br/>
   * Convenience accessor of static APP
   * @protected
   */
  get APP() {
    use(this);
    return BaseComponent.APP;
  }

  /**
   * <h4>`componentInfo` public property</h4>
   * Stringified Object of component properties
   * <br/><br/>
   * <h5>Uses</h5>
   * - `.constructor.name`
   * - `.componentId`
   * - `.props.id`
   * - `.id`
   * - `.ref`
   * - `.displayName`
   * - `.fullName`
   * - `._mounted`
   * - `.context.root.constructor.name`
   * - `keys(.props)`
   * - `keys(.state)`
   * - `keys(.refs)`
   * <br/><br/>
   * ToDo: fixup for refs and others
   * <br/><br/>
   * @protected
   * @return {string}
   */
  get componentInfo() {
    const o={
      'constructor.name':this.constructor.name,
      'componentId':this.componentId,
      'props.id':this.props.id,
      'id':this.id,
      'ref':this.ref,
      'displayName':this.displayName,
      'fullName':this.fullName,
      '_mounted':this._mounted,
    };
    const root = this.context.root;
    if(root) {
      o.contextRoot=root.constructor.name;
    }
    if(Object.keys(this.props).length) {
      o['Object.keys(props)']=Object.keys(this.props);
    }
    if(Object.keys(this.state).length) {
      o['Object.keys(state)']=Object.keys(this.state);
    }
    if(Object.keys(this.refs).length) {
      o['Object.keys(refs)']=Object.keys(this.refs);
    }
    return JSON.stringify(o);
    // eslint-disable-next-line
    }//-componentInfo get

  /**
   * <h4>`constructor`</h4>
   * <br/><br/>
   * <h5>Actions</h5>
   * - {@link BaseComponent._registerInstance}
   * - {@link BaseMixin#_autobind}
   * - initialize static `hear`
   * - initialize `App.keysDown`
   * - hook mouseover on `[data-react-class]` and update `state.reactTip`, `state.reactTipHtml`
   * @protected
   * @param {Object} publicProps
   * @param {Object} publicContext
   */
  constructor(publicProps, publicContext) {
    super(publicProps, publicContext);
    //initPubs(this.constructor);
    BaseComponent._registerInstance(this);
    this._autobind();
    ///                                         initialize static hear
    if(this.constructor.hear) {
      const  hear = this.constructor.hear;
      for (let i in hear) {
        if(!hear.hasOwnProperty(i)) {
          continue;
        }
        const h = hear[i];
        const cls = h[0];
        const fn = h[1];
        const path = `pubs${i.substr(cls.name.length)}`;
        //noinspection JSUnresolvedFunction
        const val = objectPath.get(cls, path);
        if('Dispatch' !== val.constructor.name) {
          throw new Error(`Dispatch not found:${cls.name}${path}`);
        }
        val.hear(fn.bind(this));
      }
    }
    ///                                         initialize App.keysDown
    if (document&&!BaseComponent.APP.keysDown) {
      BaseComponent.APP.keysDown={};
      //noinspection LocalVariableNamingConventionJS
      const APP = BaseComponent.APP;
      const km = {
        '_16': 'shift'
        , '_17': 'ctrl'
        , '_18': 'alt'
      };
      $(document).on('keydown', e => {
        const k = km[`_${e.keyCode}`];
        if (k) { // ctrl
          APP.keysDown[k]=true;
        }
      });
      $(document).on('keyup', e => {
        const k = km[`_${e.keyCode}`];
        if (k) {
          delete APP.keysDown[k];
        }
      });
      ///                                          hook mouseover on `[data-react-class]` and update `state.reactTip`, `state.reactTipHtml`
      $(document).on('mouseover', '[data-react-class]', e =>{
        //console.log(e.currentTarget);
        APP.reactMouseOverNode = e.currentTarget;
        const html = BaseComponent._reactNodeHtml();
        const node = e.currentTarget;
        let head=`${node.attributes['data-react-class'].value}:${node.attributes['data-react-file'].value}`;
        this.setState({ reactTip: head, reactTipHtml: html });
        return false;
      });
    }//-keysDown
    // eslint-disable-next-line
    }//-constructor

  /**
   * <h4>`say` private command</h4>
   * Dispatch.say wrapping data and using extra and instance signIt
   * <br/><br/>
   * @protected
   * @param {Dispatch} disp
   * @param {*} data
   * @param {string} [extra]
   */
  say(disp, data, extra) {
    const datawrap = {
      sender: this
      , data
    };
    let extrawrap = this.signIt;
    if (extra) {
      extrawrap=extra;
      if (this.signIt) {
        extrawrap+=`.${this.signIt}`;
      }
    }
    disp.say(datawrap, extrawrap);
    // eslint-disable-next-line
    }//-say

  //noinspection FunctionWithMultipleLoopsJS
  /**
   * <h4>`proxyFrom` public command</h4>
   * Inherit from obj, functions and get/set
   * <br/><br/>
   * @protected
   */
  proxyFrom(obj) {
    let proto = Object.getPrototypeOf(obj);
    while(proto) {
      for(let prop of Object.getOwnPropertyNames(proto)) {
        if(prop in this) {
          continue;
        }
        if(("constructor" !== prop) && (proto[prop] instanceof Function)) {
          this[prop] = proto[prop].bind(obj);
        } else {
          let descriptor = Object.getOwnPropertyDescriptor(proto, prop);
          if(descriptor && ("get" in descriptor) && descriptor.get) {
            descriptor.get = descriptor.get.bind(obj);
            if(("set" in descriptor) && descriptor.set) {
              descriptor.set = descriptor.set.bind(obj);
            }
            Object.defineProperty(this, prop, descriptor);
          }
        }
      }
      proto = Object.getPrototypeOf(proto);
    }
    // eslint-disable-next-line
    }//-proxyFrom

  /**
   * <h4>`componentWillUpdate` event</h4>
   * Maybe Lifecycle Debug - {@link BaseComponent._componentLifecycleDebug}
   * <br/><br/>
   * @protected
   */
  componentWillUpdate() {
    this._componentLifecycleDebug('componentWillUpdate');
    // eslint-disable-next-line
    }//-componentWillUpdate

  /**
   * <h4>`componentDidUpdate` event</h4>
   * Maybe Lifecycle Debug - {@link BaseComponent._componentLifecycleDebug}
   * <br/><br/>
   * @protected
   */
  componentDidUpdate(prevProps, prevState) {
    this._componentLifecycleDebug('componentDidUpdate', { prevProps:prevProps, prevState:prevState });
    // eslint-disable-next-line
    }//-componentDidUpdate

  /**
   * <h4>`componentWillReceiveProps` event</h4>
   * Maybe Lifecycle Debug - {@link BaseComponent._componentLifecycleDebug}
   * <br/><br/>
   * @protected
   */
  componentWillReceiveProps(props) {
    this._componentLifecycleDebug('componentWillReceiveProps', { props:props });
    // eslint-disable-next-line
    }//-componentWillReceiveProps

  /**
   * <h4>`componentDidMount` event</h4>
   * Set mounted flag when
   * Maybe Lifecycle Debug - {@link BaseComponent._componentLifecycleDebug}
   * <br/><br/>
   * @protected
   */
  componentDidMount() {
    this._mounted=true;
    BaseComponent._registerMount(this);
    this._componentLifecycleDebug('componentDidMount');
    // eslint-disable-next-line
    }//-componentDidMount

  /**
   * <h4>`componentWillUnmount` even</h4>
   * Set mounted flag when
   * Maybe Lifecycle Debug - {@link BaseComponent._componentLifecycleDebug}
   * <br/><br/>
   * @protected
   */
  componentWillUnmount() {
    this._componentLifecycleDebug('componentWillUnmount');
    BaseComponent._registerUnmount(this);
    this._mounted=false;
    // eslint-disable-next-line
    }//-componentWillUnmount

  /**
   * <h4>`componentWillMount` event</h4>
   * Maybe Lifecycle Debug - {@link BaseComponent._componentLifecycleDebug}
   * <br/><br/>
   * @protected
   */
  componentWillMount() {
    this._componentLifecycleDebug('componentWillMount');
    // eslint-disable-next-line
    }//-componentWillMount

  //noinspection OverlyComplexFunctionJS
  /**
   * <h4>`_componentLifecycleDebug` private command</h4>
   * Send styled lifecycle messages to console if _letDebugLifecycle
   * <br/><br/>
   * @protected
   */
  _componentLifecycleDebug(evt, ob) {
    const doit=this._letDebugLifecycle(evt);
    if(!doit) {
      return;
    }
    ///                                         cssStr utility
    function cssStr(color, radius, borderStyle, borderWidth, borderColor) {
      return `color:${color};border-radius:${radius}px;border-style:${borderStyle};border-width:${borderWidth}px;border-color:${borderColor};`;
    }
    const std='font-size:13px;background-color:#000000;padding-left:8px;padding-right:8px;';
    const evtc={
      componentWillMount           :cssStr('#00AA00', '3', 'dashed', '1', '#FFFF00'), //yellow  dashed  3
      componentDidMount            :cssStr('#00FF00', '3', ' solid', '1', '#FF0000'), //green   solid   3
      componentWillUnmount         :cssStr('#008800', '3', 'dotted', '1', '#FF8000'), //orange  dotted 3
      componentWillReceiveProps    :cssStr('#990099', '5', ' solid', '1', '#FF00FF'), //pink    solid  6
      shouldComponentUpdate        :cssStr('#0066CC', '0', 'double', '3', '#00FFFF'), //cyan    double  0
      componentWillUpdate          :cssStr('#0080FF', '0', ' inset', '2', '#FFFF00'), //yellow  inset   0
      componentDidUpdate           :cssStr('#3399FF', '0', 'outset', '2', '#00FF00'), //green   outset  0
    };
    const nm={
      componentWillMount           :'WillMount       ',           //yellow  dashed  3
      componentDidMount            :'DidMount        ',           //green   solid   3
      componentWillUnmount         :'WillUnmount     ',           //orange  dotted 3
      componentWillReceiveProps    :'WillReceiveProps',           //pink    solid  6
      shouldComponentUpdate        :'ShouldUpdate    ',           //cyan    double  0
      componentWillUpdate          :'WillUpdate      ',           //yellow  inset   0
      componentDidUpdate           :'DidUpdate       ',           //green   outset  0
    };
    //noinspection JSCheckFunctionSignatures
    const node=this.node;//ReactDOM.findDOMNode(this);
    const namer=this.signature;
    ///                                         get detail level from localStorage, default 1
    let detail=`${null === localStorage.getItem('::lifecycle:detail') ? '1' : localStorage.getItem('::lifecycle:detail')}`;
    if('1234'.indexOf(detail)===-1) {detail='1';}
    ///                                         output based on detail and ob is defined
    if('4' === detail) {
      if (ob === undefined) {
        console.info(`%c:${nm[evt]}`, std + evtc[evt], namer, node, this);
      } else {
        console.info(`%c:${nm[evt]}`, std + evtc[evt], namer, ob, node, this);
      }
    }else{
      if (ob === undefined) {
        console.info(`%c:${nm[evt]}`, std + evtc[evt], namer, this._componentLifecycleInfo(), node);
      } else {
        console.info(`%c:${nm[evt]}`, std + evtc[evt], namer, this._componentLifecycleInfo(), ob, node);
      }
    }
    ///                                         check maybe call debugger -- :debugger:<class>:<event>
    const cls = this.constructor.name;
    if(localStorage.getItem(`::lifecycle:debugger:*:${evt}`)
     ||localStorage.getItem(`::lifecycle:debugger:${cls}:${evt}`)
     ||localStorage.getItem(`::lifecycle:debugger:${cls}:*`)
     ) {
      //noinspection OverlyComplexBooleanExpressionJS
      if(!localStorage.getItem(`::lifecycle:debugger-:*:${evt}`)
       &&!localStorage.getItem(`::lifecycle:debugger-:${cls}:${evt}`)
       &&!localStorage.getItem(`::lifecycle:debugger-:${cls}:*`)
       &&!localStorage.getItem('::lifecycle:debugger-:*:*')
       &&!localStorage.getItem('::lifecycle:debugger-')
       ) {
        debugger;
      }
    }
    // eslint-disable-next-line
    }//-_componentLifecycleDebug

  /**
   * <h4>`_componentLifecycleInfo` private function</h4>
   * get JSON.stringified object details based on localStorage settings
   * <br/><br/>
   * <h5>Uses</h5>
   * - `.props.id`
   * - `.id`
   * - `._mounted`
   * - `keys(.props)`
   * - `keys(.state)`
   * - `keys(.refs)`
   * <br/><br/>
   * ToDo: fix for refs and others
   * <br/><br/>
   * @protected
   * @return {string}
   */
  _componentLifecycleInfo() {
    ///                                         get detail level from localStorage, default 1
    let detail=`${null === localStorage.getItem('::lifecycle:detail') ? '1' : localStorage.getItem('::lifecycle:detail')}`;
    if('1234'.indexOf(detail)===-1) {detail='1';}
    ///                                         bail this if detail is 1
    if('1' === detail) {
      return '';
    }
    ///                                         add more info
    const o={
      'props.id':this.props.id,
      'id':this.id,
      '_mounted':this._mounted,
    };
    if('3' === detail) {
      if(Object.keys(this.props).length) {
        o['Object.keys(props)']=Object.keys(this.props);
      }
      if(Object.keys(this.state).length) {
        o['Object.keys(state)']=Object.keys(this.state);
      }
      if(Object.keys(this.refs).length) {
        o['Object.keys(refs)']=Object.keys(this.refs);
      }
    }
    return JSON.stringify(o);
    // eslint-disable-next-line
    }//-_componentLifecycleInfo

  /**
   * <h4>`_letDebugLifecycle` private function</h4>
   * Decide debug filters for given lifecycle event
   * <br/><br/>
   * This uses localStorage to allow/deny output, default is deny.
   * Keys begin with '::lifecycle'
   * <br/><br/>
   * @protected
   * @param {string} evt (*'')
   * @return {boolean}
   */
   /// KEY             VALUE         MEANING
   ///
   /// :allow          any           if missing, disengage system permanently
   /// :suspend        any           if present, deny all immediately (short-circuit queries)
   /// :detail         1 | 2 | 3 | 4 level of output detail
   ///
   /// :classes        classlist     allow these classes output
   /// :classes-       classlist     deny  these classes output, overrides :classes
   ///
   /// :events         eventlist     allow these events output
   /// :events-        eventlist     deny  these events output, overrides :events
   ///
   /// :class-:<name>  any           deny  this class, overrides :classes
   /// :class:<name>   any           allow this class, overrides :class- and :class-:<name>
   ///
   /// :event-:<name>  any           deny  this event, overrides :events
   /// :event:<name>   any           allow this event, overrides :events- and :event-:<name>
   ///
   /// :debugger:<class|*>:<event|*>     call debugger
   /// :debugger-:<class|*>:<event|*>    block debugger
   ///
   /// EXAMPLE START
    /*
    localStorage.setItem('-::item','EXAMPLE of disabled item, placing a dash in front so it is not picked up');
    //
    localStorage.setItem('::lifecycle:detail','4');
    localStorage.setItem('::lifecycle:allow','not having any value under this key will permanently short-circuit lifecycle reporting/queries system at start');
    localStorage.setItem('-::lifecycle:suspend','any value here will suspend reporting');
    localStorage.setItem('::lifecycle:IconMenuOnDemand:componentDidUpdate','any value here will report for the specified class and event');
    //
    localStorage.setItem('-::update:forceUpdateState','any value here will branch execution to use .setState(dummy) instead of .forceUpdate -- comparison switch');
    localStorage.setItem('-::update:noTimeout','any value here will execute updates immediately without timeout');
    */
  _letDebugLifecycle(evt='') {
    ///                                         main shortcircuit
    if(BaseComponent._lifeCycleDebugDisable) {
      return false;
    }
    ///                                         set main shortcircuit
    if(!localStorage.getItem('::lifecycle:allow')) {
      BaseComponent._lifeCycleDebugDisable=true;
      return false;
    }
    ///                                         bail on suspend
    if(localStorage.getItem('::lifecycle:suspend')) {
      return false;
    }
    ///                                         utility in list or list is star
    function inlist_star(l, v) {
      if('*'===l) {
        return true;
      }
      const ls=l.split(' ');
      return ls.indexOf(v)!==-1;
    }
    ///                                         check complex flags
    function checkfor(t, n) {
      const sfx='class' === t?'es':'s';
      const ts=t+sfx;
      const plural =localStorage.getItem(`::lifecycle:${ts}`);
      const plural_=localStorage.getItem(`::lifecycle:${ts}-`);
      const sing   =localStorage.getItem(`::lifecycle:${t}:${n}`);
      const sing_  =localStorage.getItem(`::lifecycle:${t}-:${n}`);
      //
      const in_plural =(null === plural)?false:inlist_star(plural, n);
      const in_plural_=(null === plural_)?false:inlist_star(plural_, n);
      const in_sing   =(null !== sing);
      const in_sing_  =(null !== sing_);
      /// in_sing>in_sing_>in_plural_>in_plural
      //noinspection OverlyComplexBooleanExpressionJS
      return (in_plural || in_sing) && ((!in_plural_)&&(!in_sing_));
    }//-checkfor
    const c_class=checkfor('class', this.constructor.name);
    const c_evt  =checkfor('event', evt);
    const both   =localStorage.getItem(`::lifecycle:${this.constructor.name}:${evt}`);
    return (c_class&&c_evt) || both;
    // eslint-disable-next-line
    }//-_letDebugLifecycle
}//-BaseComponent
/**
 * Use BC._.<class>._<componentId> to access components from devtools cmdline
 */
window.BC=window.BC?window.BC:BaseComponent;

