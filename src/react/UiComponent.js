/* global */
/**
 * <h4>`UiComponent` react class export</h4>
 * <a href="aip://open/C%3A%5C%5F%5Casciiw%5Csrc%5Creact%5CUiComponent%2Ejs">C:\_\asciiw\src\react\UiComponent.js</a><br>
 * <br/><br/>
 * <h5>NPM Dependencies</h5>
 * - {@link ReactDOM} - `react-dom`
 * <br/><br/>
 * <h5>Project Dependencies</h5>
 * None
 * <br/><br/>
 * <h5>Local Dependencies</h5>
 * - {@link BaseComponent} - `./BaseComponent`
 * - {@link use}           - `./BaseMixin`
 * <br/><br/>
 * <h5>Product</h5>
 * {@link UiComponent}
 * @protected
 * @see https://bitbucket.org/mindprism/asciiw/src/master/src/react/UiComponent.js
 */
const src_react_UiComponent_js= "/src/react/UiComponent.js";

///
/// Base Imports
import ReactDOM from 'react-dom';
///
/// Local Imports
import BaseComponent from './BaseComponent';
import { use }   from './BaseMixin';
///
/// Local Helpers
use(src_react_UiComponent_js);

/**
 * <h4>`UiComponent` react class</h4>
 * Set context root using `props.id`
 * <br/><br/>
 * <h5>constructor</h5>
 * - {@link UiComponent#constructor} - Set context root using `props.id`
 * <h5>public properties</h5>
 * - {@link UiComponent#node} - get dom node if mounted
 * <br/><br/>
 * - {@link src_react_UiComponent_js} file
 * @public
 */
export default class UiComponent extends BaseComponent {
  //
  /**
   * <h4>`node` public property</h4>
   * Get dom node if mounted
   * <br/><br/>
   * @public
   * @return {node}
   */
  get node() {
    //noinspection JSCheckFunctionSignatures
    return ReactDOM.findDOMNode(this);
    // eslint-disable-next-line
    }//-node get

  /**
   * <h4>`constructor`</h4>
   * Set context root using `props.id`
   * <br/><br/>
   * @public
   * @param {Object} publicProps
   * @param {Object} publicContext
   */
  constructor(publicProps, publicContext) {
    super(publicProps, publicContext);
    if (this.context && this.context.root && this.props.id) {
      this.context.root[this.props.id] = this;
    }
    // eslint-disable-next-line
    }//-constructor
}
