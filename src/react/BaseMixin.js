/**
 * <h4>`BaseMixin` class, `mixin` function, `use` function </h4>
 * <a href="aip://open/C%3A%5C%5F%5Casciiw%5Csrc%5Creact%5CBaseMixin%2Ejs">C:\_\asciiw\src\react\BaseMixin.js</a><br>
 * <br/><br/>
 * <h5>NPM Dependencies</h5>
 * None
 * <br/><br/>
 * <h5>Project Dependencies</h5>
 * None
 * <br/><br/>
 * <h5>Local Dependencies</h5>
 * None
 * <br/><br/>
 * <h5>Products</h5>
 * - {@link mixin} - function
 * - {@link use} - function
 * - {@link BaseMixin} - class
 * <br/><br/>
 * @protected
 * @see https://bitbucket.org/mindprism/asciiw/src/master/src/react/BaseMixin.js
 */
const src_react_BaseMixin_js= "/src/react/BaseMixin.js";
//noinspection JSUnusedLocalSymbols
/**
 * <h4>`use` function export</h4>
 * Webstorm noop to get rid of unused lint warnings
 * <br/><br/>
 * @public
 * @return {Object} - null
 */
export function use(...many) { // eslint-disable-line
  return null;
}

///
/// Local Helpers
use(src_react_BaseMixin_js);

//noinspection FunctionWithMultipleLoopsJS
/**
 * <h4>`mixin` function export</h4>
 * Assume properties and functions from given mixins
 * <br/><br/>
 * @public
 * @param {Component} parent
 * @param {class} mixins
 * @return  {Component} with mixins
 */
export function mixin(parent, ...mixins) {
  class Mixed extends parent {}
  for(let mixin of mixins) {
    let proto = Object.getPrototypeOf(new mixin());
    for(let prop of Object.getOwnPropertyNames(proto)) {
      if(("constructor" !== prop) && (proto[prop] instanceof Function)) {
        Mixed.prototype[prop] = proto[prop];
      }
    }
    for(let prop of Object.getOwnPropertyNames(mixin)) {
      let descriptor = Object.getOwnPropertyDescriptor(mixin, prop);
      if(descriptor && ("get" in descriptor)) {
        Object.defineProperty(Mixed, prop, descriptor);
      }
    }
  }
  return Mixed;
}//-mixin

/**
 * <h4>`BaseMixin` react mixin class export</h4>
 * Mixin for React
 * <br/><br/>
 * <h5>public commands</h5>
 * - {@link BaseMixin#_autobind} - Assign and bind to this each method with leading $, this prototype chain
 * <br/><br/>
 * @protected
 */
export class BaseMixin {
  //noinspection FunctionWithMultipleLoopsJS
  /**
   * <h4>`_autobind`</h4>
   * Assign and bind to this each method with leading $, this prototype chain
   * <br/><br/>
   * @protected
   */
  _autobind() {
    let proto = Object.getPrototypeOf(this);
    let bound = {};
    //
    while(proto) {
      for(let prop of Object.getOwnPropertyNames(proto)) {
        if((0 === prop.indexOf("$")) && !(prop in bound)) {
          let name = prop.substring(1);
          this[name] = proto[prop].bind(this);
          bound[prop] = this[name];
        }
      }
      proto = Object.getPrototypeOf(proto);
    }
  }//-_autobind
}//-BaseMixin
