## RMF-src

`C:\_\asciiw\src`

- `FmColorName` - `cyan-dark`
- `ColorHex` - `#4A9EFE`
- `ColorRgb` - `74,158,254`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-cyan-dark-48.png)](aip://open/C%3A%5C_%5Casciiw%5Csrc) _Main Application_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-16.png)](aip://open/C%3A%5C_%5Casciiw%5CREADME.md) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5CDesktop.ini) - ini

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprism/asciiw/src/master/src/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/esdoc-16.png)](http://mrobbinsassoc.com/projects/asciiw/esdoc/file/src/index.js.html) - esdoc
- [![](http://mrobbinsassoc.com/images/icons/md/plato-16.png)](http://mrobbinsassoc.com/projects/asciiw/plato/files/_index_js/index.html) - plato

### Anticipates

_none_

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5Csrc%5Capi%5CREADME.md) `api` - Rest Api
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-cyan-16.png)](aip://open/C%3A%5C_%5Casciiw%5Csrc%5Ccomponents%5CREADME.md) `components` - React Components
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-cyan-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5Csrc%5Creact%5CREADME.md) `react` - React Base
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-green-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5Csrc%5Cservices%5CREADME.md) `services` - Various Services
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-purple-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5Csrc%5Cutils%5CREADME.md) `utils` - Utilities and Data Containers

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_jsx-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5CApp.jsx) `App.jsx` - Main component
- [![](http://mrobbinsassoc.com/images/icons/md/_scss-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5CApp.scss) `App.scss`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5CApp.test.js) `App.test.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_jsx-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5CAppHeader.jsx) `AppHeader.jsx` - Main component header
- [![](http://mrobbinsassoc.com/images/icons/md/_scss-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5CAppHeader.scss) `AppHeader.scss`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Cexternals.js) `externals.js` - EsDoc Definitions
- [![](http://mrobbinsassoc.com/images/icons/md/_js-index-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Cindex.js) `index.js` - App mounter
- [![](http://mrobbinsassoc.com/images/icons/md/_scss-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Cindex.scss) `index.scss`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Clatesunset.png) `latesunset.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Clogo.svg) `logo.svg`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Cmra-%31%39%32.png) `mra-192.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5CREADME.html) `README.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5Csrc%5Ctypedefs.js) `typedefs.js`

