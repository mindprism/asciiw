/* global */
/**
 * <h4>`AppHeader` component</h4>
 * <a href="aip://open/C%3A%5C%5F%5Casciiw%5Csrc%5CAppHeader%2Ejsx">C:\_\asciiw\src\AppHeader.jsx</a><br>
 * <br/><br/>
 * Header for App
 * <h5>NPM Dependencies</h5>
 * - {@link React}          - `react`
 * - {@link Component}      - `react`
 * - {@link PropTypes}      - `react`
 * - {@link $}              - `jquery`
 * - {@link classNames}     - `classnames`
 * <br/><br/>
 * <h5>Project Dependencies</h5>
 * - {@link Utils}        - `../utils/utils`
 * - {@link Dispatch}     - `../utils/utils`
 * - {@link UiComponent}  - `../react/UiComponent`
 * <br/><br/>
 * <h5>Local Dependencies</h5>
 * - {@link AdPic} - `./components/AdPic`
 * <br/><br/>
 * <h5>Produces</h5>
 * - {@link AppHeader}
 * @protected
 * @see https://bitbucket.org/mindprism/asciiw/src/master/src/AppHeader.jsx
 */
const src_AppHeader_jsx= "/src/AppHeader.jsx";

/// Base Imports
import UiComponent from './react/UiComponent';
import React, { Component, PropTypes } from 'react';
//noinspection JSUnresolvedVariable
import $ from 'jquery';
import classNames from 'classnames';
///
/// Util Imports
import Utils from './utils/utils';
import { use, Dispatch } from './utils/utils';
///
/// Service Imports
///
/// Component Imports
import AdPic from './components/AdPic';
///
/// Local Imports
import './AppHeader.css';
//noinspection JSUnresolvedVariable
import logo from './logo.svg';
//noinspection JSUnresolvedVariable
import mra from './mra-192.png';
///
/// Local Helpers
use($);
use(Utils);
use(Dispatch);
use(Component);
use(PropTypes);
use(src_AppHeader_jsx);

/// Class
/**
 * <h4>`AppHeader` React Component default export</h4>
 * - {@link AppHeader.displayName} - AppHeader
 * - {@link AppHeader.propTypes} - none
 * - {@link AppHeader.defaultProps} - none
 * - {@link AppHeader#constructor} - no actions
 * - {@link AppHeader#render} - div>(img.logo img.mra h1 h3 div.reactTip div.ads>([AdPic]) p.App-intro)
 * <br/><br/>
 * <h5>NPM Dependencies</h5>
 * - {@link classNames}     - `classnames`
 * <br/><br/>
 * <h5>Local Dependencies</h5>
 * - {@link UiComponent}  - `../react/UiComponent`
 * - {@link AdPic}        - `./components/AdPic`
 * <br/><br/>
 * - file {@link src_AppHeader_jsx}
 * @protected
 */
class AppHeader extends UiComponent {
  //
  /**
   * <h4>`AppHeader.displayName` static member</h4>
   *  String is used in debugging messages. JSX sets this value automatically.
   * <br/><br/>
   * @protected
   */
  static displayName = 'AppHeader';

  /**
   * <h4>`AppHeader.propTypes` static member</h4>
   *   Defined as a property on the component class itself, to define what types the props should be. It should be a map from prop names to types as defined in React.PropTypes. In development mode, when an invalid value is provided for a prop, a warning will be shown in the JavaScript console. In production mode, propTypes checks are skipped for efficiency.
   * <br/><br/>
   * None
   * <br/><br/>
   * @protected
   */
  static propTypes = {};

  /**
   * <h4>`AppHeader.defaultProps` static member</h4>
   *  Defined as a property on the component class itself, to set the default props for the class. This is used for undefined props, but not for null props.
   * <br/><br/>
   * None
   * <br/><br/>
   * @protected
   */
  static defaultProps = {};

  /**
   * <h4>`AppHeader` constructor</h4>
   * - call `super`
   * <br/><br/>
   * - see {@link AppHeader_React_Component_export} for docs
   * - see {@link src_AppHeader_jsx} for file docs
   * <br/><br/>
   * @protected
   * @param {Object} props
   * @param {Object} context
   */
  constructor(props, context) {
    super(props, context);
    // set state
    this.state = { // keep to avoid no-useless-constructors
    };
  }

  /// LifeCycle
    /// componentWillMount
    /// componentDidMount
    /// componentWillReceiveProps
    /// shouldComponentUpdate
    /// componentWillUpdate
    /// componentDidUpdate
    /// componentWillUnmount
  ///

  /**
   * <h4>`AppHeader#render` function</h4>
   * div>(img.logo img.mra h1 h3 div.reactTip div.ads>([AdPic]) p.App-intro)
   * <br/><br/>
   * todo - parameterize adpic count
   * <br/><br/>
   * <h5>Dependencies</h5>
   * - {@link AdPic} - `./components/AdPic`
   * @protected
   */
  render() {
    use(this); // webstorm can be static
    const rootClasses = classNames({
      'AppHeader': true
      , 'root-AppHeader': true
    });
    //noinspection MagicNumberJS
    const adCount=Math.floor(window.innerWidth/100);
    return (
      <div className={rootClasses} data-react-class="AppHeader" data-react-file="AppHeader.jsx">
        <img src={logo} className="App-logo right" alt="logo" />
        <img src={mra} className="App-logo left" alt="logo" />
        <h1>Discount Ascii Warehouse</h1>
        <h3>Adopt A Rescued Ascii Face Before It's Too Late!</h3>
        <div className="reactTip"><code>{this.state.reactTip}</code></div>
        <div className="ads">
          {[...new Array(adCount-2)].map((e, i)=>{return (<AdPic key={i} href="http://placekitten.com/" width="100px" height="100px"/>); })}
        </div>
        <p className="App-intro">
          Sponsored by: <em>Cute Kitties Who Already Have Nice Homes</em>;
        </p>
      </div>
    );
  }
}
/**
 * <h4>`AppHeader` React Component default export</h4>
 * <br/><br/>
 * - file {@link src_AppHeader_jsx}
 * @protected
 */
const AppHeader_React_Component_export = 'EsDocs symbol';
use(AppHeader_React_Component_export);
export default AppHeader;
