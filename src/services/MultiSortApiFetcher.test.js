/* global */
/**
 * <h4>`MultiSortApiFetcher` tests</h4>
 * <a href="aip://open/C%3A%5C%5F%5Casciiw%5Csrc%5Cservices%5CMultiSortApiFetcher%2Etest%2Ejs">C:\_\asciiw\src\services\MultiSortApiFetcher.test.js</a><br>
 * <br/><br/>
 * <h5>NPM Dependencies</h5>
 * <br/><br/>
 * <h5>Project Dependencies</h5>
 * - {@link Utils} - `../utils/utils`
 * <br/><br/>
 * <h5>Local Dependencies</h5>
 * - {@link NewLineJsonRequester} - `../api/NewLineJsonRequester`
 * - {@link MultiSortApiFetcher} - `../services/MultiSortApiFetcher`
 * <br/><br/>
 * @protected
 * @see https://bitbucket.org/mindprism/asciiw/src/master/src/services/MultiSortApiFetcher.test.js
 */
const src_services_MultiSortApiFetcher_test_js= "/src/services/MultiSortApiFetcher.test.js";

///
/// Util Imports
import Utils from '../utils/utils';
import { use } from '../utils/utils';
import JestHelpers from '../utils/JestHelpers';

const J = JestHelpers;
/// Local Imports
import NewLineJsonRequester from '../api/NewLineJsonRequester';
import MultiSortApiFetcher from '../services/MultiSortApiFetcher';
///
/// Local Helpers
use(Utils);
use(src_services_MultiSortApiFetcher_test_js);
///
/// Testing Helpers

/**
 * <h4>`logFaces` helper</h4>
 * <br/><br/>
 * @protected
 */
const logFaces = (data) => {
  const l=data.length;
  let s='';
  for (let i = 0; i < l; i++) {
    const item = data[i];
    s+= `${item.face} | `;
  }
  console.log(s);
};

///
/// Test Suites
/**
 * <h4>`MultiSortApiFetcher should construct` suite</h4>
 * <br/><br/>
 * @protected
 */
const MultiSortApiFetcher_should_construct_suite = true;
if (MultiSortApiFetcher_should_construct_suite) {
  /**
   * @test {NewLineJsonRequester}
   * @test {MultiSortApiFetcher}
   */
  describe('should construct', () => {
    /**
     * @test {NewLineJsonRequester}
     * @test {MultiSortApiFetcher}
     */
    it('should construct', () => {
      const requester = new NewLineJsonRequester('http://localhost:3000/api', {});
      //noinspection ObjectAllocationIgnored,JSCheckFunctionSignatures
      new MultiSortApiFetcher({ api:requester, keyField:'id' });
    }); //-test
  }); //-suite
} //-MultiSortApiFetcher_should_construct_suite

/**
 * <h4>`MultiSortApiFetcher instances` suite</h4>
 * <br/><br/>
 * @protected
 */
const MultiSortApiFetcher_instances_suite = true;
if (MultiSortApiFetcher_instances_suite) {
  /**
   * @test {NewLineJsonRequester}
   * @test {MultiSortApiFetcher}
   * @test {MultiSortApiFetcher#fetch}
   */
  describe('with an instance', () => {
    let fetcher;
    /**
     *
     */
    beforeAll(() => {
      const requester = new NewLineJsonRequester('http://localhost:3000/api', {});
      //noinspection JSCheckFunctionSignatures
      fetcher = new MultiSortApiFetcher({ api:requester, keyField:'id' });
    }); //-beforeAll
    /**
     * @test {MultiSortApiFetcher#fetch}
     */
    it('should get 2 items using id', () => {
      const p = fetcher.fetch('id', 2);
      p.then((data) =>{
        console.log(data.length);
        J.expectArray(data);
        expect(data.length).toBe(4);
        logFaces(data);
        //console.log(data[0].face,data[0].face);
      }); //-then
      return p;
    }); //-test
    /**
     * @test {MultiSortApiFetcher#fetch}
     */
    it('should get 2 items using size', () => {
      const p = fetcher.fetch('size', 2);
      p.then((data) =>{
        console.log(data.length);
        J.expectArray(data);
        //expect(data.length).toBe(2);
        logFaces(data);
      }); //-then
      return p;
    }); //-test
  }); //-suite
} //-MultiSortApiFetcher_instances_suite

