/* global */
/**
 * <h4>`MultiSortObjectCache` service class</h4>
 * <a href="aip://open/C%3A%5C%5F%5Casciiw%5Csrc%5Cservices%5CMultiSortApiFetcher%2Ejs">C:\_\asciiw\src\services\MultiSortApiFetcher.js</a><br>
 * <br/><br/>
 * <h5>NPM Dependencies</h5>
 * - {@link $}              - `jquery`
 * <br/><br/>
 * <h5>Project Dependencies</h5>
 * - {@link Utils} - `../utils/utils`
 * <br/><br/>
 * <h5>Local Dependencies</h5>
 * - {@link MultiSortObjectCache} - `../utils/MultiSortObjectCache`
 * <br/><br/>
 * @protected
 * @see https://bitbucket.org/mindprism/asciiw/src/master/src/services/MultiSortApiFetcher.js
 */
const src_services_MultiSortApiFetcher_js= "/src/services/MultiSortApiFetcher.js";

/// Base Imports
//noinspection JSUnresolvedVariable
import $ from 'jquery';
///
/// Util Imports
import Utils from '../utils/utils';
import { use, Dispatch } from '../utils/utils';
///
/// Local Imports
import MultiSortObjectCache from '../utils/MultiSortObjectCache';
///
/// Local Helpers
use($);
use(Utils);
use(src_services_MultiSortApiFetcher_js);

/// Class
/**
 * <h4>`MultiSortApiFetcher` service class</h4>
 * Fetch and cache using a {@link ApiRequester} and {@link MultiSortObjectCache}
 * <br/><br/>
 * <h5>static private members</h5>
 * - {@link MultiSortApiFetcher.defaultOptions} - fetchSize :Number (*10), api :undefined (!), keyField :String (!), requestTimeout :Number (*10000)
 * <br/><br/>
 * <h5>static public members</h5>
 * - {@link MultiSortApiFetcher.pubs} - busy, requesting, timeout, data
 * <br/><br/>
 * <h5>public properties</h5>
 * - {@link MultiSortApiFetcher#signIt} - Specialty instance string
 * <br/><br/>
 * <h5>private commands</h5>
 * - {@link MultiSortApiFetcher#say} - Dispatch.say wrapping data and using extra and instance signIt
 * <br/><br/>
 * <h5>private members</h5>
 * - {@link MultiSortApiFetcher#_options} - Constructor _options - open ended for now, See {@link MultiSortApiFetcher.defaultOptions}
 * - {@link MultiSortApiFetcher#_sortFulfilledLengths} - Skip values keyed on sort field
 * - {@link MultiSortApiFetcher#_requesting} - Debounce requests
 * - {@link MultiSortApiFetcher#_requestTimeoutId} - To cancel request watcher
 * <br/><br/>
 * <h5>constructor</h5>
 * - {@link MultiSortApiFetcher#constructor} - Assign options, set cache with new MultiSortObjectCache
 * <br/><br/>
 * <h5>private functions</h5>
 * - {@link MultiSortApiFetcher#_getSkipForSorted} - Return/create skip for particular sort
 * <br/><br/>
 * <h5>private commands</h5>
 * - {@link MultiSortApiFetcher#_setSkipForSorted} - Set skip point for particular sort
 * - {@link MultiSortApiFetcher#_ifCanThenBeginRequest} - If requesting say busy and return false, else requesting true, say requesting true, start request watcher
 * - {@link MultiSortApiFetcher#_request} - If requesting return undefined, else do request with api, return promise
 * <br/><br/>
 * <h5>private handlers</h5>
 * - {@link MultiSortApiFetcher#_handleData} - Clear request watcher timeout, do says, add items to cache
 * - {@link MultiSortApiFetcher#_handleRequestTimeout} - Clear request watcher, say timeout say requesting false
 * <br/><br/>
 * <h5>public commands</h5>
 * - {@link MultiSortApiFetcher#fetch} - If skip is same more than limit, return using cache and do another request, else do request
 * <br/><br/>
 * <h5>Dependencies</h5>
 * - {@link $}       - `jquery`
 * <br/><br/>
 * - file {@link src_services_MultiSortApiFetcher_js}
 * @protected
 */
class MultiSortApiFetcher {
  //
  //noinspection MagicNumberJS
  /**
   * <h4>`MultiSortApiFetcher.defaultOptions` dictionary</h4>
   * fetchSize :Number (*10), api :undefined (!), keyField :String (!), requestTimeout :Number (*10000)
   * <br/><br/>
   * @protected
   * @type {{fetchSize: number, api: undefined, keyField: undefined, requestTimeout: number}}
   */
  static defaultOptions = {
    fetchSize: 10
    , api: undefined          // required class
    , keyField: undefined     // optional string
    , requestTimeout: 10*1000 // optional number
  };

  /**
   * <h4>`MultiSortApiFetcher.pubs` static object</h4>
   * {@link PubSub} {@link Dispatch} Interface
   * See {@link AppDisplay#pubs} for documentation
   * <br/><br/>
   * - `busy` - notification, request was made while busy, no data
   * - `requesting` - toggle, boolean
   * - `timeout` - server timed out, null
   * - `data` - got data, data
   * <br/><br/>
   * @type {Object}
   * @property {Dispatch} busy - notification, request was made while busy, no data
   * @property {Dispatch} requesting - toggle, boolean
   * @property {Dispatch} timeout - server timed out, null
   * @property {Dispatch} data - got data, data
   * @public
   */
  static pubs = {
    busy:'notification, request was made while busy, no data'
    , requesting:'toggle, boolean'
    , timeout:'server timed out, null'
    , data:'got data, data'
  };

  /**
   * <h4>`sighIt` readonly property</h4>
   * Specialty instance string
   * <br/><br/>
   * <h5>Uses</h5>
   * - `._options.id`
   * - `._options.name`
   * - `._options.api.constructor.name`
   * - `._options.api.url`
   * <br/><br/>
   * @protected
   * @return {string|undefined}
   */
  get signIt() {
    let namer='';
    if(this._options.id) {
      namer=`${namer}.${this._options.id}`;
    }
    if(this._options.name) {
      namer=`${namer}.${this._options.name}`;
    }
    if(this._options.api) {
      namer=`${namer}.${this._options.api.constructor.name}`;
    }
    if(this._options.api) {
      namer=`${namer}.${this._options.api.url}`;
    }
    if ('' === namer) {
      return undefined;
    }
    return namer;
    // eslint-disable-next-line
    }//-signIt get

  /**
   * <h4>`MultiSortApiFetcher#say` private command</h4>
   * Dispatch.say wrapping data and using extra and instance signIt
   * <br/><br/>
   * Standard say taken from {@link BaseComponent#say}
   * <br/><br/>
   * @protected
   * @param {Dispatch|string} disp - string is to fool webstorm
   * @param {*} data
   * @param {string} [extra]
   */
  say(disp, data, extra) {
    const datawrap ={
      sender: this
      , data
    };
    let extrawrap = this.signIt;
    if (extra) {
      extrawrap=extra;
      if (this.signIt) {
        extrawrap+=`.${this.signIt}`;
      }
    }
    disp.say(datawrap, extrawrap);
    // eslint-disable-next-line
    }//-say

  /**
   * <h4>`MultiSortApiFetcher#_options` private dictionary</h4>
   * Constructor _options - open ended for now, See {@link MultiSortApiFetcher.defaultOptions}
   * <br/><br/>
   * fetchSize :number (*10), api :ApiRequester (!), keyField :string (*undefined), requestTimeout :number (*10000)
   * <br/><br/>
   * @protected
   * @type {MultiSortApiFetcherConstructorOptions}
   * @property {number} fetchSize (*10)
   * @property {ApiRequester} api (!)
   * @property {string} keyField (*undefined)
   * @property {number} requestTimeout (*10000)
   */
  _options = {};

  /**
   * <h4>`MultiSortApiFetcher#_sortFulfilledLengths` private dictionary</h4>
   * Skip values keyed on sort field
   * <br/><br/>
   * @protected
   * @type {Object}
   */
  _sortFulfilledLengths = {};

  /**
   * <h4>`MultiSortApiFetcher#_requesting` private flag</h4>
   * Debounce requests
   * <br/><br/>
   * @protected
   */
  _requesting = false;

  /**
   * <h4>`MultiSortApiFetcher#_requestTimeoutId` private handle</h4>
   * To cancel request watcher
   * <br/><br/>
   * @protected
   */
  _requestTimeoutId = null;

  /**
   * <h4>`MultiSortApiFetcher` constructor</h4>
   * Assign options, set cache with new MultiSortObjectCache
   * <br/><br/>
   * <h5>Dependencies</h5>
   * - {@link MultiSortObjectCache} - this.cache, constructed with options.keyField
   * <br/><br/>
   * @public
   * @param {MultiSortApiFetcherConstructorOptions} [options={}]
   */
  constructor(options = {}) {
    this._options = Object.assign({}, this.constructor.defaultOptions, options);
    this.cache = new MultiSortObjectCache({
      keyField: this._options.keyField
    });
  }

  /**
   * <h4>`MultiSortApiFetcher#_getSkipForSorted` start point</h4>
   * Return/create skip for particular sort
   * <br/><br/>
   * @protected
   * @param {string} sortField
   */
  _getSkipForSorted(sortField) {
    const lens=this._sortFulfilledLengths;
    if (!lens[sortField]) {
      lens[sortField]=0;
    }
    return lens[sortField];
  }

  /**
   * <h4>`MultiSortApiFetcher#_setSkipForSorted` set start point</h4>
   * Set skip point for particular sort
   * <br/><br/>
   * @protected
   * @param {string} sortField
   * @param {number} len
   */
  _setSkipForSorted(sortField, len) {
    const lens=this._sortFulfilledLengths;
    lens[sortField]=len;
  }

  /**
   * <h4>`MultiSortApiFetcher#_handleData` bound handler</h4>
   * Clear request watcher timeout, do says, add items to cache
   * <br/><br/>
   * - Clear and null {@link MultiSortApiFetcher#_requestTimeoutId}
   * - say `requesting false`
   * - say `data`
   * - add `items` to {@link MultiSortApiFetcher#cache}
   * @protected
   * @param {[Face]} items
   */
  _handleData = (items) =>{
    clearTimeout(this._requestTimeoutId);
    this._requestTimeoutId = null;
    this._requesting = false;
    this.say(MultiSortApiFetcher.pubs.requesting, false);
    this.say(MultiSortApiFetcher.pubs.data, items);
    this.cache.add(items);
  };

  /**
   * <h4>`MultiSortApiFetcher#_handleRequestTimeout` private bound handler</h4>
   * Clear request watcher, say timeout say requesting false
   * <br/><br/>
   * - null {@link MultiSortApiFetcher#_requestTimeoutId}
   * - say `timeout`
   * - say `requesting false`
   * <br/><br/>
   * @protected
   */
  _handleRequestTimeout = () => {
    this._requestTimeoutId=null;
    this.say(MultiSortApiFetcher.pubs.timeout, null);
    this.say(MultiSortApiFetcher.pubs.requesting, false);
  };

  /**
   * <h4>`MultiSortApiFetcher#_ifCanThenBeginRequest` setup request query/command</h4>
   * If requesting say busy and return false, else requesting true, say requesting true, start request watcher
   * <br/><br/>
   * - If `._requesting` say `busy` return `false`
   * - Set `._requesting` to `true`
   * - Say `requesting true`
   * - Set `._requestTimoutId` watch `_handleRequestTimeout` for `._options._requestTimeout`
   * <br/><br/>
   * @protected
   */
  _ifCanThenBeginRequest() {
    if (this._requesting) {
      this.say(MultiSortApiFetcher.pubs.busy, null);
      return false;
    }
    this._requesting = true;
    this.say(MultiSortApiFetcher.pubs.requesting, true);
    this._requestTimeoutId = setTimeout(this._handleRequestTimeout, this._options._requestTimeout);
    return true;
  }

  /**
   * <h4>`MultiSortApiFetcher#_request` main internal command</h4>
   * If requesting return undefined, else do request with api, return promise
   * <br/><br/>
   * - return undefined if not `._ifCanThenBeginRequest`
   * - use `limit` from param else `._options.fetchSize`
   * - use `skip` from `._getSkipForSorted` using `sortField`
   * - request from api using sortField, skip and limit*2
   * - return promise
   * <br/><br/>
   * @protected
   * @param {string} sortField
   * @param {number} [limit]
   * @return {Promise|undefined}
   */
  _request(sortField, limit) {
    let can = this._ifCanThenBeginRequest();
    if (!can) {
      return;
    }
    const lim = limit || this._options.fetchSize;
    const api = this._options.api;
    const skip = this._getSkipForSorted(sortField);
    return new Promise((resolve, reject) => {
      const prom = api.request({ sort:sortField, skip:skip, limit: lim*2 });
      prom.then((data) => {
        this._handleData(data);
        resolve(this.cache.sortedBy(sortField));
      });
      prom.catch((xx) => {
        reject(xx);
      });
    });
  }

  /**
   * <h4>`MultiSortApiFetcher#fetch` main public command</h4>
   * If skip is same more than limit, return using cache and do another request, else do request
   * <br/><br/>
   * @public
   * @param {string} sortField
   * @param {number} limit
   * @return {Promise}
   */
  fetch(sortField, limit) {
    const skip=this._getSkipForSorted(sortField);
    console.log('skip, limit', skip, limit);
    if (skip >= limit) {
      console.log('doing cache+request');
      //noinspection JSUnusedLocalSymbols
      return new Promise((resolve, reject) => { // eslint-disable-line
        let a = this.cache.sortedBy(sortField);
        resolve(a);
        //resolve(a.slice(0, limit));
        //this._request(sortField, limit);
      });
    } else {
      console.log('doing request');
      //noinspection JSUnusedLocalSymbols
      return new Promise((resolve, reject) => { // eslint-disable-line
        const p = this._request(sortField, limit);
        if(!p) {
          this.say(MultiSortApiFetcher.pubs.busy, null);
          return;
        }
        p.then((data) => {
          this._setSkipForSorted(sortField, data.length);
          resolve(data);
          //resolve(data.slice(0, limit));
        });
      });
    }
  }
}
Dispatch.initPubs(MultiSortApiFetcher);
/**
 * <h4>`MultiSortApiFetcher` service default export</h4>
 * <br/><br/>
 * - file {@link src_services_MultiSortApiFetcher_js}
 * @protected
 */
const MultiSortApiFetcher_export = 'EsDocs symbol';
use(MultiSortApiFetcher_export);

export default MultiSortApiFetcher;
