# Notes on Challenge
Good Day! There is a short [video](https://www.screencast.com/t/Olr2ccWWRvS) of the application.

## First of all...

ANGTFT - "Ain't Nobody Got Time For That!" -- will be cited when I come across areas I neglected to develop
due to the limited anticipated investment and instead preferring to give a more rounded view of my capabilities.

## Issues with the Assigned Challenge

I am not sure whether some issues are intentional or incidental, but I am treating the task as a real-world problem.

- **API: Item id** Is not tied to a particular **Face**, it is basically a misnomer. It is instead a record number unique to the sort, followed by random digits.
I have chosen to use the **Face** string itself as a unique Id, which I need because of the *keep-displayed-items-during-sort-switch* approach I decided to take.

- **UI Functional Design:** Given that the sort can be changed mid-stream, in order to display a "true list" (with no gaps between items under a particular sort -- gaps being 'records on the server but not displayed'),
the items loaded need to be discarded/removed from the display.<br/><br/> If you **keep** the items loaded/displayed, there will be such gaps, and these will be *misinformation* to the user.
The must-reload is very dissatisfying, so I decided to go halfway toward a solution by keeping the items, and allowing new items to thread themselves into proper positions.
<br/><br/>I have also animated this reordering/inserting to make it more fun. This design is more challenging.
<br/><br/>To take it to completion (ANGTFT), what has to be done is that the items in the cache have to be given a set of bi-directional pointers for each sort type.
Then the potential 'gaps' can be shown in the UI. Then the user will have an idea of what they are really looking at, and see what is really unescapable in a system where you keep the items that you have when switching the sort.
<br/><br/>If this all sounds confusing, I ask that you take a quiet moment and think about it. There is no perfect solution if you wish to keep your items, and throwing them all away doesn't feel right either. A more informative UI could be made if a more sophisticated API was available - for instance - showing the number of records in a gap.
<br/><br/>I suppose a 'sort visible by...' as well as a 'get new list sorted by...' could be done, but even then you have to thread in new items when you scroll down for the former.


## Features

- Bootstrapped from create-react-app
- ES6 focused
- Promises not callbacks
- Sass

- Components made with *steps* toward generic design, this means they are not airtight (ANGTFT)
- Engineered a Declaritive PubSub system which is used instead of attributes as callbacks, or using react context. See 
[AppDisplay.pubs](http://mrobbinsassoc.com/projects/asciiw/esdoc/class/src/components/AppDisplay.jsx~AppDisplay.html#static-member-pubs) :: [source](http://mrobbinsassoc.com/projects/asciiw/esdoc/file/src/components/AppDisplay.jsx.html#lineNumber223), 
[AppDisplay.hear](http://mrobbinsassoc.com/projects/asciiw/esdoc/class/src/components/AppDisplay.jsx~AppDisplay.html#static-member-hear) :: [source](http://mrobbinsassoc.com/projects/asciiw/esdoc/file/src/components/AppDisplay.jsx.html#lineNumber169) 
and [utils.js:Dispatch](http://mrobbinsassoc.com/projects/asciiw/esdoc/class/src/utils/utils.js~Dispatch.html) :: [source](http://mrobbinsassoc.com/projects/asciiw/esdoc/file/src/utils/utils.js.html#lineNumber241)

- Full EsDoc inline markup and documentation [online](http://mrobbinsassoc.com/projects/asciiw/esdoc), [repo](https://bitbucket.org/mindprism/asciiw-esdoc)
- Plato complexity report [online](http://mrobbinsassoc.com/projects/asciiw/plato), [repo](https://bitbucket.org/mindprism/asciiw-plato)
- Enhanced, automated Readme
- Some [TDD for apis](http://mrobbinsassoc.com/projects/asciiw/esdoc/test.html) using Jest

- Grid sort animation (react-motion)
- Face animation and styling (jquery, trianglify, tinycolor2)
- Css head injection for trianglify images
- Proxy to api

- Modest inspector - hold ctrl with mouseover React component for tooltip info
- Balance between component inheritance and dynamic extension

Thank you for your time, I look forward to hearing from you,

Mark Robbins
mark.robbins@mrobbinsassoc.com
