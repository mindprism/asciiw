## RMF-.idea-markdown-navigator

`C:\_\asciiw\.idea\markdown-navigator`

- `FmColorName` - `gray`
- `ColorHex` - `#9999A2`
- `ColorRgb` - `153,153,162`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-48.png)](aip://open/C%3A%5C_%5Casciiw%5C.idea%5Cmarkdown-navigator) _Markdown Navigator Profiles_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-dark-16.png)](aip://open/C%3A%5C_%5Casciiw%5C.idea%5CREADME.md) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.idea%5Cmarkdown-navigator%5CDesktop.ini) - ini

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprism/asciiw/src/master/.idea/markdown-navigator/) - bitbucket

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.idea%5Cmarkdown-navigator%5Cprofiles_settings.xml) `profiles_settings.xml` - Individual settings
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.idea%5Cmarkdown-navigator%5CREADME.html) `README.html`

