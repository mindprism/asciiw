## README

`C:\_\asciiw\.idea`

- `FmColorName` - `yellow-dark`
- `ColorHex` - `#E6F940`
- `ColorRgb` - `230,249,64`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-dark-48.png)](aip://open/C%3A%5C_%5Casciiw%5C.idea) _Webstorm Configuration_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-16.png)](aip://open/C%3A%5C_%5Casciiw%5CREADME.md) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.idea%5CDesktop.ini) - ini

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprism/asciiw/src/master/.idea/) - bitbucket

### Anticipates

_none_

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-16.png)](aip://open/C%3A%5C_%5Casciiw%5C.idea%5CinspectionProfiles%5CREADME.md) `inspectionProfiles` - Various Inspection Profile Configurations
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-16.png)](aip://open/C%3A%5C_%5Casciiw%5C.idea%5Cmarkdown-navigator%5CREADME.md) `markdown-navigator` - Markdown Navigator Profiles

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.idea%5Casciiw.iml) `asciiw.iml`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.idea%5Cdeployment.xml) `deployment.xml`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.idea%5Cencodings.xml) `encodings.xml` - `file:charset`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.idea%5Cmarkdown-navigator.xml) `markdown-navigator.xml` - MarkdownProjectSettings
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.idea%5Cmisc.xml) `misc.xml`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.idea%5Cmodules.xml) `modules.xml` - ProjectModuleManager references `*.iml`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.idea%5CREADME.html) `README.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.idea%5Cvcs.xml) `vcs.xml` - VcsDirectoryMappings Git
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.idea%5CwatcherTasks.xml) `watcherTasks.xml` - File Watchers: `Scss Indexer` - `_tools/styleindexer.bat`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C.idea%5CwebServers.xml) `webServers.xml`

