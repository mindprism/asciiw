## README

`C:\_\asciiw\_extra\_i`

- `FmColorName` - `yellow-light`
- `ColorHex` - `#F5FE9E`
- `ColorRgb` - `245,254,158`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-light-48.png)](aip://open/C%3A%5C_%5Casciiw%5C_extra%5C_i) _summary_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-16.png)](aip://open/C%3A%5C_%5Casciiw%5C_extra%5CREADME.md) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C_extra%5C_i%5CDesktop.ini) - ini

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprism/asciiw/src/master/_extra/_i/) - bitbucket

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C_extra%5C_i%5C%21i-js-alias.txt) `!i-js-alias.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C_extra%5C_i%5C%21i-react-component-file.txt) `!i-react-component-file.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C_extra%5C_i%5C%21i-urls.txt) `!i-urls.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C_extra%5C_i%5CREADME.html) `README.html`

