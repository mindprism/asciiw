## RMF-_extra

`C:\_\asciiw\_extra`

- `FmColorName` - `yellow`
- `ColorHex` - `#F2FE7B`
- `ColorRgb` - `242,254,123`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-48.png)](aip://open/C%3A%5C_%5Casciiw%5C_extra) _summary_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-16.png)](aip://open/C%3A%5C_%5Casciiw%5CREADME.md) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C_extra%5CDesktop.ini) - ini

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprism/asciiw/src/master/_extra/) - bitbucket

### Anticipates

_none_

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-light-16.png)](aip://open/C%3A%5C_%5Casciiw%5C_extra%5C_i%5CREADME.md) `_i`

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Casciiw%5C_extra%5CREADME.html) `README.html`

